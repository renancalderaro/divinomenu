// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static m0(id) => "Pedido: #${id} foi cancelado";

  static m1(productName) =>
      "Você tem certeza que deseja remover o ${productName} dos seu pedido?";

  static m2(productName) =>
      "O produto ${productName} foi adicionado ao seu carrinho";

  static m3(productName) => "O ${productName} foi incluído em seu carrinho...";

  static m4(productName) => "O ${productName} foi removido do seu carrinho";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function>{
        "about": MessageLookupByLibrary.simpleMessage("Sobre"),
        "add": MessageLookupByLibrary.simpleMessage("Adicionar"),
        "add_delivery_address": MessageLookupByLibrary.simpleMessage(
            "Adicionar endereço de entrega"),
        "add_new_delivery_address": MessageLookupByLibrary.simpleMessage(
            "Adicionar endereço de entrega"),
        "add_to_cart": MessageLookupByLibrary.simpleMessage("Comprar"),
        "address": MessageLookupByLibrary.simpleMessage("Endereço"),
        "addresses_refreshed_successfuly": MessageLookupByLibrary.simpleMessage(
            "Endereço atualizado com sucesso"),
        "all": MessageLookupByLibrary.simpleMessage("Todos"),
        "all_product":
            MessageLookupByLibrary.simpleMessage("Todos os produtos"),
        "app_language": MessageLookupByLibrary.simpleMessage("Idioma"),
        "app_settings": MessageLookupByLibrary.simpleMessage("Configurações"),
        "application_preferences":
            MessageLookupByLibrary.simpleMessage("Preferências"),
        "apply_filters":
            MessageLookupByLibrary.simpleMessage("Aplicar filtros"),
        "areYouSureYouWantToCancelThisOrder":
            MessageLookupByLibrary.simpleMessage(
                "Tem certeza de que deseja cancelar este pedido?"),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancelar"),
        "cancelOrder": MessageLookupByLibrary.simpleMessage("Cancelar pedido"),
        "canceled": MessageLookupByLibrary.simpleMessage("Cancelado"),
        "card_american_on_delivery":
            MessageLookupByLibrary.simpleMessage("Amex - Débito ou Crédito"),
        "card_elo_on_delivery":
            MessageLookupByLibrary.simpleMessage("Elo - Débito ou Crédito"),
        "card_hiper_on_delivery":
            MessageLookupByLibrary.simpleMessage("Hiper - Débito ou Crédito"),
        "card_masterd_on_delivery": MessageLookupByLibrary.simpleMessage(
            "MasterCard - Débito ou Crédito"),
        "card_number": MessageLookupByLibrary.simpleMessage("NÚMERO DO CARTÃO"),
        "card_text_on_delivery":
            MessageLookupByLibrary.simpleMessage("Cartão na entrega"),
        "card_visad_on_delivery":
            MessageLookupByLibrary.simpleMessage("Visa - Débito ou Crédito"),
        "cart": MessageLookupByLibrary.simpleMessage("Carrinho"),
        "carts_refreshed_successfuly": MessageLookupByLibrary.simpleMessage(
            "Carrinho atualizado com sucesso"),
        "cash_on_delivery":
            MessageLookupByLibrary.simpleMessage("Dinheiro na entrega"),
        "category": MessageLookupByLibrary.simpleMessage("Categoria"),
        "category_refreshed_successfuly": MessageLookupByLibrary.simpleMessage(
            "Categoria atualizada com sucesso"),
        "checkout": MessageLookupByLibrary.simpleMessage("Finalizar"),
        "clear": MessageLookupByLibrary.simpleMessage("Limpar"),
        "clickOnTheProductToGetMoreDetailsAboutIt":
            MessageLookupByLibrary.simpleMessage(
                "Clique no produto para ver mais sobre ele"),
        "clickToPayWithRazorpayMethod": MessageLookupByLibrary.simpleMessage(
            "Clique para pagar com RazorPay"),
        "click_on_the_stars_below_to_leave_comments":
            MessageLookupByLibrary.simpleMessage(
                "Clique nas estrelas abaixo para comentar"),
        "click_to_confirm_your_address_and_pay_or_long_press":
            MessageLookupByLibrary.simpleMessage(
                "Clique para confirmar seu endereço e pagar ou pressione e segure para editar seu endereço"),
        "click_to_pay_cash_on_delivery": MessageLookupByLibrary.simpleMessage(
            "Clique para pagar em dinheiro na entrega"),
        "click_to_pay_on_pickup": MessageLookupByLibrary.simpleMessage(
            "Clique para pagar quando retirar o produto"),
        "click_to_pay_with_your_mastercard":
            MessageLookupByLibrary.simpleMessage(
                "Clique para pagar com seu MasterCard"),
        "click_to_pay_with_your_paypal_account":
            MessageLookupByLibrary.simpleMessage(
                "Clique para pagar com sua conta do PayPal"),
        "click_to_pay_with_your_visa_card":
            MessageLookupByLibrary.simpleMessage(
                "Clique para pagar com seu cartão Visa"),
        "close": MessageLookupByLibrary.simpleMessage("Fechar"),
        "closed": MessageLookupByLibrary.simpleMessage("Fechado"),
        "completeYourProfileDetailsToContinue":
            MessageLookupByLibrary.simpleMessage(
                "Insira o seu telefone para continuar"),
        "confirm_payment":
            MessageLookupByLibrary.simpleMessage("Confirme o pagamento"),
        "confirm_your_delivery_address": MessageLookupByLibrary.simpleMessage(
            "Confirme seu endereço de entrega"),
        "confirmation": MessageLookupByLibrary.simpleMessage("Confirmação"),
        "current_location":
            MessageLookupByLibrary.simpleMessage("Localização atual"),
        "cvc": MessageLookupByLibrary.simpleMessage("CVC"),
        "cvv": MessageLookupByLibrary.simpleMessage("CVV"),
        "dark_mode": MessageLookupByLibrary.simpleMessage("Modo escuro"),
        "default_credit_card":
            MessageLookupByLibrary.simpleMessage("Cartão de crédito padrão"),
        "deliverable": MessageLookupByLibrary.simpleMessage("Disponível"),
        "delivery": MessageLookupByLibrary.simpleMessage("Entrega"),
        "deliveryAddressOutsideTheDeliveryRangeOfThisMarkets":
            MessageLookupByLibrary.simpleMessage(
                "Este restaurante não faz entrega no endereço escolhido ou algum produto não está disponível em estoque."),
        "deliveryMethodNotAllowed":
            MessageLookupByLibrary.simpleMessage("Não permitida!"),
        "delivery_address":
            MessageLookupByLibrary.simpleMessage("Endereço de entrega"),
        "delivery_address_removed_successfully":
            MessageLookupByLibrary.simpleMessage(
                "Endereço de entrega removido com sucesso!"),
        "delivery_addresses":
            MessageLookupByLibrary.simpleMessage("Endereços de entrega"),
        "delivery_fee": MessageLookupByLibrary.simpleMessage("Taxa de entrega"),
        "delivery_or_pickup": MessageLookupByLibrary.simpleMessage("Entrega"),
        "description": MessageLookupByLibrary.simpleMessage("Descrição"),
        "details": MessageLookupByLibrary.simpleMessage("Detalhes"),
        "discover__explorer": MessageLookupByLibrary.simpleMessage("Explorar"),
        "dont_have_any_item_in_the_notification_list":
            MessageLookupByLibrary.simpleMessage("Sem notificações"),
        "dont_have_any_item_in_your_cart": MessageLookupByLibrary.simpleMessage(
            "Não tem nenhum item no seu carrinho"),
        "edit": MessageLookupByLibrary.simpleMessage("Editar"),
        "email": MessageLookupByLibrary.simpleMessage("Endereço de e-mail"),
        "email_address":
            MessageLookupByLibrary.simpleMessage("Endereço de e-mail"),
        "email_to_reset_password":
            MessageLookupByLibrary.simpleMessage("Digite o seu e-mail"),
        "english": MessageLookupByLibrary.simpleMessage("Inglês"),
        "error_verify_email_settings": MessageLookupByLibrary.simpleMessage(
            "Erro! Verificar configurações de email"),
        "exp_date": MessageLookupByLibrary.simpleMessage("Data de validade"),
        "expiry_date": MessageLookupByLibrary.simpleMessage("DATA DE VALIDADE"),
        "faq": MessageLookupByLibrary.simpleMessage("Perguntas frequentes"),
        "faqsRefreshedSuccessfuly": MessageLookupByLibrary.simpleMessage(
            "Faqs atualizadas com sucesso"),
        "favorite_products":
            MessageLookupByLibrary.simpleMessage("Produtos Favoritos"),
        "favorites": MessageLookupByLibrary.simpleMessage("Favoritos"),
        "favorites_refreshed_successfuly": MessageLookupByLibrary.simpleMessage(
            "Favoritos atualizado com sucesso"),
        "featured_products":
            MessageLookupByLibrary.simpleMessage("Recomendados"),
        "fields":
            MessageLookupByLibrary.simpleMessage("Categorias de restaurantes"),
        "filter": MessageLookupByLibrary.simpleMessage("Filtros"),
        "forMoreDetailsPleaseChatWithOurManagers":
            MessageLookupByLibrary.simpleMessage(
                "For more details, please chat with our managers"),
        "free": MessageLookupByLibrary.simpleMessage("Grátis"),
        "full_address": MessageLookupByLibrary.simpleMessage(
            "Insira a rua, número, complementos, bairro e cidade"),
        "full_name": MessageLookupByLibrary.simpleMessage("Nome completo"),
        "guest":
            MessageLookupByLibrary.simpleMessage("Clique aqui para entrar"),
        "haveCouponCode":
            MessageLookupByLibrary.simpleMessage("Você tem um cupom?"),
        "help__support":
            MessageLookupByLibrary.simpleMessage("Ajuda e Suporte"),
        "help_support": MessageLookupByLibrary.simpleMessage("Ajuda e Suporte"),
        "help_supports":
            MessageLookupByLibrary.simpleMessage("Ajuda e Suporte"),
        "hint_full_address":
            MessageLookupByLibrary.simpleMessage("Rua, bairro, cidade, país"),
        "home": MessageLookupByLibrary.simpleMessage("Início"),
        "home_address": MessageLookupByLibrary.simpleMessage("Endereço"),
        "how_would_you_rate_this_market": MessageLookupByLibrary.simpleMessage(
            "Como você classificaria este restaurante?"),
        "how_would_you_rate_this_market_": MessageLookupByLibrary.simpleMessage(
            "Como você classificaria este restaurante?"),
        "i_dont_have_an_account":
            MessageLookupByLibrary.simpleMessage("Eu não tenho uma conta"),
        "i_forgot_password": MessageLookupByLibrary.simpleMessage(
            " Eu não lembro a minha senha :("),
        "i_have_account_back_to_login":
            MessageLookupByLibrary.simpleMessage("Eu já tenho conta"),
        "i_remember_my_password_return_to_login":
            MessageLookupByLibrary.simpleMessage("Lembrei a minha senha"),
        "information": MessageLookupByLibrary.simpleMessage("Sobre"),
        "invalidCouponCode":
            MessageLookupByLibrary.simpleMessage("Cupom Invalido"),
        "items": MessageLookupByLibrary.simpleMessage("Itens"),
        "john_doe": MessageLookupByLibrary.simpleMessage("Divino Menu"),
        "keep_your_old_meals_of_this_market":
            MessageLookupByLibrary.simpleMessage(
                "Manter os produtos ddeste restaurante"),
        "km": MessageLookupByLibrary.simpleMessage("Km"),
        "kriacoes_agency_module_minimum_order":
            MessageLookupByLibrary.simpleMessage("Pedido Minimo"),
        "kriacoes_agency_module_minimum_order_alert_par2":
            MessageLookupByLibrary.simpleMessage(
                "\n O valor do seu pedido atual é:"),
        "kriacoes_agency_module_minimum_order_alert_part1":
            MessageLookupByLibrary.simpleMessage(
                "O valor mínimo do pedido não está incluído nas taxas."),
        "kriacoes_agency_module_otp_verification_btn_check":
            MessageLookupByLibrary.simpleMessage("Verificar"),
        "kriacoes_agency_module_otp_verification_btn_signup":
            MessageLookupByLibrary.simpleMessage("Continuar"),
        "kriacoes_agency_module_otp_verification_cannot_create_phoneauthcredential":
            MessageLookupByLibrary.simpleMessage(
                "O código não pode ficar em branco!"),
        "kriacoes_agency_module_otp_verification_change_number_msg_1":
            MessageLookupByLibrary.simpleMessage("O seu telefone"),
        "kriacoes_agency_module_otp_verification_change_number_msg_2":
            MessageLookupByLibrary.simpleMessage(
                "foi verificado, para alterar seu número de telefone você deverá fazer a verificação novamente, gostaria de alterar?"),
        "kriacoes_agency_module_otp_verification_change_number_no":
            MessageLookupByLibrary.simpleMessage("Não"),
        "kriacoes_agency_module_otp_verification_change_number_title":
            MessageLookupByLibrary.simpleMessage("Alterar Telefone"),
        "kriacoes_agency_module_otp_verification_change_number_yes":
            MessageLookupByLibrary.simpleMessage("Alterar"),
        "kriacoes_agency_module_otp_verification_choose_country":
            MessageLookupByLibrary.simpleMessage("Escolha seu país"),
        "kriacoes_agency_module_otp_verification_choose_country_last":
            MessageLookupByLibrary.simpleMessage("Último escolhido"),
        "kriacoes_agency_module_otp_verification_choose_country_name":
            MessageLookupByLibrary.simpleMessage("Digite o nome do país"),
        "kriacoes_agency_module_otp_verification_choose_country_search":
            MessageLookupByLibrary.simpleMessage("Pesquisar"),
        "kriacoes_agency_module_otp_verification_empty":
            MessageLookupByLibrary.simpleMessage(
                "O telefone não pode ficar vazio!"),
        "kriacoes_agency_module_otp_verification_empty_code":
            MessageLookupByLibrary.simpleMessage(
                "O código não pode ficar vazio!"),
        "kriacoes_agency_module_otp_verification_error_invalid_verification_code":
            MessageLookupByLibrary.simpleMessage(
                "O código informado é inválido!"),
        "kriacoes_agency_module_otp_verification_error_session_expired":
            MessageLookupByLibrary.simpleMessage(
                "O código enviando expirou, solicite um novo código..."),
        "kriacoes_agency_module_otp_verification_generic_msg":
            MessageLookupByLibrary.simpleMessage("Ocorreu um erro: "),
        "kriacoes_agency_module_otp_verification_generic_title":
            MessageLookupByLibrary.simpleMessage("Opss..."),
        "kriacoes_agency_module_otp_verification_give_the_code":
            MessageLookupByLibrary.simpleMessage("Informe o código"),
        "kriacoes_agency_module_otp_verification_header":
            MessageLookupByLibrary.simpleMessage("Insira seu n° de celular"),
        "kriacoes_agency_module_otp_verification_hint":
            MessageLookupByLibrary.simpleMessage("(xx) xxxxx-xxxx"),
        "kriacoes_agency_module_otp_verification_incorrect":
            MessageLookupByLibrary.simpleMessage(
                "O telefone informado não está correto, informe o código do país, o prefixo da operadora e seu número"),
        "kriacoes_agency_module_otp_verification_invalid_format_msg":
            MessageLookupByLibrary.simpleMessage(
                "O formato do número de telefone fornecido está incorreto. Insira o número de telefone: [número do assinante incluindo código de área]."),
        "kriacoes_agency_module_otp_verification_invalid_format_title":
            MessageLookupByLibrary.simpleMessage("Formato inválido!"),
        "kriacoes_agency_module_otp_verification_network_msg":
            MessageLookupByLibrary.simpleMessage(
                "Ocorreu um erro com a rede, por favor tente novamente."),
        "kriacoes_agency_module_otp_verification_network_title":
            MessageLookupByLibrary.simpleMessage("Erro de conexão!"),
        "kriacoes_agency_module_otp_verification_not_authorized_msg":
            MessageLookupByLibrary.simpleMessage(
                "Este aplicativo não está autorizado, por favor verifique as credenciais do firebase."),
        "kriacoes_agency_module_otp_verification_not_authorized_title":
            MessageLookupByLibrary.simpleMessage("Não autorizado!"),
        "kriacoes_agency_module_otp_verification_phone":
            MessageLookupByLibrary.simpleMessage("Telefone"),
        "kriacoes_agency_module_otp_verification_phone_confirmed_header":
            MessageLookupByLibrary.simpleMessage("Telefone confirmado!"),
        "kriacoes_agency_module_otp_verification_phone_signup_step":
            MessageLookupByLibrary.simpleMessage(
                "Seu número de telefone foi confirmado com sucesso, agora conclua seu cadastro."),
        "kriacoes_agency_module_otp_verification_send_code":
            MessageLookupByLibrary.simpleMessage("Enviar Código"),
        "kriacoes_agency_module_otp_verification_sms_send_code":
            MessageLookupByLibrary.simpleMessage("Sms enviado para"),
        "kriacoes_agency_module_otp_verification_sub_header":
            MessageLookupByLibrary.simpleMessage(
                "Enviaremos um código SMS para o número"),
        "kriacoes_agency_module_otp_verification_too_long_msg":
            MessageLookupByLibrary.simpleMessage(
                "O número de telefone fornecido está incorreto. Ele está muito longo para ser um telefone, verifique e tente novamente..."),
        "kriacoes_agency_module_otp_verification_too_long_title":
            MessageLookupByLibrary.simpleMessage("Número muito grande!"),
        "kriacoes_agency_module_otp_verification_too_short_msg":
            MessageLookupByLibrary.simpleMessage(
                "O número de telefone fornecido está incorreto. Ele está muito curto para ser um telefone, verifique e tente novamente..."),
        "kriacoes_agency_module_otp_verification_too_short_title":
            MessageLookupByLibrary.simpleMessage("Número muito curto!"),
        "kriacoes_agency_module_otp_verification_we_have_blocked_msg":
            MessageLookupByLibrary.simpleMessage(
                "Bloqueamos todas as solicitações deste dispositivo devido a atividade incomum. Tente mais tarde."),
        "kriacoes_agency_module_otp_verification_we_have_blocked_title":
            MessageLookupByLibrary.simpleMessage("Número bloqueado!"),
        "kriacoes_agency_module_otp_verification_wrong_code":
            MessageLookupByLibrary.simpleMessage(
                "O código informado é inválido!"),
        "kriacoes_agency_module_otp_verification_wrong_size_code":
            MessageLookupByLibrary.simpleMessage(
                "O código precisa ter 6 dígitos!"),
        "languages": MessageLookupByLibrary.simpleMessage("Idiomas"),
        "lets_start_with_login":
            MessageLookupByLibrary.simpleMessage("Seja bem-vindo!"),
        "lets_start_with_register": MessageLookupByLibrary.simpleMessage(
            "Cadastre-se e explore o Divino Menu"),
        "light_mode": MessageLookupByLibrary.simpleMessage("Modo de Claro"),
        "log_out": MessageLookupByLibrary.simpleMessage("Sair"),
        "login": MessageLookupByLibrary.simpleMessage("Entrar"),
        "loginAccountOrCreateNewOneForFree":
            MessageLookupByLibrary.simpleMessage(
                "Entre com sua conta ou cadastre-se"),
        "long_press_to_edit_item_swipe_item_to_delete_it":
            MessageLookupByLibrary.simpleMessage(
                "Pressione e segure para editar o item, deslize o item para excluí-lo"),
        "makeItDefault":
            MessageLookupByLibrary.simpleMessage("Marcar como principal"),
        "maps_explorer":
            MessageLookupByLibrary.simpleMessage("Explore ao seu redor"),
        "market_refreshed_successfuly": MessageLookupByLibrary.simpleMessage(
            "restaurante atualizada com sucesso"),
        "markets_near_to":
            MessageLookupByLibrary.simpleMessage("Restaurantes próximos a "),
        "markets_near_to_your_current_location":
            MessageLookupByLibrary.simpleMessage(
                "Restaurantes próximos a sua localização atual "),
        "markets_results": MessageLookupByLibrary.simpleMessage(
            "Restaurantes próximos a sua localização atual"),
        "mastercard": MessageLookupByLibrary.simpleMessage("Cartão MasterCard"),
        "messages": MessageLookupByLibrary.simpleMessage("Messages"),
        "mi": MessageLookupByLibrary.simpleMessage("mi"),
        "most_popular":
            MessageLookupByLibrary.simpleMessage("restaurantes Mais Populares"),
        "my_orders": MessageLookupByLibrary.simpleMessage("Meus Pedidos"),
        "near_to": MessageLookupByLibrary.simpleMessage("Entregar na:"),
        "near_to_your_current_location": MessageLookupByLibrary.simpleMessage(
            "Restaurantes próximos à sua localização atual"),
        "newMessageFrom":
            MessageLookupByLibrary.simpleMessage("New message from"),
        "new_address_added_successfully": MessageLookupByLibrary.simpleMessage(
            "Novo endereço adicionado com sucesso"),
        "new_order_from_client":
            MessageLookupByLibrary.simpleMessage("Novo pedido do cliente"),
        "notValidAddress":
            MessageLookupByLibrary.simpleMessage("Not valid address"),
        "not_a_valid_address":
            MessageLookupByLibrary.simpleMessage("Endereço inválido"),
        "not_a_valid_biography":
            MessageLookupByLibrary.simpleMessage("Sobre inválido"),
        "not_a_valid_cvc": MessageLookupByLibrary.simpleMessage("CVC inválido"),
        "not_a_valid_date":
            MessageLookupByLibrary.simpleMessage("Data não válida"),
        "not_a_valid_email":
            MessageLookupByLibrary.simpleMessage("Não é um email válido"),
        "not_a_valid_full_name": MessageLookupByLibrary.simpleMessage(
            "Não é um nome completo válido"),
        "not_a_valid_number":
            MessageLookupByLibrary.simpleMessage("Não é um número válido"),
        "not_a_valid_phone":
            MessageLookupByLibrary.simpleMessage("Não é um celular válido"),
        "not_deliverable":
            MessageLookupByLibrary.simpleMessage("Não Disponível"),
        "notificationWasRemoved": MessageLookupByLibrary.simpleMessage(
            "As notificações foram removidas"),
        "notifications": MessageLookupByLibrary.simpleMessage("Notificações"),
        "notifications_refreshed_successfuly":
            MessageLookupByLibrary.simpleMessage(
                "Notificações atualizadas com sucesso"),
        "number": MessageLookupByLibrary.simpleMessage("Número"),
        "oneOrMoreProductsInYourCartNotDeliverable":
            MessageLookupByLibrary.simpleMessage(
                "Um ou mais produtos não está disponíveis para entrega, verifique e tente novamente."),
        "open": MessageLookupByLibrary.simpleMessage("Aberto"),
        "opened_markets":
            MessageLookupByLibrary.simpleMessage("restaurantes abertos"),
        "options": MessageLookupByLibrary.simpleMessage("Compre Junto"),
        "or_checkout_with":
            MessageLookupByLibrary.simpleMessage("Ou finalizar com"),
        "order": MessageLookupByLibrary.simpleMessage("Pedido"),
        "orderDetails":
            MessageLookupByLibrary.simpleMessage("Detalhes do pedido"),
        "orderThisorderidHasBeenCanceled": m0,
        "order_id": MessageLookupByLibrary.simpleMessage("Número do pedido"),
        "order_refreshed_successfuly": MessageLookupByLibrary.simpleMessage(
            "Pedido atualizado com sucesso"),
        "order_status_changed":
            MessageLookupByLibrary.simpleMessage("Status do pedido alterado"),
        "ordered_by_nearby_first": MessageLookupByLibrary.simpleMessage(
            "Classificando por proximidade"),
        "orders_refreshed_successfuly": MessageLookupByLibrary.simpleMessage(
            "Pedidos atualizados com sucesso"),
        "password": MessageLookupByLibrary.simpleMessage("Senha"),
        "pay_on_pickup":
            MessageLookupByLibrary.simpleMessage("Pagar na retirada"),
        "payment_card_updated_successfully":
            MessageLookupByLibrary.simpleMessage(
                "Cartão de pagamento atualizado com sucesso"),
        "payment_mode":
            MessageLookupByLibrary.simpleMessage("Formas de pagamento"),
        "payment_options":
            MessageLookupByLibrary.simpleMessage("Opções de pagamento"),
        "payment_settings":
            MessageLookupByLibrary.simpleMessage("Configurações de pagamento"),
        "payment_settings_updated_successfully":
            MessageLookupByLibrary.simpleMessage(
                "Configurações de pagamento atualizadas com sucesso"),
        "payments_settings":
            MessageLookupByLibrary.simpleMessage("Configurações de pagamentos"),
        "paypal": MessageLookupByLibrary.simpleMessage("PayPal"),
        "paypal_payment":
            MessageLookupByLibrary.simpleMessage("Pagamento PayPal"),
        "pharms_account_login":
            MessageLookupByLibrary.simpleMessage("Acessar minha conta"),
        "pharms_account_singup":
            MessageLookupByLibrary.simpleMessage("Quero me cadastrar"),
        "pharms_alert_part1": MessageLookupByLibrary.simpleMessage(
            "O valor do pedido mínimo não inclui a taxa de entrega.\n"),
        "pharms_alert_part2": MessageLookupByLibrary.simpleMessage(
            "Valor atual do seu carrinho "),
        "pharms_barcode_error": MessageLookupByLibrary.simpleMessage(
            "Falha ao obter a versão da plataforma."),
        "pharms_bottom_navigation_cart":
            MessageLookupByLibrary.simpleMessage("Carrinho"),
        "pharms_bottom_navigation_favorites":
            MessageLookupByLibrary.simpleMessage("Favoritos"),
        "pharms_bottom_navigation_home":
            MessageLookupByLibrary.simpleMessage("Início"),
        "pharms_bottom_navigation_map":
            MessageLookupByLibrary.simpleMessage("Mapa"),
        "pharms_bottom_navigation_orders":
            MessageLookupByLibrary.simpleMessage("Pedidos"),
        "pharms_cart_bottom_address":
            MessageLookupByLibrary.simpleMessage("Definir endereço"),
        "pharms_cart_bottom_address_btn":
            MessageLookupByLibrary.simpleMessage("Definir"),
        "pharms_cart_bottom_address_msg": MessageLookupByLibrary.simpleMessage(
            "Para prosseguir é necessário definir seu endereço atual"),
        "pharms_cart_bottom_pay":
            MessageLookupByLibrary.simpleMessage("Realizar pagamento"),
        "pharms_cart_remove_btncancel":
            MessageLookupByLibrary.simpleMessage("Cancelar"),
        "pharms_cart_remove_btnok":
            MessageLookupByLibrary.simpleMessage("Sim, remover"),
        "pharms_cart_remove_msg": m1,
        "pharms_cart_remove_title":
            MessageLookupByLibrary.simpleMessage("Remover produto?"),
        "pharms_coupon_check": MessageLookupByLibrary.simpleMessage("Aplicar"),
        "pharms_delivery_address":
            MessageLookupByLibrary.simpleMessage("Endereço de entrega: "),
        "pharms_delivery_address_bottom_select":
            MessageLookupByLibrary.simpleMessage(
                "Selecione abaixo o endereço desejado"),
        "pharms_delivery_address_bottom_title":
            MessageLookupByLibrary.simpleMessage(
                "Onde você quer receber o seu pedido?"),
        "pharms_delivery_address_change":
            MessageLookupByLibrary.simpleMessage("Alterar endereço"),
        "pharms_delivery_address_sing": MessageLookupByLibrary.simpleMessage(
            "Declaro que o meu endereço está correto."),
        "pharms_delivery_home":
            MessageLookupByLibrary.simpleMessage("Entrega: "),
        "pharms_delivery_pickup_":
            MessageLookupByLibrary.simpleMessage("Entregar no endereço: "),
        "pharms_delivery_pickup_add": MessageLookupByLibrary.simpleMessage(
            "Por favor informe uma referência"),
        "pharms_delivery_pickup_add_hint":
            MessageLookupByLibrary.simpleMessage("Ponto de Referência"),
        "pharms_delivery_pickup_add_main":
            MessageLookupByLibrary.simpleMessage("Referência: "),
        "pharms_delivery_pickup_confirm_address":
            MessageLookupByLibrary.simpleMessage("Confirme o seu endereço"),
        "pharms_delivery_pickup_refer": MessageLookupByLibrary.simpleMessage(
            "Por favor informe um complemento"),
        "pharms_delivery_pickup_refer_add":
            MessageLookupByLibrary.simpleMessage("Complemento"),
        "pharms_delivery_pickup_refer_hint":
            MessageLookupByLibrary.simpleMessage("Apto / Bloco / Casa"),
        "pharms_delivery_pickup_refer_main":
            MessageLookupByLibrary.simpleMessage("Complemento: "),
        "pharms_details_info":
            MessageLookupByLibrary.simpleMessage("Informações"),
        "pharms_drag_product_remove": MessageLookupByLibrary.simpleMessage(
            "Arraste o produto para a esquerda para remover do seu carrinho"),
        "pharms_for_only": MessageLookupByLibrary.simpleMessage("Por apenas: "),
        "pharms_have_account":
            MessageLookupByLibrary.simpleMessage("Eu já possuo uma conta"),
        "pharms_hint_email":
            MessageLookupByLibrary.simpleMessage("exemplo@divinomenu.com.br"),
        "pharms_hint_login":
            MessageLookupByLibrary.simpleMessage("Acessar minha conta"),
        "pharms_hint_mobile":
            MessageLookupByLibrary.simpleMessage("+55 123456789"),
        "pharms_hint_phone":
            MessageLookupByLibrary.simpleMessage("Número do seu telefone"),
        "pharms_hint_phone_ddi":
            MessageLookupByLibrary.simpleMessage("+55 DDD Número"),
        "pharms_make_the_change_easier":
            MessageLookupByLibrary.simpleMessage("Facilite o troco"),
        "pharms_minimum_title": MessageLookupByLibrary.simpleMessage(
            "Este restaurante possui um valor de pedido mínimo de "),
        "pharms_near_by": MessageLookupByLibrary.simpleMessage("perto de "),
        "pharms_need_change":
            MessageLookupByLibrary.simpleMessage("Precisa de troco?"),
        "pharms_not_have_pharmacies": MessageLookupByLibrary.simpleMessage(
            "- Não há mais Restaurantes próximos -"),
        "pharms_orders_success_products_total":
            MessageLookupByLibrary.simpleMessage("Valor total dos produtos"),
        "pharms_orders_success_total":
            MessageLookupByLibrary.simpleMessage("Total"),
        "pharms_otp_code_expires": MessageLookupByLibrary.simpleMessage(
            "O código expirou, tente novamente!"),
        "pharms_partner_not_found": MessageLookupByLibrary.simpleMessage(
            "Não encontramos estabelecimentos próximos ao endereço inserido. Já estamos trabalhando para em breve você encontrar."),
        "pharms_product_btn":
            MessageLookupByLibrary.simpleMessage("Ver carrinho"),
        "pharms_product_msg": m2,
        "pharms_product_not_found": MessageLookupByLibrary.simpleMessage(
            "Neste momento, não encontramos o que deseja nos Restaurantes próximos. Mas em breve ele estará disponível para compra aqui no aplicativo!"),
        "pharms_product_title": MessageLookupByLibrary.simpleMessage(
            "Produto adicionado ao carrinho!"),
        "pharms_products_delivery":
            MessageLookupByLibrary.simpleMessage("Tx. de Entrega"),
        "pharms_route_error_btn":
            MessageLookupByLibrary.simpleMessage("Atualizar"),
        "pharms_route_error_msg": MessageLookupByLibrary.simpleMessage(
            "Clique para atualizar o aplicativo."),
        "pharms_route_error_title":
            MessageLookupByLibrary.simpleMessage("Ops! Algo deu errado..."),
        "pharms_search_results_barcode": MessageLookupByLibrary.simpleMessage(
            "Leitor de código de barras ao lado"),
        "pharms_search_results_barcode_hint":
            MessageLookupByLibrary.simpleMessage("Código de barras: "),
        "pharms_signup_message": MessageLookupByLibrary.simpleMessage(
            "Explore o máximo do Divino Menu"),
        "pharms_signup_title":
            MessageLookupByLibrary.simpleMessage("Falta Pouco!"),
        "pharms_total":
            MessageLookupByLibrary.simpleMessage("Valor total dos produtos"),
        "pharms_warning_area": MessageLookupByLibrary.simpleMessage(
            "Ei! No momento só estamos atuando na cidade de Niterói-RJ. \nSe você não é de Niterói, este abaixo sou eu indo até a sua cidade, me aguardeee!"),
        "pharms_warning_gps": MessageLookupByLibrary.simpleMessage(
            "Ahh! Quase que eu esqueço! \n Vamos inserir a sua localização?"),
        "pharms_warning_login":
            MessageLookupByLibrary.simpleMessage("Vamos começar?"),
        "pharms_welcome": MessageLookupByLibrary.simpleMessage("Olá, "),
        "pharms_what_value":
            MessageLookupByLibrary.simpleMessage("Troco para qual valor?"),
        "phone": MessageLookupByLibrary.simpleMessage("Celular"),
        "phoneNumber":
            MessageLookupByLibrary.simpleMessage("Número do Celular"),
        "pickup": MessageLookupByLibrary.simpleMessage("Não entregamos"),
        "pickup_your_product_from_the_market":
            MessageLookupByLibrary.simpleMessage(
                "Retire seu produtono restaurante"),
        "productRefreshedSuccessfuly": MessageLookupByLibrary.simpleMessage(
            "Produto atualizado com sucesso"),
        "product_categories":
            MessageLookupByLibrary.simpleMessage("Categorias de Produtos"),
        "products": MessageLookupByLibrary.simpleMessage("Produtos"),
        "products_result": MessageLookupByLibrary.simpleMessage("Produto"),
        "products_results": MessageLookupByLibrary.simpleMessage("Produtos"),
        "profile": MessageLookupByLibrary.simpleMessage("Perfil"),
        "profile_settings":
            MessageLookupByLibrary.simpleMessage("Configurações de perfil"),
        "profile_settings_updated_successfully":
            MessageLookupByLibrary.simpleMessage(
                "Configurações de perfil atualizadas com sucesso"),
        "quantity": MessageLookupByLibrary.simpleMessage("Quantidade"),
        "razorpay": MessageLookupByLibrary.simpleMessage("RazorPay"),
        "razorpayPayment":
            MessageLookupByLibrary.simpleMessage("RazorPay Payment"),
        "recent_orders":
            MessageLookupByLibrary.simpleMessage("Pedidos recentes"),
        "recent_reviews":
            MessageLookupByLibrary.simpleMessage("Comentários recentes"),
        "recents_search":
            MessageLookupByLibrary.simpleMessage("Pesquisa recente"),
        "register": MessageLookupByLibrary.simpleMessage("Cadastrar"),
        "reset": MessageLookupByLibrary.simpleMessage("Limpar"),
        "reset_cart": MessageLookupByLibrary.simpleMessage(
            "Ops! É preciso limpar o carrinho."),
        "reset_your_cart_and_order_meals_form_this_market":
            MessageLookupByLibrary.simpleMessage(
                "Manter os produtos ddeste restaurante"),
        "reviews": MessageLookupByLibrary.simpleMessage("Avaliações"),
        "reviews_refreshed_successfully": MessageLookupByLibrary.simpleMessage(
            "Comentários atualizados com sucesso!"),
        "save": MessageLookupByLibrary.simpleMessage("Salvar"),
        "search": MessageLookupByLibrary.simpleMessage("Procurar"),
        "search_for_markets_or_products": MessageLookupByLibrary.simpleMessage(
            "Digite o nome ou código de barras"),
        "select_options_to_add_them_on_the_product":
            MessageLookupByLibrary.simpleMessage(
                "Escolha também mais produtos."),
        "select_your_preferred_languages": MessageLookupByLibrary.simpleMessage(
            "Selecione seu idioma preferido"),
        "select_your_preferred_payment_mode":
            MessageLookupByLibrary.simpleMessage("Selecione a opção abaixo"),
        "send_password_reset_link": MessageLookupByLibrary.simpleMessage(
            "Enviar link de redefinição de senha"),
        "settings": MessageLookupByLibrary.simpleMessage("Configurações"),
        "shopping": MessageLookupByLibrary.simpleMessage("Carrinho"),
        "shopping_cart":
            MessageLookupByLibrary.simpleMessage("Ver todos os produtos"),
        "should_be_a_valid_email":
            MessageLookupByLibrary.simpleMessage("Deve ser um email válido"),
        "should_be_more_than_3_characters":
            MessageLookupByLibrary.simpleMessage(
                "Deve ter mais de 3 caracteres"),
        "should_be_more_than_3_letters":
            MessageLookupByLibrary.simpleMessage("Deve ter mais de 3 letras"),
        "should_be_more_than_6_letters":
            MessageLookupByLibrary.simpleMessage("Deve ter mais de 6 letras"),
        "signinToChatWithOurManagers": MessageLookupByLibrary.simpleMessage(
            "Sign-In to chat with our managers"),
        "skip": MessageLookupByLibrary.simpleMessage(
            "Voltar para a página inicial"),
        "smsHasBeenSentTo":
            MessageLookupByLibrary.simpleMessage("SMS has been sent to"),
        "start_exploring":
            MessageLookupByLibrary.simpleMessage("Continuar Comprando"),
        "submit": MessageLookupByLibrary.simpleMessage("Enviar"),
        "subtotal": MessageLookupByLibrary.simpleMessage("Subtotal"),
        "swipeLeftTheNotificationToDeleteOrReadUnreadIt":
            MessageLookupByLibrary.simpleMessage(
                "Arraste para o lado para excluir ou marcar como lida"),
        "tapAgainToLeave":
            MessageLookupByLibrary.simpleMessage("Toque novamente para sair"),
        "tax": MessageLookupByLibrary.simpleMessage("Desconto"),
        "tell_us_about_this_market":
            MessageLookupByLibrary.simpleMessage("Escreva sua avaliação aqui"),
        "tell_us_about_this_product":
            MessageLookupByLibrary.simpleMessage("Avalie este produto"),
        "the_address_updated_successfully":
            MessageLookupByLibrary.simpleMessage(
                "O endereço foi atualizado com sucesso"),
        "the_market_has_been_rated_successfully":
            MessageLookupByLibrary.simpleMessage(
                "O restaurante foi classificado com sucesso"),
        "the_product_has_been_rated_successfully":
            MessageLookupByLibrary.simpleMessage(
                "O produto foi classificado com sucesso"),
        "the_product_was_add_from_your_cart": m3,
        "the_product_was_removed_from_your_cart": m4,
        "thisMarketNotSupportDeliveryMethod":
            MessageLookupByLibrary.simpleMessage(
                "Este restaurante não suporta o método de entrega."),
        "thisNotificationHasMarkedAsRead": MessageLookupByLibrary.simpleMessage(
            "Essa notificação foi marcada como lida"),
        "thisNotificationHasMarkedAsUnread":
            MessageLookupByLibrary.simpleMessage(
                "Essa notificação foi marcada como não lida"),
        "thisProductWasAddedToFavorite": MessageLookupByLibrary.simpleMessage(
            "This product was added to favorite"),
        "thisProductWasRemovedFromFavorites":
            MessageLookupByLibrary.simpleMessage(
                "This product was removed from favorites"),
        "this_account_not_exist":
            MessageLookupByLibrary.simpleMessage("Esta conta não existe"),
        "this_email_account_exists": MessageLookupByLibrary.simpleMessage(
            "Esta conta de email já existe"),
        "this_market_is_closed_": MessageLookupByLibrary.simpleMessage(
            "Não é possível continuar pois o estabelecimento está fechado."),
        "this_product_was_added_to_cart": MessageLookupByLibrary.simpleMessage(
            "Este produto foi adicionado aos favoritos"),
        "top_markets":
            MessageLookupByLibrary.simpleMessage("Restaurantes próximos a"),
        "total": MessageLookupByLibrary.simpleMessage("Total"),
        "tracking_order": MessageLookupByLibrary.simpleMessage("Rastreamento"),
        "tracking_refreshed_successfuly": MessageLookupByLibrary.simpleMessage(
            "Rastreamento atualizado com sucesso"),
        "trending_this_week":
            MessageLookupByLibrary.simpleMessage("Tendências desta semana"),
        "typeToStartChat":
            MessageLookupByLibrary.simpleMessage("Type to start chat"),
        "unknown": MessageLookupByLibrary.simpleMessage(
            "Clique aqui e use a sua localização atual"),
        "validCouponCode":
            MessageLookupByLibrary.simpleMessage("validar Cupom"),
        "verify": MessageLookupByLibrary.simpleMessage("Verificar"),
        "verifyPhoneNumber":
            MessageLookupByLibrary.simpleMessage("Verify Phone Number"),
        "verify_your_internet_connection": MessageLookupByLibrary.simpleMessage(
            "Verifique sua conexão com a Internet"),
        "verify_your_quantity_and_click_checkout":
            MessageLookupByLibrary.simpleMessage(
                "Verifique seu pedido e clique em Finalizar"),
        "version": MessageLookupByLibrary.simpleMessage("Versão"),
        "view": MessageLookupByLibrary.simpleMessage("Ver pedido"),
        "viewDetails": MessageLookupByLibrary.simpleMessage("Ver detalhes"),
        "visa_card": MessageLookupByLibrary.simpleMessage("Cartão Visa"),
        "weAreSendingOtpToValidateYourMobileNumberHang":
            MessageLookupByLibrary.simpleMessage(
                "We are sending OTP to validate your mobile number. Hang on!"),
        "welcome": MessageLookupByLibrary.simpleMessage("Bem-vinda(a)"),
        "what_they_say": MessageLookupByLibrary.simpleMessage("Avaliações"),
        "wrong_email_or_password":
            MessageLookupByLibrary.simpleMessage("e-mail ou senha incorretos"),
        "yes": MessageLookupByLibrary.simpleMessage("sim"),
        "youDontHaveAnyConversations": MessageLookupByLibrary.simpleMessage(
            "You don\'t have any conversations"),
        "youDontHaveAnyOrder":
            MessageLookupByLibrary.simpleMessage("Você não tem nenhum pedido"),
        "you_can_discover_markets": MessageLookupByLibrary.simpleMessage(
            "Você pode descobrir restaurantes e produtos ao seu redor e faça o seu pedido e após alguns minutos, nós entregamos para você"),
        "you_must_add_products_of_the_same_markets_choose_one":
            MessageLookupByLibrary.simpleMessage(
                "Você deve adicionar produtos somente de 1 restaurante"),
        "you_must_signin_to_access_to_this_section":
            MessageLookupByLibrary.simpleMessage(
                "Faça login e explore o máximo do Divino Menu"),
        "your_address":
            MessageLookupByLibrary.simpleMessage("Seu endereço completo"),
        "your_biography": MessageLookupByLibrary.simpleMessage("Sobre Você"),
        "your_credit_card_not_valid": MessageLookupByLibrary.simpleMessage(
            "O seu cartão de crédito não é válido"),
        "your_order_has_been_successfully_submitted":
            MessageLookupByLibrary.simpleMessage(
                "Seu pedido foi enviado com sucesso!"),
        "your_reset_link_has_been_sent_to_your_email":
            MessageLookupByLibrary.simpleMessage(
                "Seu link de redefinição foi enviado para seu e-mail")
      };
}
