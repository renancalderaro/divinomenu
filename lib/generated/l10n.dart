// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();

  static S current;

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();

      return S.current;
    });
  }

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Sobre`
  String get about {
    return Intl.message(
      'Sobre',
      name: 'about',
      desc: '',
      args: [],
    );
  }

  /// `Adicionar`
  String get add {
    return Intl.message(
      'Adicionar',
      name: 'add',
      desc: '',
      args: [],
    );
  }

  /// `Adicionar endereço de entrega`
  String get add_delivery_address {
    return Intl.message(
      'Adicionar endereço de entrega',
      name: 'add_delivery_address',
      desc: '',
      args: [],
    );
  }

  /// `Adicionar endereço de entrega`
  String get add_new_delivery_address {
    return Intl.message(
      'Adicionar endereço de entrega',
      name: 'add_new_delivery_address',
      desc: '',
      args: [],
    );
  }

  /// `Comprar`
  String get add_to_cart {
    return Intl.message(
      'Comprar',
      name: 'add_to_cart',
      desc: '',
      args: [],
    );
  }

  /// `Endereço`
  String get address {
    return Intl.message(
      'Endereço',
      name: 'address',
      desc: '',
      args: [],
    );
  }

  /// `Endereço atualizado com sucesso`
  String get addresses_refreshed_successfuly {
    return Intl.message(
      'Endereço atualizado com sucesso',
      name: 'addresses_refreshed_successfuly',
      desc: '',
      args: [],
    );
  }

  /// `Todos`
  String get all {
    return Intl.message(
      'Todos',
      name: 'all',
      desc: '',
      args: [],
    );
  }

  /// `Todos os produtos`
  String get all_product {
    return Intl.message(
      'Todos os produtos',
      name: 'all_product',
      desc: '',
      args: [],
    );
  }

  /// `Idioma`
  String get app_language {
    return Intl.message(
      'Idioma',
      name: 'app_language',
      desc: '',
      args: [],
    );
  }

  /// `Configurações`
  String get app_settings {
    return Intl.message(
      'Configurações',
      name: 'app_settings',
      desc: '',
      args: [],
    );
  }

  /// `Preferências`
  String get application_preferences {
    return Intl.message(
      'Preferências',
      name: 'application_preferences',
      desc: '',
      args: [],
    );
  }

  /// `Aplicar filtros`
  String get apply_filters {
    return Intl.message(
      'Aplicar filtros',
      name: 'apply_filters',
      desc: '',
      args: [],
    );
  }

  /// `Tem certeza de que deseja cancelar este pedido?`
  String get areYouSureYouWantToCancelThisOrder {
    return Intl.message(
      'Tem certeza de que deseja cancelar este pedido?',
      name: 'areYouSureYouWantToCancelThisOrder',
      desc: '',
      args: [],
    );
  }

  /// `Cancelar`
  String get cancel {
    return Intl.message(
      'Cancelar',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Cancelar pedido`
  String get cancelOrder {
    return Intl.message(
      'Cancelar pedido',
      name: 'cancelOrder',
      desc: '',
      args: [],
    );
  }

  /// `Cancelado`
  String get canceled {
    return Intl.message(
      'Cancelado',
      name: 'canceled',
      desc: '',
      args: [],
    );
  }

  /// `Amex - Débito ou Crédito`
  String get card_american_on_delivery {
    return Intl.message(
      'Amex - Débito ou Crédito',
      name: 'card_american_on_delivery',
      desc: '',
      args: [],
    );
  }

  /// `Elo - Débito ou Crédito`
  String get card_elo_on_delivery {
    return Intl.message(
      'Elo - Débito ou Crédito',
      name: 'card_elo_on_delivery',
      desc: '',
      args: [],
    );
  }

  /// `Hiper - Débito ou Crédito`
  String get card_hiper_on_delivery {
    return Intl.message(
      'Hiper - Débito ou Crédito',
      name: 'card_hiper_on_delivery',
      desc: '',
      args: [],
    );
  }

  /// `MasterCard - Débito ou Crédito`
  String get card_masterd_on_delivery {
    return Intl.message(
      'MasterCard - Débito ou Crédito',
      name: 'card_masterd_on_delivery',
      desc: '',
      args: [],
    );
  }

  /// `NÚMERO DO CARTÃO`
  String get card_number {
    return Intl.message(
      'NÚMERO DO CARTÃO',
      name: 'card_number',
      desc: '',
      args: [],
    );
  }

  /// `Cartão na entrega`
  String get card_text_on_delivery {
    return Intl.message(
      'Cartão na entrega',
      name: 'card_text_on_delivery',
      desc: '',
      args: [],
    );
  }

  /// `Visa - Débito ou Crédito`
  String get card_visad_on_delivery {
    return Intl.message(
      'Visa - Débito ou Crédito',
      name: 'card_visad_on_delivery',
      desc: '',
      args: [],
    );
  }

  /// `Carrinho`
  String get cart {
    return Intl.message(
      'Carrinho',
      name: 'cart',
      desc: '',
      args: [],
    );
  }

  /// `Carrinho atualizado com sucesso`
  String get carts_refreshed_successfuly {
    return Intl.message(
      'Carrinho atualizado com sucesso',
      name: 'carts_refreshed_successfuly',
      desc: '',
      args: [],
    );
  }

  /// `Dinheiro na entrega`
  String get cash_on_delivery {
    return Intl.message(
      'Dinheiro na entrega',
      name: 'cash_on_delivery',
      desc: '',
      args: [],
    );
  }

  /// `Categoria`
  String get category {
    return Intl.message(
      'Categoria',
      name: 'category',
      desc: '',
      args: [],
    );
  }

  /// `Categoria atualizada com sucesso`
  String get category_refreshed_successfuly {
    return Intl.message(
      'Categoria atualizada com sucesso',
      name: 'category_refreshed_successfuly',
      desc: '',
      args: [],
    );
  }

  /// `Finalizar`
  String get checkout {
    return Intl.message(
      'Finalizar',
      name: 'checkout',
      desc: '',
      args: [],
    );
  }

  /// `Limpar`
  String get clear {
    return Intl.message(
      'Limpar',
      name: 'clear',
      desc: '',
      args: [],
    );
  }

  /// `Clique no produto para ver mais sobre ele`
  String get clickOnTheProductToGetMoreDetailsAboutIt {
    return Intl.message(
      'Clique no produto para ver mais sobre ele',
      name: 'clickOnTheProductToGetMoreDetailsAboutIt',
      desc: '',
      args: [],
    );
  }

  /// `Clique para pagar com RazorPay`
  String get clickToPayWithRazorpayMethod {
    return Intl.message(
      'Clique para pagar com RazorPay',
      name: 'clickToPayWithRazorpayMethod',
      desc: '',
      args: [],
    );
  }

  /// `Clique nas estrelas abaixo para comentar`
  String get click_on_the_stars_below_to_leave_comments {
    return Intl.message(
      'Clique nas estrelas abaixo para comentar',
      name: 'click_on_the_stars_below_to_leave_comments',
      desc: '',
      args: [],
    );
  }

  /// `Clique para confirmar seu endereço e pagar ou pressione e segure para editar seu endereço`
  String get click_to_confirm_your_address_and_pay_or_long_press {
    return Intl.message(
      'Clique para confirmar seu endereço e pagar ou pressione e segure para editar seu endereço',
      name: 'click_to_confirm_your_address_and_pay_or_long_press',
      desc: '',
      args: [],
    );
  }

  /// `Clique para pagar em dinheiro na entrega`
  String get click_to_pay_cash_on_delivery {
    return Intl.message(
      'Clique para pagar em dinheiro na entrega',
      name: 'click_to_pay_cash_on_delivery',
      desc: '',
      args: [],
    );
  }

  /// `Clique para pagar quando retirar o produto`
  String get click_to_pay_on_pickup {
    return Intl.message(
      'Clique para pagar quando retirar o produto',
      name: 'click_to_pay_on_pickup',
      desc: '',
      args: [],
    );
  }

  /// `Clique para pagar com seu MasterCard`
  String get click_to_pay_with_your_mastercard {
    return Intl.message(
      'Clique para pagar com seu MasterCard',
      name: 'click_to_pay_with_your_mastercard',
      desc: '',
      args: [],
    );
  }

  /// `Clique para pagar com sua conta do PayPal`
  String get click_to_pay_with_your_paypal_account {
    return Intl.message(
      'Clique para pagar com sua conta do PayPal',
      name: 'click_to_pay_with_your_paypal_account',
      desc: '',
      args: [],
    );
  }

  /// `Clique para pagar com seu cartão Visa`
  String get click_to_pay_with_your_visa_card {
    return Intl.message(
      'Clique para pagar com seu cartão Visa',
      name: 'click_to_pay_with_your_visa_card',
      desc: '',
      args: [],
    );
  }

  /// `Fechar`
  String get close {
    return Intl.message(
      'Fechar',
      name: 'close',
      desc: '',
      args: [],
    );
  }

  /// `Fechada`
  String get closed {
    return Intl.message(
      'Fechado',
      name: 'closed',
      desc: '',
      args: [],
    );
  }

  /// `Insira o seu telefone para continuar`
  String get completeYourProfileDetailsToContinue {
    return Intl.message(
      'Insira o seu telefone para continuar',
      name: 'completeYourProfileDetailsToContinue',
      desc: '',
      args: [],
    );
  }

  /// `Confirme o pagamento`
  String get confirm_payment {
    return Intl.message(
      'Confirme o pagamento',
      name: 'confirm_payment',
      desc: '',
      args: [],
    );
  }

  /// `Confirme seu endereço de entrega`
  String get confirm_your_delivery_address {
    return Intl.message(
      'Confirme seu endereço de entrega',
      name: 'confirm_your_delivery_address',
      desc: '',
      args: [],
    );
  }

  /// `Confirmação`
  String get confirmation {
    return Intl.message(
      'Confirmação',
      name: 'confirmation',
      desc: '',
      args: [],
    );
  }

  /// `Localização atual`
  String get current_location {
    return Intl.message(
      'Localização atual',
      name: 'current_location',
      desc: '',
      args: [],
    );
  }

  /// `CVC`
  String get cvc {
    return Intl.message(
      'CVC',
      name: 'cvc',
      desc: '',
      args: [],
    );
  }

  /// `CVV`
  String get cvv {
    return Intl.message(
      'CVV',
      name: 'cvv',
      desc: '',
      args: [],
    );
  }

  /// `Modo escuro`
  String get dark_mode {
    return Intl.message(
      'Modo escuro',
      name: 'dark_mode',
      desc: '',
      args: [],
    );
  }

  /// `Cartão de crédito padrão`
  String get default_credit_card {
    return Intl.message(
      'Cartão de crédito padrão',
      name: 'default_credit_card',
      desc: '',
      args: [],
    );
  }

  /// `Pronta entrega`
  String get deliverable {
    return Intl.message(
      'Disponível',
      name: 'deliverable',
      desc: '',
      args: [],
    );
  }

  /// `Entrega`
  String get delivery {
    return Intl.message(
      'Entrega',
      name: 'delivery',
      desc: '',
      args: [],
    );
  }

  /// `Este restaurante não faz entrega no endereço escolhido ou algum produto não está disponível em estoque.`
  String get deliveryAddressOutsideTheDeliveryRangeOfThisMarkets {
    return Intl.message(
      'Este restaurante não faz entrega no endereço escolhido ou algum produto não está disponível em estoque.',
      name: 'deliveryAddressOutsideTheDeliveryRangeOfThisMarkets',
      desc: '',
      args: [],
    );
  }

  /// `Não permitida!`
  String get deliveryMethodNotAllowed {
    return Intl.message(
      'Não permitida!',
      name: 'deliveryMethodNotAllowed',
      desc: '',
      args: [],
    );
  }

  /// `Endereço de entrega`
  String get delivery_address {
    return Intl.message(
      'Endereço de entrega',
      name: 'delivery_address',
      desc: '',
      args: [],
    );
  }

  /// `Endereço de entrega removido com sucesso!`
  String get delivery_address_removed_successfully {
    return Intl.message(
      'Endereço de entrega removido com sucesso!',
      name: 'delivery_address_removed_successfully',
      desc: '',
      args: [],
    );
  }

  /// `Endereços de entrega`
  String get delivery_addresses {
    return Intl.message(
      'Endereços de entrega',
      name: 'delivery_addresses',
      desc: '',
      args: [],
    );
  }

  /// `Taxa de entrega`
  String get delivery_fee {
    return Intl.message(
      'Taxa de entrega',
      name: 'delivery_fee',
      desc: '',
      args: [],
    );
  }

  /// `Entrega`
  String get delivery_or_pickup {
    return Intl.message(
      'Entrega',
      name: 'delivery_or_pickup',
      desc: '',
      args: [],
    );
  }

  /// `Descrição`
  String get description {
    return Intl.message(
      'Descrição',
      name: 'description',
      desc: '',
      args: [],
    );
  }

  /// `Detalhes`
  String get details {
    return Intl.message(
      'Detalhes',
      name: 'details',
      desc: '',
      args: [],
    );
  }

  /// `Explorar`
  String get discover__explorer {
    return Intl.message(
      'Explorar',
      name: 'discover__explorer',
      desc: '',
      args: [],
    );
  }

  /// `Sem notificações`
  String get dont_have_any_item_in_the_notification_list {
    return Intl.message(
      'Sem notificações',
      name: 'dont_have_any_item_in_the_notification_list',
      desc: '',
      args: [],
    );
  }

  /// `Não tem nenhum item no seu carrinho`
  String get dont_have_any_item_in_your_cart {
    return Intl.message(
      'Não tem nenhum item no seu carrinho',
      name: 'dont_have_any_item_in_your_cart',
      desc: '',
      args: [],
    );
  }

  /// `Editar`
  String get edit {
    return Intl.message(
      'Editar',
      name: 'edit',
      desc: '',
      args: [],
    );
  }

  /// `Endereço de e-mail`
  String get email {
    return Intl.message(
      'Endereço de e-mail',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `Endereço de e-mail`
  String get email_address {
    return Intl.message(
      'Endereço de e-mail',
      name: 'email_address',
      desc: '',
      args: [],
    );
  }

  /// `Digite o seu e-mail`
  String get email_to_reset_password {
    return Intl.message(
      'Digite o seu e-mail',
      name: 'email_to_reset_password',
      desc: '',
      args: [],
    );
  }

  /// `Inglês`
  String get english {
    return Intl.message(
      'Inglês',
      name: 'english',
      desc: '',
      args: [],
    );
  }

  /// `Erro! Verificar configurações de email`
  String get error_verify_email_settings {
    return Intl.message(
      'Erro! Verificar configurações de email',
      name: 'error_verify_email_settings',
      desc: '',
      args: [],
    );
  }

  /// `Data de validade`
  String get exp_date {
    return Intl.message(
      'Data de validade',
      name: 'exp_date',
      desc: '',
      args: [],
    );
  }

  /// `DATA DE VALIDADE`
  String get expiry_date {
    return Intl.message(
      'DATA DE VALIDADE',
      name: 'expiry_date',
      desc: '',
      args: [],
    );
  }

  /// `Perguntas frequentes`
  String get faq {
    return Intl.message(
      'Perguntas frequentes',
      name: 'faq',
      desc: '',
      args: [],
    );
  }

  /// `Faqs atualizadas com sucesso`
  String get faqsRefreshedSuccessfuly {
    return Intl.message(
      'Faqs atualizadas com sucesso',
      name: 'faqsRefreshedSuccessfuly',
      desc: '',
      args: [],
    );
  }

  /// `Produtos Favoritos`
  String get favorite_products {
    return Intl.message(
      'Produtos Favoritos',
      name: 'favorite_products',
      desc: '',
      args: [],
    );
  }

  /// `Favoritos`
  String get favorites {
    return Intl.message(
      'Favoritos',
      name: 'favorites',
      desc: '',
      args: [],
    );
  }

  /// `Favoritos atualizado com sucesso`
  String get favorites_refreshed_successfuly {
    return Intl.message(
      'Favoritos atualizado com sucesso',
      name: 'favorites_refreshed_successfuly',
      desc: '',
      args: [],
    );
  }

  /// `Recomendados`
  String get featured_products {
    return Intl.message(
      'Recomendados',
      name: 'featured_products',
      desc: '',
      args: [],
    );
  }

  /// `Categorias de restaurantes`
  String get fields {
    return Intl.message(
      'Categorias de restaurantes',
      name: 'fields',
      desc: '',
      args: [],
    );
  }

  /// `Filtros`
  String get filter {
    return Intl.message(
      'Filtros',
      name: 'filter',
      desc: '',
      args: [],
    );
  }

  /// `For more details, please chat with our managers`
  String get forMoreDetailsPleaseChatWithOurManagers {
    return Intl.message(
      'For more details, please chat with our managers',
      name: 'forMoreDetailsPleaseChatWithOurManagers',
      desc: '',
      args: [],
    );
  }

  /// `Grátis`
  String get free {
    return Intl.message(
      'Grátis',
      name: 'free',
      desc: '',
      args: [],
    );
  }

  /// `Insira a rua, número, complementos, bairro e cidade`
  String get full_address {
    return Intl.message(
      'Insira a rua, número, complementos, bairro e cidade',
      name: 'full_address',
      desc: '',
      args: [],
    );
  }

  /// `Nome completo`
  String get full_name {
    return Intl.message(
      'Nome completo',
      name: 'full_name',
      desc: '',
      args: [],
    );
  }

  /// `Clique aqui para entrar`
  String get guest {
    return Intl.message(
      'Clique aqui para entrar',
      name: 'guest',
      desc: '',
      args: [],
    );
  }

  /// `Você tem um cupom?`
  String get haveCouponCode {
    return Intl.message(
      'Você tem um cupom?',
      name: 'haveCouponCode',
      desc: '',
      args: [],
    );
  }

  /// `Ajuda e Suporte`
  String get help__support {
    return Intl.message(
      'Ajuda e Suporte',
      name: 'help__support',
      desc: '',
      args: [],
    );
  }

  /// `Ajuda e Suporte`
  String get help_support {
    return Intl.message(
      'Ajuda e Suporte',
      name: 'help_support',
      desc: '',
      args: [],
    );
  }

  /// `Ajuda e Suporte`
  String get help_supports {
    return Intl.message(
      'Ajuda e Suporte',
      name: 'help_supports',
      desc: '',
      args: [],
    );
  }

  /// `Rua, bairro, cidade, país`
  String get hint_full_address {
    return Intl.message(
      'Rua, bairro, cidade, país',
      name: 'hint_full_address',
      desc: '',
      args: [],
    );
  }

  /// `Início`
  String get home {
    return Intl.message(
      'Início',
      name: 'home',
      desc: '',
      args: [],
    );
  }

  /// `Endereço`
  String get home_address {
    return Intl.message(
      'Endereço',
      name: 'home_address',
      desc: '',
      args: [],
    );
  }

  /// `Como você classificaria este restaurante?`
  String get how_would_you_rate_this_market {
    return Intl.message(
      'Como você classificaria este restaurante?',
      name: 'how_would_you_rate_this_market',
      desc: '',
      args: [],
    );
  }

  /// `Como você classificaria este restaurante?`
  String get how_would_you_rate_this_market_ {
    return Intl.message(
      'Como você classificaria este restaurante?',
      name: 'how_would_you_rate_this_market_',
      desc: '',
      args: [],
    );
  }

  /// `Eu não tenho uma conta`
  String get i_dont_have_an_account {
    return Intl.message(
      'Eu não tenho uma conta',
      name: 'i_dont_have_an_account',
      desc: '',
      args: [],
    );
  }

  /// `Eu não lembro a minha senha :(`
  String get i_forgot_password {
    return Intl.message(
      'Eu não lembro a minha senha :(',
      name: 'i_forgot_password',
      desc: '',
      args: [],
    );
  }

  /// `Eu já tenho conta`
  String get i_have_account_back_to_login {
    return Intl.message(
      'Eu já tenho conta',
      name: 'i_have_account_back_to_login',
      desc: '',
      args: [],
    );
  }

  /// `Lembrei a minha senha`
  String get i_remember_my_password_return_to_login {
    return Intl.message(
      'Lembrei a minha senha',
      name: 'i_remember_my_password_return_to_login',
      desc: '',
      args: [],
    );
  }

  /// `Sobre`
  String get information {
    return Intl.message(
      'Sobre',
      name: 'information',
      desc: '',
      args: [],
    );
  }

  /// `Cupom Invalido`
  String get invalidCouponCode {
    return Intl.message(
      'Cupom Invalido',
      name: 'invalidCouponCode',
      desc: '',
      args: [],
    );
  }

  /// `Itens`
  String get items {
    return Intl.message(
      'Itens',
      name: 'items',
      desc: '',
      args: [],
    );
  }

  /// `Divino Menu`
  String get john_doe {
    return Intl.message(
      'Divino Menu',
      name: 'john_doe',
      desc: '',
      args: [],
    );
  }

  /// `Manter os produtos ddeste restaurante`
  String get keep_your_old_meals_of_this_market {
    return Intl.message(
      'Manter os produtos ddeste restaurante',
      name: 'keep_your_old_meals_of_this_market',
      desc: '',
      args: [],
    );
  }

  /// `Km`
  String get km {
    return Intl.message(
      'Km',
      name: 'km',
      desc: '',
      args: [],
    );
  }

  /// `Verificar`
  String get kriacoes_agency_module_otp_verification_btn_check {
    return Intl.message(
      'Verificar',
      name: 'kriacoes_agency_module_otp_verification_btn_check',
      desc: '',
      args: [],
    );
  }

  /// `Continuar`
  String get kriacoes_agency_module_otp_verification_btn_signup {
    return Intl.message(
      'Continuar',
      name: 'kriacoes_agency_module_otp_verification_btn_signup',
      desc: '',
      args: [],
    );
  }

  /// `O código não pode ficar em branco!`
  String
      get kriacoes_agency_module_otp_verification_cannot_create_phoneauthcredential {
    return Intl.message(
      'O código não pode ficar em branco!',
      name:
          'kriacoes_agency_module_otp_verification_cannot_create_phoneauthcredential',
      desc: '',
      args: [],
    );
  }

  /// `O seu telefone`
  String get kriacoes_agency_module_otp_verification_change_number_msg_1 {
    return Intl.message(
      'O seu telefone',
      name: 'kriacoes_agency_module_otp_verification_change_number_msg_1',
      desc: '',
      args: [],
    );
  }

  /// `foi verificado, para alterar seu número de telefone você deverá fazer a verificação novamente, gostaria de alterar?`
  String get kriacoes_agency_module_otp_verification_change_number_msg_2 {
    return Intl.message(
      'foi verificado, para alterar seu número de telefone você deverá fazer a verificação novamente, gostaria de alterar?',
      name: 'kriacoes_agency_module_otp_verification_change_number_msg_2',
      desc: '',
      args: [],
    );
  }

  /// `Não`
  String get kriacoes_agency_module_otp_verification_change_number_no {
    return Intl.message(
      'Não',
      name: 'kriacoes_agency_module_otp_verification_change_number_no',
      desc: '',
      args: [],
    );
  }

  /// `Alterar Telefone`
  String get kriacoes_agency_module_otp_verification_change_number_title {
    return Intl.message(
      'Alterar Telefone',
      name: 'kriacoes_agency_module_otp_verification_change_number_title',
      desc: '',
      args: [],
    );
  }

  /// `Alterar`
  String get kriacoes_agency_module_otp_verification_change_number_yes {
    return Intl.message(
      'Alterar',
      name: 'kriacoes_agency_module_otp_verification_change_number_yes',
      desc: '',
      args: [],
    );
  }

  /// `Escolha seu país`
  String get kriacoes_agency_module_otp_verification_choose_country {
    return Intl.message(
      'Escolha seu país',
      name: 'kriacoes_agency_module_otp_verification_choose_country',
      desc: '',
      args: [],
    );
  }

  /// `Último escolhido`
  String get kriacoes_agency_module_otp_verification_choose_country_last {
    return Intl.message(
      'Último escolhido',
      name: 'kriacoes_agency_module_otp_verification_choose_country_last',
      desc: '',
      args: [],
    );
  }

  /// `Digite o nome do país`
  String get kriacoes_agency_module_otp_verification_choose_country_name {
    return Intl.message(
      'Digite o nome do país',
      name: 'kriacoes_agency_module_otp_verification_choose_country_name',
      desc: '',
      args: [],
    );
  }

  /// `Pesquisar`
  String get kriacoes_agency_module_otp_verification_choose_country_search {
    return Intl.message(
      'Pesquisar',
      name: 'kriacoes_agency_module_otp_verification_choose_country_search',
      desc: '',
      args: [],
    );
  }

  /// `O telefone não pode ficar vazio!`
  String get kriacoes_agency_module_otp_verification_empty {
    return Intl.message(
      'O telefone não pode ficar vazio!',
      name: 'kriacoes_agency_module_otp_verification_empty',
      desc: '',
      args: [],
    );
  }

  /// `O código não pode ficar vazio!`
  String get kriacoes_agency_module_otp_verification_empty_code {
    return Intl.message(
      'O código não pode ficar vazio!',
      name: 'kriacoes_agency_module_otp_verification_empty_code',
      desc: '',
      args: [],
    );
  }

  /// `O código informado é inválido!`
  String
      get kriacoes_agency_module_otp_verification_error_invalid_verification_code {
    return Intl.message(
      'O código informado é inválido!',
      name:
          'kriacoes_agency_module_otp_verification_error_invalid_verification_code',
      desc: '',
      args: [],
    );
  }

  /// `O código enviando expirou, solicite um novo código...`
  String get kriacoes_agency_module_otp_verification_error_session_expired {
    return Intl.message(
      'O código enviando expirou, solicite um novo código...',
      name: 'kriacoes_agency_module_otp_verification_error_session_expired',
      desc: '',
      args: [],
    );
  }

  /// `Ocorreu um erro: `
  String get kriacoes_agency_module_otp_verification_generic_msg {
    return Intl.message(
      'Ocorreu um erro: ',
      name: 'kriacoes_agency_module_otp_verification_generic_msg',
      desc: '',
      args: [],
    );
  }

  /// `Opss...`
  String get kriacoes_agency_module_otp_verification_generic_title {
    return Intl.message(
      'Opss...',
      name: 'kriacoes_agency_module_otp_verification_generic_title',
      desc: '',
      args: [],
    );
  }

  /// `Informe o código`
  String get kriacoes_agency_module_otp_verification_give_the_code {
    return Intl.message(
      'Informe o código',
      name: 'kriacoes_agency_module_otp_verification_give_the_code',
      desc: '',
      args: [],
    );
  }

  /// `Insira seu n° de celular`
  String get kriacoes_agency_module_otp_verification_header {
    return Intl.message(
      'Insira seu n° de celular',
      name: 'kriacoes_agency_module_otp_verification_header',
      desc: '',
      args: [],
    );
  }

  /// `(xx) xxxxx-xxxx`
  String get kriacoes_agency_module_otp_verification_hint {
    return Intl.message(
      '(xx) xxxxx-xxxx',
      name: 'kriacoes_agency_module_otp_verification_hint',
      desc: '',
      args: [],
    );
  }

  /// `O telefone informado não está correto, informe o código do país, o prefixo da operadora e seu número`
  String get kriacoes_agency_module_otp_verification_incorrect {
    return Intl.message(
      'O telefone informado não está correto, informe o código do país, o prefixo da operadora e seu número',
      name: 'kriacoes_agency_module_otp_verification_incorrect',
      desc: '',
      args: [],
    );
  }

  /// `O formato do número de telefone fornecido está incorreto. Insira o número de telefone: [número do assinante incluindo código de área].`
  String get kriacoes_agency_module_otp_verification_invalid_format_msg {
    return Intl.message(
      'O formato do número de telefone fornecido está incorreto. Insira o número de telefone: [número do assinante incluindo código de área].',
      name: 'kriacoes_agency_module_otp_verification_invalid_format_msg',
      desc: '',
      args: [],
    );
  }

  /// `Formato inválido!`
  String get kriacoes_agency_module_otp_verification_invalid_format_title {
    return Intl.message(
      'Formato inválido!',
      name: 'kriacoes_agency_module_otp_verification_invalid_format_title',
      desc: '',
      args: [],
    );
  }

  /// `Ocorreu um erro com a rede, por favor tente novamente.`
  String get kriacoes_agency_module_otp_verification_network_msg {
    return Intl.message(
      'Ocorreu um erro com a rede, por favor tente novamente.',
      name: 'kriacoes_agency_module_otp_verification_network_msg',
      desc: '',
      args: [],
    );
  }

  /// `Erro de conexão!`
  String get kriacoes_agency_module_otp_verification_network_title {
    return Intl.message(
      'Erro de conexão!',
      name: 'kriacoes_agency_module_otp_verification_network_title',
      desc: '',
      args: [],
    );
  }

  /// `Este aplicativo não está autorizado, por favor verifique as credenciais do firebase.`
  String get kriacoes_agency_module_otp_verification_not_authorized_msg {
    return Intl.message(
      'Este aplicativo não está autorizado, por favor verifique as credenciais do firebase.',
      name: 'kriacoes_agency_module_otp_verification_not_authorized_msg',
      desc: '',
      args: [],
    );
  }

  /// `Não autorizado!`
  String get kriacoes_agency_module_otp_verification_not_authorized_title {
    return Intl.message(
      'Não autorizado!',
      name: 'kriacoes_agency_module_otp_verification_not_authorized_title',
      desc: '',
      args: [],
    );
  }

  /// `Telefone`
  String get kriacoes_agency_module_otp_verification_phone {
    return Intl.message(
      'Telefone',
      name: 'kriacoes_agency_module_otp_verification_phone',
      desc: '',
      args: [],
    );
  }

  /// `Telefone confirmado!`
  String get kriacoes_agency_module_otp_verification_phone_confirmed_header {
    return Intl.message(
      'Telefone confirmado!',
      name: 'kriacoes_agency_module_otp_verification_phone_confirmed_header',
      desc: '',
      args: [],
    );
  }

  /// `Seu número de telefone foi confirmado com sucesso, agora conclua seu cadastro.`
  String get kriacoes_agency_module_otp_verification_phone_signup_step {
    return Intl.message(
      'Seu número de telefone foi confirmado com sucesso, agora conclua seu cadastro.',
      name: 'kriacoes_agency_module_otp_verification_phone_signup_step',
      desc: '',
      args: [],
    );
  }

  /// `Enviar Código`
  String get kriacoes_agency_module_otp_verification_send_code {
    return Intl.message(
      'Enviar Código',
      name: 'kriacoes_agency_module_otp_verification_send_code',
      desc: '',
      args: [],
    );
  }

  /// `Sms enviado para`
  String get kriacoes_agency_module_otp_verification_sms_send_code {
    return Intl.message(
      'Sms enviado para',
      name: 'kriacoes_agency_module_otp_verification_sms_send_code',
      desc: '',
      args: [],
    );
  }

  /// `Enviaremos um código SMS para o número`
  String get kriacoes_agency_module_otp_verification_sub_header {
    return Intl.message(
      'Enviaremos um código SMS para o número',
      name: 'kriacoes_agency_module_otp_verification_sub_header',
      desc: '',
      args: [],
    );
  }

  /// `O número de telefone fornecido está incorreto. Ele está muito longo para ser um telefone, verifique e tente novamente...`
  String get kriacoes_agency_module_otp_verification_too_long_msg {
    return Intl.message(
      'O número de telefone fornecido está incorreto. Ele está muito longo para ser um telefone, verifique e tente novamente...',
      name: 'kriacoes_agency_module_otp_verification_too_long_msg',
      desc: '',
      args: [],
    );
  }

  /// `Número muito grande!`
  String get kriacoes_agency_module_otp_verification_too_long_title {
    return Intl.message(
      'Número muito grande!',
      name: 'kriacoes_agency_module_otp_verification_too_long_title',
      desc: '',
      args: [],
    );
  }

  /// `O número de telefone fornecido está incorreto. Ele está muito curto para ser um telefone, verifique e tente novamente...`
  String get kriacoes_agency_module_otp_verification_too_short_msg {
    return Intl.message(
      'O número de telefone fornecido está incorreto. Ele está muito curto para ser um telefone, verifique e tente novamente...',
      name: 'kriacoes_agency_module_otp_verification_too_short_msg',
      desc: '',
      args: [],
    );
  }

  /// `Número muito curto!`
  String get kriacoes_agency_module_otp_verification_too_short_title {
    return Intl.message(
      'Número muito curto!',
      name: 'kriacoes_agency_module_otp_verification_too_short_title',
      desc: '',
      args: [],
    );
  }

  /// `Bloqueamos todas as solicitações deste dispositivo devido a atividade incomum. Tente mais tarde.`
  String get kriacoes_agency_module_otp_verification_we_have_blocked_msg {
    return Intl.message(
      'Bloqueamos todas as solicitações deste dispositivo devido a atividade incomum. Tente mais tarde.',
      name: 'kriacoes_agency_module_otp_verification_we_have_blocked_msg',
      desc: '',
      args: [],
    );
  }

  /// `Número bloqueado!`
  String get kriacoes_agency_module_otp_verification_we_have_blocked_title {
    return Intl.message(
      'Número bloqueado!',
      name: 'kriacoes_agency_module_otp_verification_we_have_blocked_title',
      desc: '',
      args: [],
    );
  }

  /// `O código informado é inválido!`
  String get kriacoes_agency_module_otp_verification_wrong_code {
    return Intl.message(
      'O código informado é inválido!',
      name: 'kriacoes_agency_module_otp_verification_wrong_code',
      desc: '',
      args: [],
    );
  }

  /// `O código precisa ter 6 dígitos!`
  String get kriacoes_agency_module_otp_verification_wrong_size_code {
    return Intl.message(
      'O código precisa ter 6 dígitos!',
      name: 'kriacoes_agency_module_otp_verification_wrong_size_code',
      desc: '',
      args: [],
    );
  }

  /// `Idiomas`
  String get languages {
    return Intl.message(
      'Idiomas',
      name: 'languages',
      desc: '',
      args: [],
    );
  }

  /// `Seja bem-vindo!`
  String get lets_start_with_login {
    return Intl.message(
      'Seja bem-vindo!',
      name: 'lets_start_with_login',
      desc: '',
      args: [],
    );
  }

  /// `Cadastre-se e explore o Divino Menu`
  String get lets_start_with_register {
    return Intl.message(
      'Cadastre-se e explore o Divino Menu',
      name: 'lets_start_with_register',
      desc: '',
      args: [],
    );
  }

  /// `Modo de Claro`
  String get light_mode {
    return Intl.message(
      'Modo de Claro',
      name: 'light_mode',
      desc: '',
      args: [],
    );
  }

  /// `Sair`
  String get log_out {
    return Intl.message(
      'Sair',
      name: 'log_out',
      desc: '',
      args: [],
    );
  }

  /// `Entrar`
  String get login {
    return Intl.message(
      'Entrar',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Login account or create new one for free`
  String get loginAccountOrCreateNewOneForFree {
    return Intl.message(
      'Entre com sua conta ou cadastre-se',
      name: 'loginAccountOrCreateNewOneForFree',
      desc: '',
      args: [],
    );
  }

  /// `Pressione e segure para editar o item, deslize o item para excluí-lo`
  String get long_press_to_edit_item_swipe_item_to_delete_it {
    return Intl.message(
      'Pressione e segure para editar o item, deslize o item para excluí-lo',
      name: 'long_press_to_edit_item_swipe_item_to_delete_it',
      desc: '',
      args: [],
    );
  }

  /// `Marcar como principal`
  String get makeItDefault {
    return Intl.message(
      'Marcar como principal',
      name: 'makeItDefault',
      desc: '',
      args: [],
    );
  }

  /// `Explore ao seu redor`
  String get maps_explorer {
    return Intl.message(
      'Explore ao seu redor',
      name: 'maps_explorer',
      desc: '',
      args: [],
    );
  }

  /// `restaurante atualizadp com sucesso`
  String get market_refreshed_successfuly {
    return Intl.message(
      'restaurante atualizado com sucesso',
      name: 'market_refreshed_successfuly',
      desc: '',
      args: [],
    );
  }

  /// `Restaurantes próximos a `
  String get markets_near_to {
    return Intl.message(
      'Restaurantes próximos a ',
      name: 'markets_near_to',
      desc: '',
      args: [],
    );
  }

  /// `Restaurantes próximos a sua localização atual `
  String get markets_near_to_your_current_location {
    return Intl.message(
      'Restaurantes próximos a sua localização atual ',
      name: 'markets_near_to_your_current_location',
      desc: '',
      args: [],
    );
  }

  /// `Restaurantes próximos a sua localização atual`
  String get markets_results {
    return Intl.message(
      'Restaurantes próximos a sua localização atual',
      name: 'markets_results',
      desc: '',
      args: [],
    );
  }

  /// `Cartão MasterCard`
  String get mastercard {
    return Intl.message(
      'Cartão MasterCard',
      name: 'mastercard',
      desc: '',
      args: [],
    );
  }

  /// `Messages`
  String get messages {
    return Intl.message(
      'Messages',
      name: 'messages',
      desc: '',
      args: [],
    );
  }

  /// `mi`
  String get mi {
    return Intl.message(
      'mi',
      name: 'mi',
      desc: '',
      args: [],
    );
  }

  /// `restaurantes Mais Populares`
  String get most_popular {
    return Intl.message(
      'restaurantes Mais Populares',
      name: 'most_popular',
      desc: '',
      args: [],
    );
  }

  /// `Meus Pedidos`
  String get my_orders {
    return Intl.message(
      'Meus Pedidos',
      name: 'my_orders',
      desc: '',
      args: [],
    );
  }

  /// `Entregar na:`
  String get near_to {
    return Intl.message(
      'Entregar na:',
      name: 'near_to',
      desc: '',
      args: [],
    );
  }

  /// `Restaurantes próximos à sua localização atual`
  String get near_to_your_current_location {
    return Intl.message(
      'Restaurantes próximos à sua localização atual',
      name: 'near_to_your_current_location',
      desc: '',
      args: [],
    );
  }

  /// `New message from`
  String get newMessageFrom {
    return Intl.message(
      'New message from',
      name: 'newMessageFrom',
      desc: '',
      args: [],
    );
  }

  /// `Novo endereço adicionado com sucesso`
  String get new_address_added_successfully {
    return Intl.message(
      'Novo endereço adicionado com sucesso',
      name: 'new_address_added_successfully',
      desc: '',
      args: [],
    );
  }

  /// `Novo pedido do cliente`
  String get new_order_from_client {
    return Intl.message(
      'Novo pedido do cliente',
      name: 'new_order_from_client',
      desc: '',
      args: [],
    );
  }

  /// `Not valid address`
  String get notValidAddress {
    return Intl.message(
      'Not valid address',
      name: 'notValidAddress',
      desc: '',
      args: [],
    );
  }

  /// `Endereço inválido`
  String get not_a_valid_address {
    return Intl.message(
      'Endereço inválido',
      name: 'not_a_valid_address',
      desc: '',
      args: [],
    );
  }

  /// `Sobre inválido`
  String get not_a_valid_biography {
    return Intl.message(
      'Sobre inválido',
      name: 'not_a_valid_biography',
      desc: '',
      args: [],
    );
  }

  /// `CVC inválido`
  String get not_a_valid_cvc {
    return Intl.message(
      'CVC inválido',
      name: 'not_a_valid_cvc',
      desc: '',
      args: [],
    );
  }

  /// `Data não válida`
  String get not_a_valid_date {
    return Intl.message(
      'Data não válida',
      name: 'not_a_valid_date',
      desc: '',
      args: [],
    );
  }

  /// `Não é um email válido`
  String get not_a_valid_email {
    return Intl.message(
      'Não é um email válido',
      name: 'not_a_valid_email',
      desc: '',
      args: [],
    );
  }

  /// `Não é um nome completo válido`
  String get not_a_valid_full_name {
    return Intl.message(
      'Não é um nome completo válido',
      name: 'not_a_valid_full_name',
      desc: '',
      args: [],
    );
  }

  /// `Não é um número válido`
  String get not_a_valid_number {
    return Intl.message(
      'Não é um número válido',
      name: 'not_a_valid_number',
      desc: '',
      args: [],
    );
  }

  /// `Não é um celular válido`
  String get not_a_valid_phone {
    return Intl.message(
      'Não é um celular válido',
      name: 'not_a_valid_phone',
      desc: '',
      args: [],
    );
  }

  /// `Não Disponível`
  String get not_deliverable {
    return Intl.message(
      'Não Disponível',
      name: 'not_deliverable',
      desc: '',
      args: [],
    );
  }

  /// `As notificações foram removidas`
  String get notificationWasRemoved {
    return Intl.message(
      'As notificações foram removidas',
      name: 'notificationWasRemoved',
      desc: '',
      args: [],
    );
  }

  /// `Notificações`
  String get notifications {
    return Intl.message(
      'Notificações',
      name: 'notifications',
      desc: '',
      args: [],
    );
  }

  /// `Notificações atualizadas com sucesso`
  String get notifications_refreshed_successfuly {
    return Intl.message(
      'Notificações atualizadas com sucesso',
      name: 'notifications_refreshed_successfuly',
      desc: '',
      args: [],
    );
  }

  /// `Número`
  String get number {
    return Intl.message(
      'Número',
      name: 'number',
      desc: '',
      args: [],
    );
  }

  /// `Este restaurante não faz entrega no endereço escolhido ou algum produto não está disponível em estoque.`
  String get oneOrMoreProductsInYourCartNotDeliverable {
    return Intl.message(
      'Um ou mais produtos não está disponíveis para entrega, verifique e tente novamente.',
      name: 'oneOrMoreProductsInYourCartNotDeliverable',
      desc: '',
      args: [],
    );
  }

  /// `Aberto`
  String get open {
    return Intl.message(
      'Aberto',
      name: 'open',
      desc: '',
      args: [],
    );
  }

  /// `restaurantes abertos`
  String get opened_markets {
    return Intl.message(
      'restaurantes abertos',
      name: 'opened_markets',
      desc: '',
      args: [],
    );
  }

  /// `Compre Junto`
  String get options {
    return Intl.message(
      'Compre Junto',
      name: 'options',
      desc: '',
      args: [],
    );
  }

  /// `Ou finalizar com`
  String get or_checkout_with {
    return Intl.message(
      'Ou finalizar com',
      name: 'or_checkout_with',
      desc: '',
      args: [],
    );
  }

  /// `Pedido`
  String get order {
    return Intl.message(
      'Pedido',
      name: 'order',
      desc: '',
      args: [],
    );
  }

  /// `Detalhes do pedido`
  String get orderDetails {
    return Intl.message(
      'Detalhes do pedido',
      name: 'orderDetails',
      desc: '',
      args: [],
    );
  }

  /// `Pedido: #{id} foi cancelado`
  String orderThisorderidHasBeenCanceled(Object id) {
    return Intl.message(
      'Pedido: #$id foi cancelado',
      name: 'orderThisorderidHasBeenCanceled',
      desc: '',
      args: [id],
    );
  }

  /// `Número do pedido`
  String get order_id {
    return Intl.message(
      'Número do pedido',
      name: 'order_id',
      desc: '',
      args: [],
    );
  }

  /// `Pedido atualizado com sucesso`
  String get order_refreshed_successfuly {
    return Intl.message(
      'Pedido atualizado com sucesso',
      name: 'order_refreshed_successfuly',
      desc: '',
      args: [],
    );
  }

  /// `Status do pedido alterado`
  String get order_status_changed {
    return Intl.message(
      'Status do pedido alterado',
      name: 'order_status_changed',
      desc: '',
      args: [],
    );
  }

  /// `Classificando por proximidade`
  String get ordered_by_nearby_first {
    return Intl.message(
      'Classificando por proximidade',
      name: 'ordered_by_nearby_first',
      desc: '',
      args: [],
    );
  }

  /// `Pedidos atualizados com sucesso`
  String get orders_refreshed_successfuly {
    return Intl.message(
      'Pedidos atualizados com sucesso',
      name: 'orders_refreshed_successfuly',
      desc: '',
      args: [],
    );
  }

  /// `Senha`
  String get password {
    return Intl.message(
      'Senha',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Pagar na retirada`
  String get pay_on_pickup {
    return Intl.message(
      'Pagar na retirada',
      name: 'pay_on_pickup',
      desc: '',
      args: [],
    );
  }

  /// `Cartão de pagamento atualizado com sucesso`
  String get payment_card_updated_successfully {
    return Intl.message(
      'Cartão de pagamento atualizado com sucesso',
      name: 'payment_card_updated_successfully',
      desc: '',
      args: [],
    );
  }

  /// `Formas de pagamento`
  String get payment_mode {
    return Intl.message(
      'Formas de pagamento',
      name: 'payment_mode',
      desc: '',
      args: [],
    );
  }

  /// `Opções de pagamento`
  String get payment_options {
    return Intl.message(
      'Opções de pagamento',
      name: 'payment_options',
      desc: '',
      args: [],
    );
  }

  /// `Configurações de pagamento`
  String get payment_settings {
    return Intl.message(
      'Configurações de pagamento',
      name: 'payment_settings',
      desc: '',
      args: [],
    );
  }

  /// `Configurações de pagamento atualizadas com sucesso`
  String get payment_settings_updated_successfully {
    return Intl.message(
      'Configurações de pagamento atualizadas com sucesso',
      name: 'payment_settings_updated_successfully',
      desc: '',
      args: [],
    );
  }

  /// `Configurações de pagamentos`
  String get payments_settings {
    return Intl.message(
      'Configurações de pagamentos',
      name: 'payments_settings',
      desc: '',
      args: [],
    );
  }

  /// `PayPal`
  String get paypal {
    return Intl.message(
      'PayPal',
      name: 'paypal',
      desc: '',
      args: [],
    );
  }

  /// `Pagamento PayPal`
  String get paypal_payment {
    return Intl.message(
      'Pagamento PayPal',
      name: 'paypal_payment',
      desc: '',
      args: [],
    );
  }

  /// `Celular`
  String get phone {
    return Intl.message(
      'Celular',
      name: 'phone',
      desc: '',
      args: [],
    );
  }

  /// `Número do Celular`
  String get phoneNumber {
    return Intl.message(
      'Número do Celular',
      name: 'phoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `Não entregamos`
  String get pickup {
    return Intl.message(
      'Não entregamos',
      name: 'pickup',
      desc: '',
      args: [],
    );
  }

  /// `Retire seu produtono restaurante`
  String get pickup_your_product_from_the_market {
    return Intl.message(
      'Retire seu produtono restaurante',
      name: 'pickup_your_product_from_the_market',
      desc: '',
      args: [],
    );
  }

  /// `Produto atualizado com sucesso`
  String get productRefreshedSuccessfuly {
    return Intl.message(
      'Produto atualizado com sucesso',
      name: 'productRefreshedSuccessfuly',
      desc: '',
      args: [],
    );
  }

  /// `Categorias de Produtos`
  String get product_categories {
    return Intl.message(
      'Categorias de Produtos',
      name: 'product_categories',
      desc: '',
      args: [],
    );
  }

  /// `Produtos`
  String get products {
    return Intl.message(
      'Produtos',
      name: 'products',
      desc: '',
      args: [],
    );
  }

  /// `Produto`
  String get products_result {
    return Intl.message(
      'Produto',
      name: 'products_result',
      desc: '',
      args: [],
    );
  }

  /// `Produtos`
  String get products_results {
    return Intl.message(
      'Produtos',
      name: 'products_results',
      desc: '',
      args: [],
    );
  }

  /// `Perfil`
  String get profile {
    return Intl.message(
      'Perfil',
      name: 'profile',
      desc: '',
      args: [],
    );
  }

  /// `Configurações de perfil`
  String get profile_settings {
    return Intl.message(
      'Configurações de perfil',
      name: 'profile_settings',
      desc: '',
      args: [],
    );
  }

  /// `Configurações de perfil atualizadas com sucesso`
  String get profile_settings_updated_successfully {
    return Intl.message(
      'Configurações de perfil atualizadas com sucesso',
      name: 'profile_settings_updated_successfully',
      desc: '',
      args: [],
    );
  }

  /// `Quantidade`
  String get quantity {
    return Intl.message(
      'Quantidade',
      name: 'quantity',
      desc: '',
      args: [],
    );
  }

  /// `RazorPay`
  String get razorpay {
    return Intl.message(
      'RazorPay',
      name: 'razorpay',
      desc: '',
      args: [],
    );
  }

  /// `RazorPay Payment`
  String get razorpayPayment {
    return Intl.message(
      'RazorPay Payment',
      name: 'razorpayPayment',
      desc: '',
      args: [],
    );
  }

  /// `Pedidos recentes`
  String get recent_orders {
    return Intl.message(
      'Pedidos recentes',
      name: 'recent_orders',
      desc: '',
      args: [],
    );
  }

  /// `Comentários recentes`
  String get recent_reviews {
    return Intl.message(
      'Comentários recentes',
      name: 'recent_reviews',
      desc: '',
      args: [],
    );
  }

  /// `Pesquisa recente`
  String get recents_search {
    return Intl.message(
      'Pesquisa recente',
      name: 'recents_search',
      desc: '',
      args: [],
    );
  }

  /// `Cadastrar`
  String get register {
    return Intl.message(
      'Cadastrar',
      name: 'register',
      desc: '',
      args: [],
    );
  }

  /// `Limpar`
  String get reset {
    return Intl.message(
      'Limpar',
      name: 'reset',
      desc: '',
      args: [],
    );
  }

  /// `Ops! É preciso limpar o carrinho.`
  String get reset_cart {
    return Intl.message(
      'Ops! É preciso limpar o carrinho.',
      name: 'reset_cart',
      desc: '',
      args: [],
    );
  }

  /// `Manter os produtos ddeste restaurante`
  String get reset_your_cart_and_order_meals_form_this_market {
    return Intl.message(
      'Manter os produtos ddeste restaurante',
      name: 'reset_your_cart_and_order_meals_form_this_market',
      desc: '',
      args: [],
    );
  }

  /// `Avaliações`
  String get reviews {
    return Intl.message(
      'Avaliações',
      name: 'reviews',
      desc: '',
      args: [],
    );
  }

  /// `Comentários atualizados com sucesso!`
  String get reviews_refreshed_successfully {
    return Intl.message(
      'Comentários atualizados com sucesso!',
      name: 'reviews_refreshed_successfully',
      desc: '',
      args: [],
    );
  }

  /// `Salvar`
  String get save {
    return Intl.message(
      'Salvar',
      name: 'save',
      desc: '',
      args: [],
    );
  }

  /// `Procurar`
  String get search {
    return Intl.message(
      'Procurar',
      name: 'search',
      desc: '',
      args: [],
    );
  }

  /// `Escolha também mais produtos.`
  String get select_options_to_add_them_on_the_product {
    return Intl.message(
      'Escolha também mais produtos.',
      name: 'select_options_to_add_them_on_the_product',
      desc: '',
      args: [],
    );
  }

  /// `Selecione seu idioma preferido`
  String get select_your_preferred_languages {
    return Intl.message(
      'Selecione seu idioma preferido',
      name: 'select_your_preferred_languages',
      desc: '',
      args: [],
    );
  }

  /// `Selecione a opção abaixo`
  String get select_your_preferred_payment_mode {
    return Intl.message(
      'Selecione a opção abaixo',
      name: 'select_your_preferred_payment_mode',
      desc: '',
      args: [],
    );
  }

  /// `Enviar link de redefinição de senha`
  String get send_password_reset_link {
    return Intl.message(
      'Enviar link de redefinição de senha',
      name: 'send_password_reset_link',
      desc: '',
      args: [],
    );
  }

  /// `Configurações`
  String get settings {
    return Intl.message(
      'Configurações',
      name: 'settings',
      desc: '',
      args: [],
    );
  }

  /// `Carrinho`
  String get shopping {
    return Intl.message(
      'Carrinho',
      name: 'shopping',
      desc: '',
      args: [],
    );
  }

  /// `Ver todos os produtos`
  String get shopping_cart {
    return Intl.message(
      'Ver todos os produtos',
      name: 'shopping_cart',
      desc: '',
      args: [],
    );
  }

  /// `Deve ser um email válido`
  String get should_be_a_valid_email {
    return Intl.message(
      'Deve ser um email válido',
      name: 'should_be_a_valid_email',
      desc: '',
      args: [],
    );
  }

  /// `Deve ter mais de 3 caracteres`
  String get should_be_more_than_3_characters {
    return Intl.message(
      'Deve ter mais de 3 caracteres',
      name: 'should_be_more_than_3_characters',
      desc: '',
      args: [],
    );
  }

  /// `Deve ter mais de 3 letras`
  String get should_be_more_than_3_letters {
    return Intl.message(
      'Deve ter mais de 3 letras',
      name: 'should_be_more_than_3_letters',
      desc: '',
      args: [],
    );
  }

  /// `Deve ter mais de 6 letras`
  String get should_be_more_than_6_letters {
    return Intl.message(
      'Deve ter mais de 6 letras',
      name: 'should_be_more_than_6_letters',
      desc: '',
      args: [],
    );
  }

  /// `Sign-In to chat with our managers`
  String get signinToChatWithOurManagers {
    return Intl.message(
      'Sign-In to chat with our managers',
      name: 'signinToChatWithOurManagers',
      desc: '',
      args: [],
    );
  }

  /// `Voltar para a página inicial`
  String get skip {
    return Intl.message(
      'Voltar para a página inicial',
      name: 'skip',
      desc: '',
      args: [],
    );
  }

  /// `SMS has been sent to`
  String get smsHasBeenSentTo {
    return Intl.message(
      'SMS has been sent to',
      name: 'smsHasBeenSentTo',
      desc: '',
      args: [],
    );
  }

  /// `Continuar Comprando`
  String get start_exploring {
    return Intl.message(
      'Continuar Comprando',
      name: 'start_exploring',
      desc: '',
      args: [],
    );
  }

  /// `Enviar`
  String get submit {
    return Intl.message(
      'Enviar',
      name: 'submit',
      desc: '',
      args: [],
    );
  }

  /// `Subtotal`
  String get subtotal {
    return Intl.message(
      'Subtotal',
      name: 'subtotal',
      desc: '',
      args: [],
    );
  }

  /// `Arraste para o lado para excluir ou marcar como lida`
  String get swipeLeftTheNotificationToDeleteOrReadUnreadIt {
    return Intl.message(
      'Arraste para o lado para excluir ou marcar como lida',
      name: 'swipeLeftTheNotificationToDeleteOrReadUnreadIt',
      desc: '',
      args: [],
    );
  }

  /// `Toque novamente para sair`
  String get tapAgainToLeave {
    return Intl.message(
      'Toque novamente para sair',
      name: 'tapAgainToLeave',
      desc: '',
      args: [],
    );
  }

  /// `Desconto`
  String get tax {
    return Intl.message(
      'Desconto',
      name: 'tax',
      desc: '',
      args: [],
    );
  }

  /// `Escreva sua avaliação aqui`
  String get tell_us_about_this_market {
    return Intl.message(
      'Escreva sua avaliação aqui',
      name: 'tell_us_about_this_market',
      desc: '',
      args: [],
    );
  }

  /// `Avalie este produto`
  String get tell_us_about_this_product {
    return Intl.message(
      'Avalie este produto',
      name: 'tell_us_about_this_product',
      desc: '',
      args: [],
    );
  }

  /// `O endereço foi atualizado com sucesso`
  String get the_address_updated_successfully {
    return Intl.message(
      'O endereço foi atualizado com sucesso',
      name: 'the_address_updated_successfully',
      desc: '',
      args: [],
    );
  }

  /// `O restaurante foi classificado com sucesso`
  String get the_market_has_been_rated_successfully {
    return Intl.message(
      'O restaurante foi classificado com sucesso',
      name: 'the_market_has_been_rated_successfully',
      desc: '',
      args: [],
    );
  }

  /// `O produto foi classificado com sucesso`
  String get the_product_has_been_rated_successfully {
    return Intl.message(
      'O produto foi classificado com sucesso',
      name: 'the_product_has_been_rated_successfully',
      desc: '',
      args: [],
    );
  }

  /// `O {productName} foi removido do seu carrinho`
  String the_product_was_removed_from_your_cart(Object productName) {
    return Intl.message(
      'O $productName foi removido do seu carrinho',
      name: 'the_product_was_removed_from_your_cart',
      desc: '',
      args: [productName],
    );
  }

  /// `O {productName} foi incluído em seu carrinho...`
  String the_product_was_add_from_your_cart(Object productName) {
    return Intl.message(
      'O $productName foi incluído em seu carrinho...',
      name: 'the_product_was_add_from_your_cart',
      desc: '',
      args: [productName],
    );
  }

  /// `Este restaurante não suporta o método de entrega.`
  String get thisMarketNotSupportDeliveryMethod {
    return Intl.message(
      'Este restaurante não suporta o método de entrega.',
      name: 'thisMarketNotSupportDeliveryMethod',
      desc: '',
      args: [],
    );
  }

  /// `Essa notificação foi marcada como lida`
  String get thisNotificationHasMarkedAsRead {
    return Intl.message(
      'Essa notificação foi marcada como lida',
      name: 'thisNotificationHasMarkedAsRead',
      desc: '',
      args: [],
    );
  }

  /// `Essa notificação foi marcada como não lida`
  String get thisNotificationHasMarkedAsUnread {
    return Intl.message(
      'Essa notificação foi marcada como não lida',
      name: 'thisNotificationHasMarkedAsUnread',
      desc: '',
      args: [],
    );
  }

  /// `This product was added to favorite`
  String get thisProductWasAddedToFavorite {
    return Intl.message(
      'This product was added to favorite',
      name: 'thisProductWasAddedToFavorite',
      desc: '',
      args: [],
    );
  }

  /// `This product was removed from favorites`
  String get thisProductWasRemovedFromFavorites {
    return Intl.message(
      'This product was removed from favorites',
      name: 'thisProductWasRemovedFromFavorites',
      desc: '',
      args: [],
    );
  }

  /// `Esta conta não existe`
  String get this_account_not_exist {
    return Intl.message(
      'Esta conta não existe',
      name: 'this_account_not_exist',
      desc: '',
      args: [],
    );
  }

  /// `Esta conta de email existe`
  String get this_email_account_exists {
    return Intl.message(
      'Esta conta de email já existe',
      name: 'this_email_account_exists',
      desc: '',
      args: [],
    );
  }

  /// `Este restaurante não faz entrega no endereço escolhido ou algum produto não está disponível em estoque.`
  String get this_market_is_closed_ {
    return Intl.message(
      'Não é possível continuar pois o estabelecimento está fechado.',
      name: 'this_market_is_closed_',
      desc: '',
      args: [],
    );
  }

  /// `Este produto foi adicionado aos favoritos`
  String get this_product_was_added_to_cart {
    return Intl.message(
      'Este produto foi adicionado aos favoritos',
      name: 'this_product_was_added_to_cart',
      desc: '',
      args: [],
    );
  }

  /// `Restaurantes próximos a`
  String get top_markets {
    return Intl.message(
      'Restaurantes próximos a',
      name: 'top_markets',
      desc: '',
      args: [],
    );
  }

  /// `Total`
  String get total {
    return Intl.message(
      'Total',
      name: 'total',
      desc: '',
      args: [],
    );
  }

  /// `Rastreamento`
  String get tracking_order {
    return Intl.message(
      'Rastreamento',
      name: 'tracking_order',
      desc: '',
      args: [],
    );
  }

  /// `Rastreamento atualizado com sucesso`
  String get tracking_refreshed_successfuly {
    return Intl.message(
      'Rastreamento atualizado com sucesso',
      name: 'tracking_refreshed_successfuly',
      desc: '',
      args: [],
    );
  }

  /// `Tendências desta semana`
  String get trending_this_week {
    return Intl.message(
      'Tendências desta semana',
      name: 'trending_this_week',
      desc: '',
      args: [],
    );
  }

  /// `Type to start chat`
  String get typeToStartChat {
    return Intl.message(
      'Type to start chat',
      name: 'typeToStartChat',
      desc: '',
      args: [],
    );
  }

  /// `Clique aqui e use a sua localização atual`
  String get unknown {
    return Intl.message(
      'Clique aqui e use a sua localização atual',
      name: 'unknown',
      desc: '',
      args: [],
    );
  }

  /// `validar Cupom`
  String get validCouponCode {
    return Intl.message(
      'validar Cupom',
      name: 'validCouponCode',
      desc: '',
      args: [],
    );
  }

  /// `Verificar`
  String get verify {
    return Intl.message(
      'Verificar',
      name: 'verify',
      desc: '',
      args: [],
    );
  }

  /// `Verify Phone Number`
  String get verifyPhoneNumber {
    return Intl.message(
      'Verify Phone Number',
      name: 'verifyPhoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `Verifique sua conexão com a Internet`
  String get verify_your_internet_connection {
    return Intl.message(
      'Verifique sua conexão com a Internet',
      name: 'verify_your_internet_connection',
      desc: '',
      args: [],
    );
  }

  /// `Verifique seu pedido e clique em Finalizar`
  String get verify_your_quantity_and_click_checkout {
    return Intl.message(
      'Verifique seu pedido e clique em Finalizar',
      name: 'verify_your_quantity_and_click_checkout',
      desc: '',
      args: [],
    );
  }

  /// `Versão`
  String get version {
    return Intl.message(
      'Versão',
      name: 'version',
      desc: '',
      args: [],
    );
  }

  /// `Ver pedido`
  String get view {
    return Intl.message(
      'Ver pedido',
      name: 'view',
      desc: '',
      args: [],
    );
  }

  /// `Ver detalhes`
  String get viewDetails {
    return Intl.message(
      'Ver detalhes',
      name: 'viewDetails',
      desc: '',
      args: [],
    );
  }

  /// `Cartão Visa`
  String get visa_card {
    return Intl.message(
      'Cartão Visa',
      name: 'visa_card',
      desc: '',
      args: [],
    );
  }

  /// `We are sending OTP to validate your mobile number. Hang on!`
  String get weAreSendingOtpToValidateYourMobileNumberHang {
    return Intl.message(
      'We are sending OTP to validate your mobile number. Hang on!',
      name: 'weAreSendingOtpToValidateYourMobileNumberHang',
      desc: '',
      args: [],
    );
  }

  /// `Bem-vinda(a)`
  String get welcome {
    return Intl.message(
      'Bem-vinda(a)',
      name: 'welcome',
      desc: '',
      args: [],
    );
  }

  /// `Avaliações`
  String get what_they_say {
    return Intl.message(
      'Avaliações',
      name: 'what_they_say',
      desc: '',
      args: [],
    );
  }

  /// `e-mail ou senha incorretos`
  String get wrong_email_or_password {
    return Intl.message(
      'e-mail ou senha incorretos',
      name: 'wrong_email_or_password',
      desc: '',
      args: [],
    );
  }

  /// `sim`
  String get yes {
    return Intl.message(
      'sim',
      name: 'yes',
      desc: '',
      args: [],
    );
  }

  /// `You don't have any conversations`
  String get youDontHaveAnyConversations {
    return Intl.message(
      'You don\'t have any conversations',
      name: 'youDontHaveAnyConversations',
      desc: '',
      args: [],
    );
  }

  /// `Você não tem nenhum pedido`
  String get youDontHaveAnyOrder {
    return Intl.message(
      'Você não tem nenhum pedido',
      name: 'youDontHaveAnyOrder',
      desc: '',
      args: [],
    );
  }

  /// `Você pode descobrir restaurantes e produtos ao seu redor e faça o seu pedido e após alguns minutos, nós entregamos para você`
  String get you_can_discover_markets {
    return Intl.message(
      'Você pode descobrir restaurantes e produtos ao seu redor e faça o seu pedido e após alguns minutos, nós entregamos para você',
      name: 'you_can_discover_markets',
      desc: '',
      args: [],
    );
  }

  /// `Você deve adicionar produtos somente de 1 restaurante`
  String get you_must_add_products_of_the_same_markets_choose_one {
    return Intl.message(
      'Você deve adicionar produtos somente de 1 restaurante',
      name: 'you_must_add_products_of_the_same_markets_choose_one',
      desc: '',
      args: [],
    );
  }

  /// `Faça login e explore o máximo do Divino Menu`
  String get you_must_signin_to_access_to_this_section {
    return Intl.message(
      'Faça login e explore o máximo do Divino Menu',
      name: 'you_must_signin_to_access_to_this_section',
      desc: '',
      args: [],
    );
  }

  /// `Seu endereço completo`
  String get your_address {
    return Intl.message(
      'Seu endereço completo',
      name: 'your_address',
      desc: '',
      args: [],
    );
  }

  /// `Sobre Você`
  String get your_biography {
    return Intl.message(
      'Sobre Você',
      name: 'your_biography',
      desc: '',
      args: [],
    );
  }

  /// `O seu cartão de crédito não é válido`
  String get your_credit_card_not_valid {
    return Intl.message(
      'O seu cartão de crédito não é válido',
      name: 'your_credit_card_not_valid',
      desc: '',
      args: [],
    );
  }

  /// `Seu pedido foi enviado com sucesso!`
  String get your_order_has_been_successfully_submitted {
    return Intl.message(
      'Seu pedido foi enviado com sucesso!',
      name: 'your_order_has_been_successfully_submitted',
      desc: '',
      args: [],
    );
  }

  /// `Seu link de redefinição foi enviado para seu e-mail`
  String get your_reset_link_has_been_sent_to_your_email {
    return Intl.message(
      'Seu link de redefinição foi enviado para seu e-mail',
      name: 'your_reset_link_has_been_sent_to_your_email',
      desc: '',
      args: [],
    );
  }

  /// `Digite o nome ou código de barras`
  String get search_for_markets_or_products {
    return Intl.message(
      'Digite o nome ou código de barras',
      name: 'search_for_markets_or_products',
      desc: '',
      args: [],
    );
  }

  /// `Pedido Minimo`
  String get kriacoes_agency_module_minimum_order {
    return Intl.message(
      'Pedido Minimo',
      name: 'kriacoes_agency_module_minimum_order',
      desc: '',
      args: [],
    );
  }

  /// `O valor mínimo do pedido não está incluído nas taxas.`
  String get kriacoes_agency_module_minimum_order_alert_part1 {
    return Intl.message(
      'O valor mínimo do pedido não está incluído nas taxas.',
      name: 'kriacoes_agency_module_minimum_order_alert_part1',
      desc: '',
      args: [],
    );
  }

  /// `\n O valor do seu pedido atual é:`
  String get kriacoes_agency_module_minimum_order_alert_par2 {
    return Intl.message(
      '\n O valor do seu pedido atual é:',
      name: 'kriacoes_agency_module_minimum_order_alert_par2',
      desc: '',
      args: [],
    );
  }

  /// `Não há Restaurantes próximos do seu endereço cadastradas no aplicativo. Já estamos trabalhando para em breve você encontrar.`
  String get pharms_partner_not_found {
    return Intl.message(
      'Não encontramos estabelecimentos próximos ao endereço inserido. Já estamos trabalhando para em breve você encontrar.',
      name: 'pharms_partner_not_found',
      desc: '',
      args: [],
    );
  }

  /// `Arraste o produto para a esquerda para remover do seu carrinho`
  String get pharms_drag_product_remove {
    return Intl.message(
      'Arraste o produto para a esquerda para remover do seu carrinho',
      name: 'pharms_drag_product_remove',
      desc: '',
      args: [],
    );
  }

  /// `Aplicar`
  String get pharms_coupon_check {
    return Intl.message(
      'Aplicar',
      name: 'pharms_coupon_check',
      desc: '',
      args: [],
    );
  }

  /// `Valor total dos produtos`
  String get pharms_total {
    return Intl.message(
      'Valor total dos produtos',
      name: 'pharms_total',
      desc: '',
      args: [],
    );
  }

  /// `Ei! No momento só estamos atuando na cidade de Niterói-RJ. \nSe você não é de Niterói, este abaixo sou eu indo até a sua cidade, me aguardeee!`
  String get pharms_warning_area {
    return Intl.message(
      'Ei! No momento só estamos atuando na cidade de Niterói-RJ. \n Se você não é de Niterói, este abaixo sou eu indo até a sua cidade, me aguardeee!',
      name: 'pharms_warning_area',
      desc: '',
      args: [],
    );
  }

  /// `Para usar o Divino Menu, você precisa estar logado no aplicativo.`
  String get pharms_warning_login {
    return Intl.message(
      'Vamos começar?',
      name: 'pharms_warning_login',
      desc: '',
      args: [],
    );
  }

  /// `Acessar minha conta`
  String get pharms_account_login {
    return Intl.message(
      'Acessar minha conta',
      name: 'pharms_account_login',
      desc: '',
      args: [],
    );
  }

  /// `Quero me cadastrar`
  String get pharms_account_singup {
    return Intl.message(
      'Quero me cadastrar',
      name: 'pharms_account_singup',
      desc: '',
      args: [],
    );
  }

  /// `Ahh! Quase que eu esqueço! \n Vamos inserir a sua localização?`
  String get pharms_warning_gps {
    return Intl.message(
      'Ahh! Quase que eu esqueço! \n Vamos inserir a sua localização?',
      name: 'pharms_warning_gps',
      desc: '',
      args: [],
    );
  }

  /// `perto de `
  String get pharms_near_by {
    return Intl.message(
      'perto de ',
      name: 'pharms_near_by',
      desc: '',
      args: [],
    );
  }

  /// `Bem vindo(a), `
  String get pharms_welcome {
    return Intl.message(
      'Olá, ',
      name: 'pharms_welcome',
      desc: '',
      args: [],
    );
  }

  /// `- Não há mais Restaurantes próximos -`
  String get pharms_not_have_pharmacies {
    return Intl.message(
      '- Não há mais Restaurantes próximos -',
      name: 'pharms_not_have_pharmacies',
      desc: '',
      args: [],
    );
  }

  /// `Este restaurante possui um valor de pedido mínimo de `
  String get pharms_minimum_title {
    return Intl.message(
      'Este restaurante possui um valor de pedido mínimo de ',
      name: 'pharms_minimum_title',
      desc: '',
      args: [],
    );
  }

  /// `O valor do pedido mínimo não inclui a taxa de entrega.\n`
  String get pharms_alert_part1 {
    return Intl.message(
      'O valor do pedido mínimo não inclui a taxa de entrega.\n',
      name: 'pharms_alert_part1',
      desc: '',
      args: [],
    );
  }

  /// `Valor atual do seu carrinho `
  String get pharms_alert_part2 {
    return Intl.message(
      'Valor atual do seu carrinho ',
      name: 'pharms_alert_part2',
      desc: '',
      args: [],
    );
  }

  /// `Produto adicionado ao carrinho!`
  String get pharms_product_title {
    return Intl.message(
      'Produto adicionado ao carrinho!',
      name: 'pharms_product_title',
      desc: '',
      args: [],
    );
  }

  /// `O produto {productName} foi adicionado ao seu carrinho`
  String pharms_product_msg(Object productName) {
    return Intl.message(
      'O produto $productName foi adicionado ao seu carrinho',
      name: 'pharms_product_msg',
      desc: '',
      args: [productName],
    );
  }

  /// `Ver carrinho`
  String get pharms_product_btn {
    return Intl.message(
      'Ver carrinho',
      name: 'pharms_product_btn',
      desc: '',
      args: [],
    );
  }

  /// `Por apenas: `
  String get pharms_for_only {
    return Intl.message(
      'Por apenas: ',
      name: 'pharms_for_only',
      desc: '',
      args: [],
    );
  }

  /// `Entrega: `
  String get pharms_delivery_home {
    return Intl.message(
      'Entrega: ',
      name: 'pharms_delivery_home',
      desc: '',
      args: [],
    );
  }

  /// `Endereço de entrega: `
  String get pharms_delivery_address {
    return Intl.message(
      'Endereço de entrega: ',
      name: 'pharms_delivery_address',
      desc: '',
      args: [],
    );
  }

  /// `Falta Pouco!`
  String get pharms_signup_title {
    return Intl.message(
      'Falta Pouco!',
      name: 'pharms_signup_title',
      desc: '',
      args: [],
    );
  }

  /// `Explore o máximo do Divino Menu`
  String get pharms_signup_message {
    return Intl.message(
      'Explore o máximo do Divino Menu',
      name: 'pharms_signup_message',
      desc: '',
      args: [],
    );
  }

  /// `exemplo@divinomenu.com.br`
  String get pharms_hint_email {
    return Intl.message(
      'exemplo@divinomenu.com.br',
      name: 'pharms_hint_email',
      desc: '',
      args: [],
    );
  }

  /// `+55 123456789`
  String get pharms_hint_mobile {
    return Intl.message(
      '+55 123456789',
      name: 'pharms_hint_mobile',
      desc: '',
      args: [],
    );
  }

  /// `Número do seu telefone`
  String get pharms_hint_phone {
    return Intl.message(
      'Número do seu telefone',
      name: 'pharms_hint_phone',
      desc: '',
      args: [],
    );
  }

  /// `+55 DDD Número`
  String get pharms_hint_phone_ddi {
    return Intl.message(
      '+55 DDD Número',
      name: 'pharms_hint_phone_ddi',
      desc: '',
      args: [],
    );
  }

  /// `Acessar minha conta`
  String get pharms_hint_login {
    return Intl.message(
      'Acessar minha conta',
      name: 'pharms_hint_login',
      desc: '',
      args: [],
    );
  }

  /// `Eu já possuo uma conta`
  String get pharms_have_account {
    return Intl.message(
      'Eu já possuo uma conta',
      name: 'pharms_have_account',
      desc: '',
      args: [],
    );
  }

  /// `Facilite o troco`
  String get pharms_make_the_change_easier {
    return Intl.message(
      'Facilite o troco',
      name: 'pharms_make_the_change_easier',
      desc: '',
      args: [],
    );
  }

  /// `Precisa de troco?`
  String get pharms_need_change {
    return Intl.message(
      'Precisa de troco?',
      name: 'pharms_need_change',
      desc: '',
      args: [],
    );
  }

  /// `Troco para qual valor?`
  String get pharms_what_value {
    return Intl.message(
      'Troco para qual valor?',
      name: 'pharms_what_value',
      desc: '',
      args: [],
    );
  }

  /// `Mapa`
  String get pharms_bottom_navigation_map {
    return Intl.message(
      'Mapa',
      name: 'pharms_bottom_navigation_map',
      desc: '',
      args: [],
    );
  }

  /// `Pedidos`
  String get pharms_bottom_navigation_orders {
    return Intl.message(
      'Pedidos',
      name: 'pharms_bottom_navigation_orders',
      desc: '',
      args: [],
    );
  }

  /// `Início`
  String get pharms_bottom_navigation_home {
    return Intl.message(
      'Início',
      name: 'pharms_bottom_navigation_home',
      desc: '',
      args: [],
    );
  }

  /// `Carrinho`
  String get pharms_bottom_navigation_cart {
    return Intl.message(
      'Carrinho',
      name: 'pharms_bottom_navigation_cart',
      desc: '',
      args: [],
    );
  }

  /// `Favoritos`
  String get pharms_bottom_navigation_favorites {
    return Intl.message(
      'Favoritos',
      name: 'pharms_bottom_navigation_favorites',
      desc: '',
      args: [],
    );
  }

  /// `Total`
  String get pharms_orders_success_total {
    return Intl.message(
      'Total',
      name: 'pharms_orders_success_total',
      desc: '',
      args: [],
    );
  }

  /// `Valor total dos produtos`
  String get pharms_orders_success_products_total {
    return Intl.message(
      'Valor total dos produtos',
      name: 'pharms_orders_success_products_total',
      desc: '',
      args: [],
    );
  }

  /// `Informações`
  String get pharms_details_info {
    return Intl.message(
      'Informações',
      name: 'pharms_details_info',
      desc: '',
      args: [],
    );
  }

  /// `Confirme o seu endereço`
  String get pharms_delivery_pickup_confirm_address {
    return Intl.message(
      'Confirme o seu endereço',
      name: 'pharms_delivery_pickup_confirm_address',
      desc: '',
      args: [],
    );
  }

  /// `Entregar no endereço: `
  String get pharms_delivery_pickup_ {
    return Intl.message(
      'Entregar no endereço: ',
      name: 'pharms_delivery_pickup_',
      desc: '',
      args: [],
    );
  }

  /// `Complemento: `
  String get pharms_delivery_pickup_refer_main {
    return Intl.message(
      'Complemento: ',
      name: 'pharms_delivery_pickup_refer_main',
      desc: '',
      args: [],
    );
  }

  /// `Por favor informe um complemento`
  String get pharms_delivery_pickup_refer {
    return Intl.message(
      'Por favor informe um complemento',
      name: 'pharms_delivery_pickup_refer',
      desc: '',
      args: [],
    );
  }

  /// `Complemento`
  String get pharms_delivery_pickup_refer_add {
    return Intl.message(
      'Complemento',
      name: 'pharms_delivery_pickup_refer_add',
      desc: '',
      args: [],
    );
  }

  /// `Apto / Bloco / Casa`
  String get pharms_delivery_pickup_refer_hint {
    return Intl.message(
      'Apto / Bloco / Casa',
      name: 'pharms_delivery_pickup_refer_hint',
      desc: '',
      args: [],
    );
  }

  /// `Referência: `
  String get pharms_delivery_pickup_add_main {
    return Intl.message(
      'Referência: ',
      name: 'pharms_delivery_pickup_add_main',
      desc: '',
      args: [],
    );
  }

  /// `Por favor informe uma referência`
  String get pharms_delivery_pickup_add {
    return Intl.message(
      'Por favor informe uma referência',
      name: 'pharms_delivery_pickup_add',
      desc: '',
      args: [],
    );
  }

  /// `Ponto de Referência`
  String get pharms_delivery_pickup_add_hint {
    return Intl.message(
      'Ponto de Referência',
      name: 'pharms_delivery_pickup_add_hint',
      desc: '',
      args: [],
    );
  }

  /// `O código expirou, tente novamente!`
  String get pharms_otp_code_expires {
    return Intl.message(
      'O código expirou, tente novamente!',
      name: 'pharms_otp_code_expires',
      desc: '',
      args: [],
    );
  }

  /// `Realizar pagamento`
  String get pharms_cart_bottom_pay {
    return Intl.message(
      'Realizar pagamento',
      name: 'pharms_cart_bottom_pay',
      desc: '',
      args: [],
    );
  }

  /// `Definir endereço`
  String get pharms_cart_bottom_address {
    return Intl.message(
      'Definir endereço',
      name: 'pharms_cart_bottom_address',
      desc: '',
      args: [],
    );
  }

  /// `Para prosseguir é necessário definir seu endereço atual`
  String get pharms_cart_bottom_address_msg {
    return Intl.message(
      'Para prosseguir é necessário definir seu endereço atual',
      name: 'pharms_cart_bottom_address_msg',
      desc: '',
      args: [],
    );
  }

  /// `Definir`
  String get pharms_cart_bottom_address_btn {
    return Intl.message(
      'Definir',
      name: 'pharms_cart_bottom_address_btn',
      desc: '',
      args: [],
    );
  }

  /// `Onde você quer receber o seu pedido?`
  String get pharms_delivery_address_bottom_title {
    return Intl.message(
      'Onde você quer receber o seu pedido?',
      name: 'pharms_delivery_address_bottom_title',
      desc: '',
      args: [],
    );
  }

  /// `Selecione abaixo o endereço desejado`
  String get pharms_delivery_address_bottom_select {
    return Intl.message(
      'Selecione abaixo o endereço desejado',
      name: 'pharms_delivery_address_bottom_select',
      desc: '',
      args: [],
    );
  }

  /// `Declaro que o meu endereço está correto.`
  String get pharms_delivery_address_sing {
    return Intl.message(
      'Declaro que o meu endereço está correto.',
      name: 'pharms_delivery_address_sing',
      desc: '',
      args: [],
    );
  }

  /// `Alterar endereço`
  String get pharms_delivery_address_change {
    return Intl.message(
      'Alterar endereço',
      name: 'pharms_delivery_address_change',
      desc: '',
      args: [],
    );
  }

  /// `Neste momento, não encontramos o produto desejado nos Restaurantes próximos. Mas em breve, ele estará disponível para compra aqui no aplicativo!`
  String get pharms_product_not_found {
    return Intl.message(
      'Neste momento, não encontramos o produto desejado nos Restaurantes próximos. Mas em breve, ele estará disponível para compra aqui no aplicativo!',
      name: 'pharms_product_not_found',
      desc: '',
      args: [],
    );
  }

  /// `Ops! Algo deu errado...`
  String get pharms_route_error_title {
    return Intl.message(
      'Ops! Algo deu errado...',
      name: 'pharms_route_error_title',
      desc: '',
      args: [],
    );
  }

  /// `Clique para atualizar o aplicativo.`
  String get pharms_route_error_msg {
    return Intl.message(
      'Clique para atualizar o aplicativo.',
      name: 'pharms_route_error_msg',
      desc: '',
      args: [],
    );
  }

  /// `Atualizar`
  String get pharms_route_error_btn {
    return Intl.message(
      'Atualizar',
      name: 'pharms_route_error_btn',
      desc: '',
      args: [],
    );
  }

  /// `Leitor de código de barras ao lado`
  String get pharms_search_results_barcode {
    return Intl.message(
      'Leitor de código de barras ao lado',
      name: 'pharms_search_results_barcode',
      desc: '',
      args: [],
    );
  }

  /// `Código de barras: `
  String get pharms_search_results_barcode_hint {
    return Intl.message(
      'Código de barras: ',
      name: 'pharms_search_results_barcode_hint',
      desc: '',
      args: [],
    );
  }

  /// `Tx. de Entrega`
  String get pharms_products_delivery {
    return Intl.message(
      'Tx. de Entrega',
      name: 'pharms_products_delivery',
      desc: '',
      args: [],
    );
  }

  /// `Falha ao obter a versão da plataforma.`
  String get pharms_barcode_error {
    return Intl.message(
      'Falha ao obter a versão da plataforma.',
      name: 'pharms_barcode_error',
      desc: '',
      args: [],
    );
  }

  /// `Remover produto?`
  String get pharms_cart_remove_title {
    return Intl.message(
      'Remover produto?',
      name: 'pharms_cart_remove_title',
      desc: '',
      args: [],
    );
  }

  /// `Você tem certeza que deseja remover o {productName} dos seu pedido?`
  String pharms_cart_remove_msg(Object productName) {
    return Intl.message(
      'Você tem certeza que deseja remover o $productName dos seu pedido?',
      name: 'pharms_cart_remove_msg',
      desc: '',
      args: [productName],
    );
  }

  /// `Cancelar`
  String get pharms_cart_remove_btncancel {
    return Intl.message(
      'Cancelar',
      name: 'pharms_cart_remove_btncancel',
      desc: '',
      args: [],
    );
  }

  /// `Sim, remover`
  String get pharms_cart_remove_btnok {
    return Intl.message(
      'Sim, remover',
      name: 'pharms_cart_remove_btnok',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}
