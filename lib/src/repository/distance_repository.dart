import 'package:shared_preferences/shared_preferences.dart';

void setDoubleDistance(value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setDouble('double_distance', value);
}