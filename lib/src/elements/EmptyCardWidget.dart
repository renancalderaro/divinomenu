import 'package:flutter/material.dart';
import '../../generated/l10n.dart';
import '../helpers/app_config.dart' as config;

class EmptyCardWidget extends StatefulWidget {
  EmptyCardWidget({
    Key key,
  }) : super(key: key);

  @override
  _EmptyCardWidgetState createState() => _EmptyCardWidgetState();
}

class _EmptyCardWidgetState extends State<EmptyCardWidget> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {},
      child: Column(
      children: <Widget>[
        Container(
          alignment: AlignmentDirectional.center,
          padding: EdgeInsets.symmetric(horizontal: 20),
          height: config.App(context).appHeight(25),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        gradient: LinearGradient(begin: Alignment.bottomLeft, end: Alignment.topRight, colors: [
                          Theme.of(context).accentColor.withOpacity(0.7),
                          Theme.of(context).accentColor.withOpacity(0.05),
                        ])),
                    child: Icon(
                      Icons.search_off,
                      color: Theme.of(context).scaffoldBackgroundColor,
                      size: 50,
                    ),
                  ),
                  Positioned(
                    right: -30,
                    bottom: -50,
                    child: Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                        color: Theme.of(context).scaffoldBackgroundColor.withOpacity(0.15),
                        borderRadius: BorderRadius.circular(150),
                      ),
                    ),
                  ),
                  Positioned(
                    left: -20,
                    top: -50,
                    child: Container(
                      width: 120,
                      height: 120,
                      decoration: BoxDecoration(
                        color: Theme.of(context).scaffoldBackgroundColor.withOpacity(0.15),
                        borderRadius: BorderRadius.circular(150),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 10),
              Opacity(
                opacity: 0.4,
                child: Text(
                  S.of(context).pharms_partner_not_found,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline3.merge(TextStyle(fontWeight: FontWeight.w300, fontSize: 16)),
                ),
              ),
              SizedBox(),
            ],
          ),
        ),
      ],
    )
    );
  }
}
