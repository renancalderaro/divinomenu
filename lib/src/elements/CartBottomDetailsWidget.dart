import 'package:Divino/generated/l10n.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import '../kriacoes_agency_modules/minimum_order/controllers/cart_controller.dart';
import '../repository/settings_repository.dart' as settingsRepo;
import 'BlockButtonWidget.dart';

class CartBottomDetailsWidget extends StatelessWidget {
  const CartBottomDetailsWidget({
    Key key,
    @required CartController con,
  })  : _con = con,
        super(key: key);

  final CartController _con;

  @override
  Widget build(BuildContext context) {
    return _con.carts.isEmpty
        ? SizedBox(height: 0)
        : Stack(
            alignment: AlignmentDirectional.center,
            children: <Widget>[
              SizedBox(
                width: MediaQuery.of(context).size.width - 50,
                child: BlockButtonWidget(
                  text: Text(
                    S.of(context).pharms_cart_bottom_pay,
                    style: TextStyle(color: Theme.of(context).primaryColor),
                  ),
                  color: Theme.of(context).accentColor,
                  onPressed: () {
                    if (settingsRepo.deliveryAddress.value?.address == null) {
                      AwesomeDialog(
                        context: context,
                        dialogType: DialogType.WARNING,
                        tittle: S.of(context).pharms_cart_bottom_address,
                        desc: S.of(context).pharms_cart_bottom_address_msg,
                        dismissOnTouchOutside: false,
                        btnOkColor: Theme.of(context).accentColor,
                        btnOkText: S.of(context).pharms_cart_bottom_address_btn,
                        btnOkOnPress: () {
                          Navigator.of(context)
                              .pushNamed('/Pages', arguments: 2);
                        },
                        btnOkIcon: Icons.location_history,
                      )..show();
                    } else {
                      _con.goCheckout(context);
                    }
                  },
                ),
              ),
              SizedBox(height: 80),
            ],
          );
  }
}
