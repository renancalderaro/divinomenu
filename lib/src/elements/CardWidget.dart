import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../repository/distance_repository.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '../../generated/l10n.dart';
import '../helpers/helper.dart';
import '../kriacoes_agency_modules/minimum_order/models/market.dart';
import '../repository/settings_repository.dart';
import '../elements/EmptyMarketWidget.dart';

// ignore: must_be_immutable
class CardWidget extends StatelessWidget {
  Market market;
  String heroTag;
  int NewRange;

  CardWidget({Key key, this.market, this.heroTag}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    setDoubleDistance(market.distance);

    if (market.deliveryMaxRange < market.deliveryRange) {
      NewRange = market.deliveryRange.toInt();
    } else {
      NewRange = market.deliveryMaxRange.toInt();
    }

    if (market.distance < NewRange) {
      return Container(
        width: 300,
        margin: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: [
            BoxShadow(
                color: Theme.of(context).focusColor.withOpacity(0.1),
                blurRadius: 15,
                offset: Offset(0, 5)),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            // Image of the card
            Stack(
              fit: StackFit.loose,
              alignment: AlignmentDirectional.bottomStart,
              children: <Widget>[
                if (market.closed == true)
                  ShaderMask(
                    blendMode: BlendMode.srcATop,
                    shaderCallback: (Rect bounds) {
                      return LinearGradient(
                        colors: [
                          Color.fromRGBO(0, 0, 0, 0.4),
                          Color.fromRGBO(0, 0, 0, 0.4)
                        ],
                      ).createShader(bounds);
                    },
                    child: Hero(
                      tag: this.heroTag + market.id,
                      child: ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10)),
                        child: CachedNetworkImage(
                          height: 110,
                          width: 110,
                          fit: BoxFit.cover,
                          color: const Color.fromRGBO(0, 0, 0, 1),
                          colorBlendMode: BlendMode.saturation,
                          imageUrl: market.image.url,
                          placeholder: (context, url) => Image.asset(
                            'assets/img/loading.gif',
                            fit: BoxFit.cover,
                            width: 110,
                            height: 110,
                          ),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                      ),
                    ),
                  ),
                if (market.closed == false)
                  Hero(
                    tag: this.heroTag + market.id,
                    child: ClipRRect(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                      child: CachedNetworkImage(
                        height: 110,
                        width: 110,
                        fit: BoxFit.cover,
                        imageUrl: market.image.url,
                        placeholder: (context, url) => Image.asset(
                          'assets/img/loading.gif',
                          fit: BoxFit.cover,
                          width: 110,
                          height: 110,
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                  ),
                if (market.closed == true)
                  Padding(
                    padding: const EdgeInsets.only(left: 120, bottom: 8),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Expanded(
                          flex: 0,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                market.name,
                                overflow: TextOverflow.fade,
                                softWrap: false,
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle2
                                    .merge(
                                        TextStyle(color: Colors.grey.shade500)),
                                maxLines: 2,
                              ),
                              SizedBox(height: 7),
                              Row(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        Helper.skipHtml(market.description),
                                        overflow: TextOverflow.fade,
                                        softWrap: false,
                                        style: Theme.of(context)
                                            .textTheme
                                            .caption
                                            .merge(TextStyle(
                                                color: Colors.grey.shade500)),
                                      ),
                                      Text(
                                        ' • ',
                                        overflow: TextOverflow.fade,
                                        maxLines: 1,
                                        //softWrap: false,
                                      ),
                                    ],
                                  ),
                                  market.distance > market.deliveryRange
                                      ? Helper.getDeliveryFee(
                                          market.deliveryMaxFee,
                                          context,
                                          style: Theme.of(context)
                                              .textTheme
                                              .caption
                                              .merge(TextStyle(
                                                  color: Colors.grey.shade500)),
                                        )
                                      : Helper.getDeliveryFee(
                                          market.deliveryFee,
                                          context,
                                          style: Theme.of(context)
                                              .textTheme
                                              .caption
                                              .merge(TextStyle(
                                                  color: Colors.grey.shade500)),
                                        ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(market.rate,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              .merge(TextStyle(
                                                  color:
                                                      Colors.grey.shade500))),
                                      Icon(
                                        Icons.star,
                                        color: Colors.grey.shade500,
                                        size: 16,
                                      ),
                                    ],
                                  ),
                                  Text(
                                    ' • ',
                                    overflow: TextOverflow.fade,
                                    maxLines: 1,
                                    //softWrap: false,
                                  ),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 0, vertical: 4),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 1, vertical: 3),
                                    child: market.closed
                                        ? Text(
                                            S.of(context).closed,
                                            style: Theme.of(context)
                                                .textTheme
                                                .caption
                                                .merge(TextStyle(
                                                    color: Colors.red,
                                                    fontWeight:
                                                        FontWeight.w500)),
                                          )
                                        : Text(
                                            S.of(context).open,
                                            style: Theme.of(context)
                                                .textTheme
                                                .caption
                                                .merge(TextStyle(
                                                    color: Colors.green)),
                                          ),
                                  ),
                                  Text(
                                    ' • ',
                                    overflow: TextOverflow.fade,
                                    maxLines: 1,
                                    //softWrap: false,
                                  ),
                                  market.distance > 0
                                      ? Text(
                                          Helper.getDistance(
                                              market.distance,
                                              Helper.of(context).trans(
                                                  setting.value.distanceUnit)),
                                          overflow: TextOverflow.fade,
                                          maxLines: 1,
                                          style: Theme.of(context)
                                              .textTheme
                                              .caption
                                              .merge(TextStyle(
                                                  color: Colors.grey.shade500))

                                          //softWrap: false,
                                          )
                                      : SizedBox(height: 0),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        market.address,
                                        overflow: TextOverflow.fade,
                                        softWrap: false,
                                        style: Theme.of(context)
                                            .textTheme
                                            .caption
                                            .merge(TextStyle(
                                                color: Colors.grey.shade500)),
                                        maxLines: 2,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                if (market.closed == false)
                  Padding(
                    padding: const EdgeInsets.only(left: 120, bottom: 8),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Expanded(
                          flex: 0,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                market.name,
                                overflow: TextOverflow.fade,
                                softWrap: false,
                                style: Theme.of(context).textTheme.subtitle2,
                                maxLines: 2,
                              ),
                              SizedBox(height: 7),
                              Row(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        Helper.skipHtml(market.description),
                                        overflow: TextOverflow.fade,
                                        softWrap: false,
                                        style:
                                            Theme.of(context).textTheme.caption,
                                      ),
                                      Text(
                                        ' • ',
                                        overflow: TextOverflow.fade,
                                        maxLines: 1,
                                        //softWrap: false,
                                      ),
                                    ],
                                  ),
                                  market.distance > market.deliveryRange
                                      ? Helper.getDeliveryFee(
                                          market.deliveryMaxFee, context,
                                          style: Theme.of(context)
                                              .textTheme
                                              .caption)
                                      : Helper.getDeliveryFee(
                                          market.deliveryFee, context,
                                          style: Theme.of(context)
                                              .textTheme
                                              .caption),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(market.rate,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              .merge(TextStyle(
                                                  color: Color(0xFFedc347)))),
                                      Icon(
                                        Icons.star,
                                        color: Color(0xFFedc347),
                                        size: 16,
                                      ),
                                    ],
                                  ),
                                  Text(
                                    ' • ',
                                    overflow: TextOverflow.fade,
                                    maxLines: 1,
                                    //softWrap: false,
                                  ),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 0, vertical: 4),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 1, vertical: 3),
                                    child: market.closed
                                        ? Text(
                                            S.of(context).closed,
                                            style: Theme.of(context)
                                                .textTheme
                                                .caption
                                                .merge(TextStyle(
                                                    color: Colors.red)),
                                          )
                                        : Text(
                                            S.of(context).open,
                                            style: Theme.of(context)
                                                .textTheme
                                                .caption
                                                .merge(TextStyle(
                                                    color: Colors.green)),
                                          ),
                                  ),
                                  Text(
                                    ' • ',
                                    overflow: TextOverflow.fade,
                                    maxLines: 1,
                                    //softWrap: false,
                                  ),
                                  market.distance > 0
                                      ? Text(
                                          Helper.getDistance(
                                              market.distance,
                                              Helper.of(context).trans(
                                                  setting.value.distanceUnit)),
                                          overflow: TextOverflow.fade,
                                          maxLines: 1,
                                          style: Theme.of(context)
                                              .textTheme
                                              .caption,

                                          //softWrap: false,
                                        )
                                      : SizedBox(height: 0),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        market.address,
                                        overflow: TextOverflow.fade,
                                        softWrap: false,
                                        style:
                                            Theme.of(context).textTheme.caption,
                                        maxLines: 2,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
              ],
            ),
          ],
        ),
      );
    } else {
      return EmptyMarketWidget();
    }
  }
}
