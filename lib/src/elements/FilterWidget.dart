import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_html/style.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/filter_controller.dart';
import '../elements/CircularLoadingWidget.dart';
import '../models/filter.dart';

class FilterWidget extends StatefulWidget {
  final ValueChanged<Filter> onFilter;

  FilterWidget({Key key, this.onFilter}) : super(key: key);

  @override
  _FilterWidgetState createState() => _FilterWidgetState();
}

class _FilterWidgetState extends StateMVC<FilterWidget> {
  FilterController _con;

  _FilterWidgetState() : super(FilterController()) {
    _con = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 10, top: 50),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.arrow_back),
                  color: Theme.of(context).hintColor,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                MaterialButton(
                  onPressed: () {
                    _con.clearFilter();
                  },
                  child: Text(
                    S.of(context).clear,
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                )
              ],
            ),
          ),
          Expanded(
            child: ListView(
              primary: true,
              shrinkWrap: true,
              children: <Widget>[
                _con.fields.isEmpty
                    ? CircularLoadingWidget(height: 100)
                    : ExpansionTile(
                        title: Text(
                          S.of(context).fields,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 16),
                        ),
                        children: List.generate(_con.fields.length, (index) {
                          return CheckboxListTile(
                            controlAffinity: ListTileControlAffinity.trailing,
                            value: _con.fields.elementAt(index).selected,
                            onChanged: (value) {
                              _con.onChangeFieldsFilter(index);
                            },
                            title: Text(
                              _con.fields.elementAt(index).name,
                              overflow: TextOverflow.fade,
                              softWrap: false,
                              maxLines: 1,
                            ),
                          );
                        }),
                        initiallyExpanded: true,
                      ),
              ],
            ),
          ),
          SizedBox(height: 15),
          MaterialButton(
            elevation: 0,
            onPressed: () {
              _con.saveFilter().whenComplete(() {
                widget.onFilter(_con.filter);
              });
            },
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
            color: Theme.of(context).accentColor,
            shape: StadiumBorder(),
            child: Text(
              S.of(context).apply_filters,
              textAlign: TextAlign.start,
              style: TextStyle(color: Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(height: 15)
        ],
      ),
    );
  }
}
