import 'dart:async';

import 'package:Divino/generated/l10n.dart';
import 'package:flutter/material.dart';

class EmptyMarketWidget extends StatefulWidget {
  EmptyMarketWidget({
    Key key,
  }) : super(key: key);

  @override
  _EmptyMarketWidgetState createState() => _EmptyMarketWidgetState();
}

class _EmptyMarketWidgetState extends State<EmptyMarketWidget> {
  bool loading = true;

  @override
  void initState() {
    Timer(Duration(seconds: 5), () {
      if (mounted) {
        setState(() {
          loading = false;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          alignment: AlignmentDirectional.topCenter,
          padding: EdgeInsets.symmetric(vertical: 30, horizontal: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              /*Stack(
                children: <Widget>[
                   Container(
                    width: 60,
                    height: 60,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        gradient: LinearGradient(
                            begin: Alignment.bottomLeft,
                            end: Alignment.topRight,
                            colors: [
                              Theme.of(context).focusColor.withOpacity(0.7),
                              Theme.of(context).focusColor.withOpacity(0.05),
                            ])),
                    child: Icon(
                      Icons.pin_drop,
                      color: Theme.of(context).scaffoldBackgroundColor,
                      size: 40,
                    ),
                  ),
                  Positioned(
                    right: -30,
                    bottom: -50,
                    child: Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                        color: Theme.of(context)
                            .scaffoldBackgroundColor
                            .withOpacity(0.15),
                        borderRadius: BorderRadius.circular(150),
                      ),
                    ),
                  ),
                  Positioned(
                    left: -20,
                    top: -50,
                    child: Container(
                      width: 120,
                      height: 120,
                      decoration: BoxDecoration(
                        color: Theme.of(context)
                            .scaffoldBackgroundColor
                            .withOpacity(0.15),
                        borderRadius: BorderRadius.circular(150),
                      ),
                    ),
                  )
                ],
              ),*/
              SizedBox(height: 5),
              Opacity(
                opacity: 0.6,
                child: Text(
                  S.of(context).pharms_partner_not_found,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline3.merge(
                      TextStyle(fontWeight: FontWeight.w300, fontSize: 16)),
                ),
              ),
              SizedBox(height: 50),
            ],
          ),
        ),
      ],
    );
  }
}
