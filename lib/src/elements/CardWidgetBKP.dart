import 'package:flutter/widgets.dart';

import '../repository/distance_repository.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '../../generated/l10n.dart';
import '../helpers/helper.dart';
import '../kriacoes_agency_modules/minimum_order/models/market.dart';
import '../repository/settings_repository.dart';
import 'EmptyCardWidget.dart';

// ignore: must_be_immutable
class CardWidgetBKP extends StatelessWidget {
  Market market;
  String heroTag;
  int NewRange;

  CardWidgetBKP({Key key, this.market, this.heroTag}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    setDoubleDistance(market.distance);

    if(market.deliveryMaxRange < market.deliveryRange){
      NewRange = market.deliveryRange.toInt();
    }else{
      NewRange = market.deliveryMaxRange.toInt();
    }

    if (market.distance < NewRange) {
      return Container(
        width: 300,
        margin: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: [
            BoxShadow(
                color: Theme.of(context).focusColor.withOpacity(0.1),
                blurRadius: 15,
                offset: Offset(0, 5)),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            // Image of the card
            Stack(
              fit: StackFit.loose,
              alignment: AlignmentDirectional.bottomStart,
              children: <Widget>[
                Hero(
                  tag: this.heroTag + market.id,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10)),
                    child: CachedNetworkImage(
                      height: 110,
                      width: 110,
                      fit: BoxFit.cover,
                      imageUrl: market.image.url,
                      placeholder: (context, url) => Image.asset(
                        'assets/img/loading.gif',
                        fit: BoxFit.cover,
                        width: 110,
                        height: 110,
                      ),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 120, bottom: 8),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Expanded(
                        flex: 0,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              market.name,
                              overflow: TextOverflow.fade,
                              softWrap: false,
                              style: Theme.of(context).textTheme.subtitle2,
                              maxLines: 2,
                            ),
                            SizedBox(height: 7),
                            Row(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      Helper.skipHtml(market.description),
                                      overflow: TextOverflow.fade,
                                      softWrap: false,
                                      style: Theme.of(context).textTheme.caption,
                                    ),

                                    Text(
                                      ' • ',
                                      overflow: TextOverflow.fade,
                                      maxLines: 1,
                                      //softWrap: false,
                                    ),
                                  ],
                                ),

                                market.distance > market.deliveryRange
                                ?
                                Helper.getDeliveryFee(market.deliveryMaxFee, context, style: Theme.of(context).textTheme.caption)
                                :
                                Helper.getDeliveryFee(market.deliveryFee, context, style: Theme.of(context).textTheme.caption),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(market.rate,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1
                                            .merge(TextStyle(
                                                color: Color(0xFFedc347)))),
                                    Icon(
                                      Icons.star,
                                      color: Color(0xFFedc347),
                                      size: 16,
                                    ),
                                  ],
                                ),
                                Text(
                                  ' • ',
                                  overflow: TextOverflow.fade,
                                  maxLines: 1,
                                  //softWrap: false,
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 0, vertical: 4),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 1, vertical: 3),
                                  child: market.closed
                                      ? Text(
                                          S.of(context).closed,
                                          style: Theme.of(context)
                                              .textTheme
                                              .caption
                                              .merge(
                                                  TextStyle(color: Colors.red)),
                                        )
                                      : Text(
                                          S.of(context).open,
                                          style: Theme.of(context)
                                              .textTheme
                                              .caption
                                              .merge(TextStyle(
                                                  color: Colors.green)),
                                        ),
                                ),
                                Text(
                                  ' • ',
                                  overflow: TextOverflow.fade,
                                  maxLines: 1,
                                  //softWrap: false,
                                ),
                                market.distance > 0
                                    ? Text(
                                        Helper.getDistance(
                                            market.distance,
                                            Helper.of(context).trans(
                                                setting.value.distanceUnit)),
                                        overflow: TextOverflow.fade,
                                        maxLines: 1,
                                        style:
                                            Theme.of(context).textTheme.caption,

                                        //softWrap: false,
                                      )
                                    : SizedBox(height: 0),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      market.address,
                                      overflow: TextOverflow.fade,
                                      softWrap: false,
                                      style:
                                          Theme.of(context).textTheme.caption,
                                      maxLines: 2,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      );
    } else {
      return SizedBox();
      return EmptyCardWidget();



      //return EmptyCardWidget();
      // Container(
      //   height: 50,
      //   width: 300,
      //   margin: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
      //
      // );

      // ListView.builder(
      //   shrinkWrap: true,
      //   primary: false,
      //   scrollDirection: Axis.vertical,
      //   itemCount: 1,
      //   itemBuilder: (context, index) {
      //     return GestureDetector(
      //       onTap: () {},
      //         child: Column(
      //           children: <Widget>[
      //             SizedBox(height: 25),
      //             Container(
      //               alignment: AlignmentDirectional.center,
      //               padding: EdgeInsets.symmetric(horizontal: 30),
      //               child: Column(
      //                 mainAxisAlignment: MainAxisAlignment.center,
      //                 children: <Widget>[
      //                   Stack(
      //                     children: <Widget>[
      //                       Container(
      //                         width: 80,
      //                         height: 80,
      //                         decoration: BoxDecoration(
      //                             shape: BoxShape.circle,
      //                             gradient: LinearGradient(begin: Alignment.bottomLeft, end: Alignment.topRight, colors: [
      //                               Theme.of(context).accentColor.withOpacity(0.7),
      //                               Theme.of(context).accentColor.withOpacity(0.05),
      //                             ])),
      //                         child: Icon(
      //                           Icons.search_off,
      //                           color: Theme.of(context).scaffoldBackgroundColor,
      //                           size: 50,
      //                         ),
      //                       ),
      //                       Positioned(
      //                         right: -30,
      //                         bottom: -50,
      //                         child: Container(
      //                           width: 80,
      //                           height: 80,
      //                           decoration: BoxDecoration(
      //                             color: Theme.of(context).scaffoldBackgroundColor.withOpacity(0.15),
      //                             borderRadius: BorderRadius.circular(150),
      //                           ),
      //                         ),
      //                       ),
      //                       Positioned(
      //                         left: -20,
      //                         top: -50,
      //                         child: Container(
      //                           width: 120,
      //                           height: 120,
      //                           decoration: BoxDecoration(
      //                             color: Theme.of(context).scaffoldBackgroundColor.withOpacity(0.15),
      //                             borderRadius: BorderRadius.circular(150),
      //                           ),
      //                         ),
      //                       )
      //                     ],
      //                   ),
      //                   SizedBox(height: 15),
      //                   Opacity(
      //                     opacity: 1.0,
      //                     child: Text(
      //                       S.of(context).pharms_partner_not_found,
      //                       textAlign: TextAlign.center,
      //                       style: Theme.of(context).textTheme.bodyText2,
      //                     ),
      //                   ),
      //                   SizedBox(height: 25),
      //                 ],
      //               ),
      //             ),
      //           ],
      //         )
      //     );
      //   },
      // );
    }
  }
}
