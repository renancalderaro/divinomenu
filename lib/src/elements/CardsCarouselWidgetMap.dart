import 'CardWidgetMap.dart';
import 'package:flutter/material.dart';

import '../elements/CardsCarouselLoaderWidget.dart';
import '../kriacoes_agency_modules/minimum_order/models/market.dart';
import '../models/route_argument.dart';

// ignore: must_be_immutable
class CardsCarouselWidgetMap extends StatefulWidget {
  List<Market> marketsList;
  String heroTag;

  CardsCarouselWidgetMap({Key key, this.marketsList, this.heroTag})
      : super(key: key);

  @override
  _CardsCarouselWidgetMapState createState() => _CardsCarouselWidgetMapState();
}

class _CardsCarouselWidgetMapState extends State<CardsCarouselWidgetMap> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.marketsList.isEmpty
        ? CardsCarouselLoaderWidget()
        : Container(
            height: 288,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: widget.marketsList.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.of(context).pushNamed('/Details',
                        arguments: RouteArgument(
                          id: '0',
                          param: widget.marketsList.elementAt(index).id,
                          heroTag: widget.heroTag,
                        ));
                  },
                  child: CardWidgetMap(
                      market: widget.marketsList.elementAt(index),
                      heroTag: widget.heroTag),
                );
              },
            ),
          );
  }
}
