import '../../generated/l10n.dart';
import 'package:flutter/material.dart';
import '../helpers/app_config.dart' as config;

class RoutErrorWidget extends StatefulWidget {
  RoutErrorWidget({
    Key key,
  }) : super(key: key);

  @override
  _RoutErrorWidgetState createState() => _RoutErrorWidgetState();
}

class _RoutErrorWidgetState extends State<RoutErrorWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: AlignmentDirectional.center,
      padding: EdgeInsets.symmetric(horizontal: 30),
      height: config.App(context).appHeight(70),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                width: 150,
                height: 150,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    gradient: LinearGradient(
                        begin: Alignment.bottomLeft,
                        end: Alignment.topRight,
                        colors: [
                          Theme.of(context).focusColor.withOpacity(1),
                          Theme.of(context).focusColor.withOpacity(0.05),
                        ])),
                child: Icon(
                  Icons.error,
                  color: Theme.of(context).scaffoldBackgroundColor,
                  size: 70,
                ),
              ),
              Positioned(
                right: -30,
                bottom: -50,
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    color: Theme.of(context)
                        .scaffoldBackgroundColor
                        .withOpacity(0.15),
                    borderRadius: BorderRadius.circular(150),
                  ),
                ),
              ),
              Positioned(
                left: -20,
                top: -50,
                child: Container(
                  width: 120,
                  height: 120,
                  decoration: BoxDecoration(
                    color: Theme.of(context)
                        .scaffoldBackgroundColor
                        .withOpacity(0.15),
                    borderRadius: BorderRadius.circular(150),
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 15),
          Opacity(
            opacity: 1,
            child: Text(
              S.of(context).pharms_route_error_title,
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .headline3
                  .merge(TextStyle(fontWeight: FontWeight.w300)),
            ),
          ),
          FlatButton(
            onPressed: () {},
            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 25),
            shape: StadiumBorder(),
            child: Text(
              S.of(context).pharms_route_error_msg,
              style:
                  TextStyle(fontSize: 17, color: Theme.of(context).focusColor),
            ),
          ),
          SizedBox(height: 50),
          FlatButton(
            onPressed: () {
              Navigator.of(context)
                  .pushReplacementNamed('/Pages', arguments: 2);
            },
            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 70),
            color: Theme.of(context).accentColor.withOpacity(1),
            shape: StadiumBorder(),
            child: Text(
              S.of(context).pharms_route_error_btn,
              style: Theme.of(context).textTheme.headline6.merge(
                  TextStyle(color: Theme.of(context).scaffoldBackgroundColor)),
            ),
          ),
        ],
      ),
    );
  }
}
