import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/style.dart';
import '../helpers/helper.dart';
import '../models/product.dart';
import '../models/route_argument.dart';

class ProductItemSearchWidget extends StatelessWidget {
  final String heroTag;
  final Product product;

  const ProductItemSearchWidget({Key key, this.product, this.heroTag})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Theme.of(context).accentColor,
      focusColor: Theme.of(context).accentColor,
      highlightColor: Theme.of(context).primaryColor,
      onTap: () {
        Navigator.of(context).pushNamed('/Product',
            arguments: RouteArgument(id: product.id, heroTag: this.heroTag));
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor.withOpacity(0.9),
          boxShadow: [
            BoxShadow(
                color: Theme.of(context).focusColor.withOpacity(0.1),
                blurRadius: 5,
                offset: Offset(0, 2)),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Hero(
              tag: heroTag + product.id,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                child: CachedNetworkImage(
                  height: 90,
                  width: 90,
                  fit: BoxFit.cover,
                  imageUrl: product.image.thumb,
                  placeholder: (context, url) => Image.asset(
                    'assets/img/loading.gif',
                    fit: BoxFit.cover,
                    height: 90,
                    width: 90,
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
            ),
            SizedBox(width: 15),
            Flexible(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        product.name,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: Theme.of(context)
                            .textTheme
                            .subtitle1
                            .merge(TextStyle(fontSize: 15)),
                      ),
                      SizedBox(height: 5),
                      Text(
                        product.description,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 4,
                        style: Theme.of(context).textTheme.caption,
                      ),
                      SizedBox(height: 10),
                      Row(
                        children: <Widget>[
                          Helper.getPrice(product.price, context,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .merge(TextStyle(
                                      color: Theme.of(context).accentColor,
                                      fontSize: 17.5))),
                          SizedBox(width: 10),
                          product.discountPrice > 0
                              ? Helper.getPrice(product.discountPrice, context,
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText2
                                      .merge(TextStyle(
                                          decoration:
                                              TextDecoration.lineThrough,
                                          color: Colors.grey)))
                              : SizedBox(height: 0),
                        ],
                      ),
                    ],
                  )),
                  SizedBox(width: 8),
                  /* Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      product.discountPrice > 0
                          ? Helper.getPrice(product.discountPrice, context,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText2
                                  .merge(TextStyle(
                                      decoration: TextDecoration.lineThrough,
                                      color: Colors.grey)))
                          : SizedBox(height: 0),
                      Helper.getPrice(product.price, context,
                          style: Theme.of(context).textTheme.headline4.merge(
                              TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontSize: 18))),
                    ],
                  ),*/
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
