import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../../../../generated/l10n.dart';
import '../../../controllers/market_controller.dart';
import '../../../elements/CircularLoadingWidget.dart';
import '../../../elements/GalleryCarouselWidget.dart';
import '../../../elements/ReviewsListWidget.dart';
import '../../../elements/ShoppingCartFloatButtonWidget.dart';
import '../../../helpers/helper.dart';
import '../models/market.dart';
import '../../../models/route_argument.dart';
import '../../../repository/settings_repository.dart';

class MarketWidget extends StatefulWidget {
  final RouteArgument routeArgument;
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  MarketWidget({Key key, this.parentScaffoldKey, this.routeArgument})
      : super(key: key);

  @override
  _MarketWidgetState createState() {
    return _MarketWidgetState();
  }
}

class _MarketWidgetState extends StateMVC<MarketWidget> {
  MarketController _con;

  _MarketWidgetState() : super(MarketController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.market = widget.routeArgument.param as Market;
    _con.listenForGalleries(_con.market.id);
    _con.listenForFeaturedProducts(_con.market.id);
    _con.listenForMarketReviews(id: _con.market.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_con.market.closed == true) {
      return Scaffold(
          key: _con.scaffoldKey,
          body: RefreshIndicator(
            onRefresh: _con.refreshMarket,
            child: _con.market == null
                ? CircularLoadingWidget(height: 500)
                : Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      CustomScrollView(
                        primary: true,
                        shrinkWrap: false,
                        slivers: <Widget>[
                          SliverAppBar(
                            backgroundColor:
                                Theme.of(context).primaryColor.withOpacity(0.9),
                            expandedHeight: 300,
                            elevation: 0,
                            automaticallyImplyLeading: false,
                            leading: new IconButton(
                              icon: FloatingActionButton(
                                onPressed: () {
                                  // Add your onPressed code here!
                                  Navigator.of(context).pushReplacementNamed(
                                      '/Pages',
                                      arguments: 2);
                                },
                                child: const Icon(Icons.arrow_back),
                                backgroundColor: Theme.of(context).accentColor,
                              ),

                              // icon: new Icon(Icons.arrow_back,
                              //     color: Theme.of(context).accentColor),
                              // onPressed: () => widget
                              //     .parentScaffoldKey.currentState
                              //     .openDrawer(),
                            ),
                            flexibleSpace: FlexibleSpaceBar(
                              collapseMode: CollapseMode.parallax,
                              background: Hero(
                                tag: (widget?.routeArgument?.heroTag ?? '') +
                                    _con.market.id,
                                child: CachedNetworkImage(
                                  fit: BoxFit.cover,
                                  imageUrl: _con.market.image.url,
                                  color: const Color.fromRGBO(0, 0, 0, 1),
                                  colorBlendMode: BlendMode.saturation,
                                  placeholder: (context, url) => Image.asset(
                                    'assets/img/loading.gif',
                                    fit: BoxFit.cover,
                                  ),
                                  errorWidget: (context, url, error) =>
                                      Icon(Icons.error),
                                ),
                              ),
                            ),
                          ),
                          SliverToBoxAdapter(
                            child: Wrap(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 20, left: 20, bottom: 00, top: 25),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          _con.market?.name ?? '',
                                          overflow: TextOverflow.fade,
                                          softWrap: false,
                                          maxLines: 2,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 20, left: 20, bottom: 15, top: 0),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          _con.market?.address ?? '',
                                          overflow: TextOverflow.fade,
                                          softWrap: false,
                                          maxLines: 2,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 20, left: 20, bottom: 10, top: 0),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          'Este estabelecimento está fechado',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              .merge(TextStyle(
                                                  color: Colors.red,
                                                  fontSize: 16)),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Row(
                                  children: <Widget>[
                                    SizedBox(width: 20),
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 0, vertical: 3),
                                      child: Text(
                                        Helper.getDistance(
                                            _con.market.distance,
                                            Helper.of(context).trans(
                                                setting.value.distanceUnit)),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                    ),
                                    Text(
                                      ' • ',
                                      overflow: TextOverflow.fade,
                                      maxLines: 1,
                                    ),
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 0, vertical: 3),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(_con.market.rate,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1
                                                  .merge(TextStyle(
                                                    color: Colors.black,
                                                  ))),
                                          Icon(
                                            Icons.star,
                                            color: Colors.black,
                                            size: 16,
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(width: 10),
                                    Expanded(child: SizedBox(height: 0)),
                                    SizedBox(width: 10),
                                    SizedBox(width: 20),
                                  ],
                                ),

                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 20, left: 20, bottom: 5, top: 5),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            Helper.skipHtml(
                                                _con.market.description),
                                            overflow: TextOverflow.fade,
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1,
                                          ),
                                        ],
                                      ),
                                      Text(
                                        ' • ',
                                        overflow: TextOverflow.fade,
                                        maxLines: 1,
                                      ),
                                      Text(
                                        S.of(context).pharms_delivery_home,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                      _con.market.distance >
                                              _con.market.deliveryRange
                                          ? Helper.getDeliveryFee(
                                              _con.market.deliveryMaxFee,
                                              context,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1)
                                          : Helper.getDeliveryFee(
                                              _con.market.deliveryFee, context,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1)
                                    ],
                                  ),
                                ),
                                //AREA
                                SizedBox(height: 40),
                                if (_con.market?.minimumOrder.toString() !=
                                    "0.0")
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20),
                                    child: ListTile(
                                      dense: true,
                                      contentPadding:
                                          EdgeInsets.symmetric(vertical: 0),
                                      title: Text(
                                          S
                                                  .of(context)
                                                  .kriacoes_agency_module_minimum_order +
                                              ": " +
                                              setting.value?.defaultCurrency +
                                              " " +
                                              _con.market?.minimumOrder
                                                  .toString(),
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              .merge(TextStyle(
                                                color: Colors.black,
                                              ))),
                                    ),
                                  ),

                                ImageThumbCarouselWidget(
                                    galleriesList: _con.galleries),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 12),
                                  child: Helper.applyHtml(
                                      context, _con.market.information),
                                ),
                                /*  Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 20),
                                  margin:
                                      const EdgeInsets.symmetric(vertical: 5),
                                  color: Theme.of(context).primaryColor,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          _con.market.address ?? '',
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 2,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1,
                                        ),
                                      ),
                                      SizedBox(width: 10),
                                    ],
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 20),
                                  margin:
                                      const EdgeInsets.symmetric(vertical: 5),
                                  color: Theme.of(context).primaryColor,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          '${_con.market.phone} \n${_con.market.mobile}',
                                          overflow: TextOverflow.ellipsis,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1,
                                        ),
                                      ),
                                      SizedBox(width: 10),
                                    ],
                                  ),
                                ),*/

                                SizedBox(height: 100),
                                _con.reviews.isEmpty
                                    ? Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 10, horizontal: 20),
                                        child: ListTile(
                                          dense: true,
                                          contentPadding:
                                              EdgeInsets.symmetric(vertical: 0),
                                          leading: Icon(
                                            Icons.recent_actors,
                                            color: Theme.of(context).hintColor,
                                          ),
                                          title: Text(
                                            S.of(context).what_they_say,
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline4,
                                          ),
                                        ),
                                      )
                                    : Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 10, horizontal: 20),
                                        child: ListTile(
                                          dense: true,
                                          contentPadding:
                                              EdgeInsets.symmetric(vertical: 0),
                                          leading: Icon(
                                            Icons.recent_actors,
                                            color: Theme.of(context).hintColor,
                                          ),
                                          title: Text(
                                            S.of(context).what_they_say,
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline4,
                                          ),
                                        ),
                                      ),
                                _con.reviews.isEmpty
                                    ? Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20, vertical: 10),
                                        child: ReviewsListWidget(
                                            reviewsList: _con.reviews),
                                      )
                                    : Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20, vertical: 10),
                                        child: ReviewsListWidget(
                                            reviewsList: _con.reviews),
                                      ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Positioned(
                        top: 32,
                        right: 20,
                        child: ShoppingCartFloatButtonWidget(
                            iconColor: Theme.of(context).primaryColor,
                            labelColor: Theme.of(context).hintColor,
                            routeArgument: RouteArgument(
                                id: '0',
                                param: _con.market.id,
                                heroTag: 'home_slide')),
                      ),
                    ],
                  ),
          ));
    } else {
      return Scaffold(
          key: _con.scaffoldKey,
          body: RefreshIndicator(
            onRefresh: _con.refreshMarket,
            child: _con.market == null
                ? CircularLoadingWidget(height: 500)
                : Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      CustomScrollView(
                        primary: true,
                        shrinkWrap: false,
                        slivers: <Widget>[
                          SliverAppBar(
                            backgroundColor:
                                Theme.of(context).primaryColor.withOpacity(0.9),
                            expandedHeight: 300,
                            elevation: 0,
                            automaticallyImplyLeading: false,
                            leading: new IconButton(
                              icon: FloatingActionButton(
                                onPressed: () {
                                  // Add your onPressed code here!
                                  Navigator.of(context).pushReplacementNamed(
                                      '/Pages',
                                      arguments: 2);
                                },
                                child: const Icon(Icons.arrow_back),
                                backgroundColor: Theme.of(context).accentColor,
                              ),

                              // icon: new Icon(Icons.arrow_back,
                              //     color: Theme.of(context).accentColor),
                              // onPressed: () => widget
                              //     .parentScaffoldKey.currentState
                              //     .openDrawer(),
                            ),
                            flexibleSpace: FlexibleSpaceBar(
                              collapseMode: CollapseMode.parallax,
                              background: Hero(
                                tag: (widget?.routeArgument?.heroTag ?? '') +
                                    _con.market.id,
                                child: CachedNetworkImage(
                                  fit: BoxFit.cover,
                                  imageUrl: _con.market.image.url,
                                  placeholder: (context, url) => Image.asset(
                                    'assets/img/loading.gif',
                                    fit: BoxFit.cover,
                                  ),
                                  errorWidget: (context, url, error) =>
                                      Icon(Icons.error),
                                ),
                              ),
                            ),
                          ),
                          SliverToBoxAdapter(
                            child: Wrap(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 20, left: 20, bottom: 5, top: 25),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          _con.market?.name ?? '',
                                          overflow: TextOverflow.fade,
                                          softWrap: false,
                                          maxLines: 2,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 20, left: 20, bottom: 15, top: 0),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          _con.market?.address ?? '',
                                          overflow: TextOverflow.fade,
                                          softWrap: false,
                                          maxLines: 2,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Row(
                                  children: <Widget>[
                                    SizedBox(width: 20),
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 0, vertical: 3),
                                      child: Text(
                                        Helper.getDistance(
                                            _con.market.distance,
                                            Helper.of(context).trans(
                                                setting.value.distanceUnit)),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                    ),
                                    Text(
                                      ' • ',
                                      overflow: TextOverflow.fade,
                                      maxLines: 1,
                                    ),
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 0, vertical: 3),
                                      child: _con.market.closed
                                          ? Text(
                                              S.of(context).closed,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1
                                                  .merge(TextStyle(
                                                      color: Colors.red)),
                                            )
                                          : Text(
                                              S.of(context).open,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1
                                                  .merge(TextStyle(
                                                      color: Colors.green)),
                                            ),
                                    ),
                                    Text(
                                      ' • ',
                                      overflow: TextOverflow.fade,
                                      maxLines: 1,
                                    ),
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 0, vertical: 3),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(_con.market.rate,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1
                                                  .merge(TextStyle(
                                                    color: Color(0xFFffbb2f),
                                                  ))),
                                          Icon(
                                            Icons.star,
                                            color: Color(0xFFffbb2f),
                                            size: 16,
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(width: 10),
                                    Expanded(child: SizedBox(height: 0)),
                                    SizedBox(width: 10),
                                    SizedBox(width: 20),
                                  ],
                                ),

                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 20, left: 20, bottom: 5, top: 5),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            Helper.skipHtml(
                                                _con.market.description),
                                            overflow: TextOverflow.fade,
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1,
                                          ),
                                        ],
                                      ),
                                      Text(
                                        ' • ',
                                        overflow: TextOverflow.fade,
                                        maxLines: 1,
                                      ),
                                      Text(
                                        S.of(context).pharms_delivery_home,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                      _con.market.distance >
                                              _con.market.deliveryRange
                                          ? Helper.getDeliveryFee(
                                              _con.market.deliveryMaxFee,
                                              context,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1)
                                          : Helper.getDeliveryFee(
                                              _con.market.deliveryFee, context,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1)
                                    ],
                                  ),
                                ),
                                //AREA
                                SizedBox(height: 40),
                                if (_con.market?.minimumOrder.toString() !=
                                    "0.0")
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20),
                                    child: ListTile(
                                      dense: true,
                                      contentPadding:
                                          EdgeInsets.symmetric(vertical: 0),
                                      title: Text(
                                          S
                                                  .of(context)
                                                  .kriacoes_agency_module_minimum_order +
                                              ": " +
                                              setting.value?.defaultCurrency +
                                              " " +
                                              _con.market?.minimumOrder
                                                  .toString(),
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              .merge(TextStyle(
                                                color: Theme.of(context)
                                                    .accentColor,
                                              ))),
                                    ),
                                  ),

                                ImageThumbCarouselWidget(
                                    galleriesList: _con.galleries),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 12),
                                  child: Helper.applyHtml(
                                      context, _con.market.information),
                                ),
                                /* Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 20),
                                  margin:
                                      const EdgeInsets.symmetric(vertical: 5),
                                  color: Theme.of(context).primaryColor,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          _con.market.address ?? '',
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 2,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1,
                                        ),
                                      ),
                                      SizedBox(width: 10),
                                    ],
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 20),
                                  margin:
                                      const EdgeInsets.symmetric(vertical: 5),
                                  color: Theme.of(context).primaryColor,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          '${_con.market.phone} \n${_con.market.mobile}',
                                          overflow: TextOverflow.ellipsis,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1,
                                        ),
                                      ),
                                      SizedBox(width: 10),
                                    ],
                                  ),
                                ),*/

                                SizedBox(height: 100),
                                _con.reviews.isEmpty
                                    ? Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 10, horizontal: 20),
                                        child: ListTile(
                                          dense: true,
                                          contentPadding:
                                              EdgeInsets.symmetric(vertical: 0),
                                          leading: Icon(
                                            Icons.recent_actors,
                                            color: Theme.of(context).hintColor,
                                          ),
                                          title: Text(
                                            S.of(context).what_they_say,
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline4,
                                          ),
                                        ),
                                      )
                                    : Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 10, horizontal: 20),
                                        child: ListTile(
                                          dense: true,
                                          contentPadding:
                                              EdgeInsets.symmetric(vertical: 0),
                                          leading: Icon(
                                            Icons.recent_actors,
                                            color: Theme.of(context).hintColor,
                                          ),
                                          title: Text(
                                            S.of(context).what_they_say,
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline4,
                                          ),
                                        ),
                                      ),
                                _con.reviews.isEmpty
                                    ? Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20, vertical: 10),
                                        child: ReviewsListWidget(
                                            reviewsList: _con.reviews))
                                    : Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20, vertical: 10),
                                        child: ReviewsListWidget(
                                            reviewsList: _con.reviews),
                                      ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Positioned(
                        top: 32,
                        right: 20,
                        child: ShoppingCartFloatButtonWidget(
                            iconColor: Theme.of(context).primaryColor,
                            labelColor: Theme.of(context).hintColor,
                            routeArgument: RouteArgument(
                                id: '0',
                                param: _con.market.id,
                                heroTag: 'home_slide')),
                      ),
                    ],
                  ),
          ));
    }
  }
}
