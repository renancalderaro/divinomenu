import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../../generated/l10n.dart';
import '../controllers/cart_controller.dart';
import '../../../elements/CartBottomDetailsWidget.dart';
import '../../../elements/CartItemWidget.dart';
import '../../../elements/EmptyCartWidget.dart';
import '../../../helpers/helper.dart';
import '../../../models/route_argument.dart';
import '../../../repository/settings_repository.dart';

class CartWidget extends StatefulWidget {
  final RouteArgument routeArgument;

  CartWidget({Key key, this.routeArgument}) : super(key: key);

  @override
  _CartWidgetState createState() => _CartWidgetState();
}

class _CartWidgetState extends StateMVC<CartWidget> {
  CartController _con;

  _CartWidgetState() : super(CartController()) {
    _con = controller;
  }

  double _Distance = null;

  @override
  void initState() {
    _getDistance();
    _con.listenForCarts();
    super.initState();
  }

  _getDistance() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _Distance = (prefs.getDouble('double_distance') ?? '');
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        key: _con.scaffoldKey,
        bottomNavigationBar: CartBottomDetailsWidget(con: _con),
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              if (widget.routeArgument != null) {
                Navigator.of(context).pushReplacementNamed(
                    widget.routeArgument.param,
                    arguments: RouteArgument(id: widget.routeArgument.id));
              } else {
                Navigator.of(context)
                    .pushReplacementNamed('/Pages', arguments: 2);
              }
            },
            icon: Icon(Icons.arrow_back),
            color: Theme.of(context).hintColor,
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          title: Text(
            S.of(context).cart,
            style: Theme.of(context)
                .textTheme
                .headline6
                .merge(TextStyle(letterSpacing: 1.3)),
          ),
        ),
        body: RefreshIndicator(
          onRefresh: _con.refreshCarts,
          child: _con.carts.isEmpty
              ? EmptyCartWidget()
              : Stack(
                  alignment: AlignmentDirectional.bottomCenter,
                  children: [
                    ListView(
                      primary: true,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 10),
                          child: ListTile(
                            contentPadding: EdgeInsets.symmetric(vertical: 0),
                            title: Text(
                              S.of(context).products,
                              textAlign: TextAlign.center,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            subtitle: Text(
                                S.of(context).pharms_drag_product_remove,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.center,
                                style: Theme.of(context).textTheme.caption),
                          ),
                        ),
                        ListView.separated(
                          padding: EdgeInsets.only(top: 5, bottom: 50),
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          primary: false,
                          itemCount: _con.carts.length,
                          separatorBuilder: (context, index) {
                            return SizedBox(height: 15);
                          },
                          itemBuilder: (context, index) {
                            return CartItemWidget(
                              cart: _con.carts.elementAt(index),
                              heroTag: 'cart',
                              increment: () {
                                _con.incrementQuantity(
                                    _con.carts.elementAt(index));
                              },
                              decrement: () {
                                _con.decrementQuantity(
                                    _con.carts.elementAt(index));
                              },
                              onDismissed: () {
                                _con.removeFromCart(
                                    _con.carts.elementAt(index));
                              },
                            );
                          },
                        ),
                        Container(
                          padding: const EdgeInsets.all(18),
                          margin: EdgeInsets.only(bottom: 15),
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20)),
                              boxShadow: [
                                BoxShadow(
                                    color: Theme.of(context)
                                        .focusColor
                                        .withOpacity(0.15),
                                    offset: Offset(0, 2),
                                    blurRadius: 5.0)
                              ]),
                          child: TextField(
                            keyboardType: TextInputType.text,
                            onSubmitted: (String value) {
                              _con.doApplyCoupon(value);
                            },
                            cursorColor: Theme.of(context).accentColor,
                            controller: TextEditingController()
                              ..text = coupon?.code ?? '',
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 15),
                              floatingLabelBehavior:
                                  FloatingLabelBehavior.always,
                              hintStyle: Theme.of(context).textTheme.bodyText1,
                              suffixText: coupon?.valid == null
                                  ? S.of(context).pharms_coupon_check
                                  : (coupon.valid
                                      ? S.of(context).validCouponCode
                                      : S.of(context).invalidCouponCode),
                              suffixStyle: Theme.of(context)
                                  .textTheme
                                  .caption
                                  .merge(TextStyle(
                                      color: _con.getCouponIconColor())),
                              suffixIcon: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 0),
                                child: Icon(
                                  Icons.arrow_right,
                                  color: _con.getCouponIconColor(),
                                  size: 50,
                                ),
                              ),
                              hintText: S.of(context).haveCouponCode,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                  borderSide: BorderSide(
                                      color: Theme.of(context)
                                          .focusColor
                                          .withOpacity(0.2))),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                  borderSide: BorderSide(
                                      color: Theme.of(context)
                                          .focusColor
                                          .withOpacity(0.5))),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                  borderSide: BorderSide(
                                      color: Theme.of(context)
                                          .focusColor
                                          .withOpacity(0.2))),
                            ),
                          ),
                        ),
                        Container(
                          height: 270,
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 15),
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(20),
                                  topLeft: Radius.circular(20)),
                              boxShadow: [
                                BoxShadow(
                                    color: Theme.of(context)
                                        .focusColor
                                        .withOpacity(0.15),
                                    offset: Offset(0, -2),
                                    blurRadius: 5.0)
                              ]),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width - 40,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                if (_con.carts[0].product.market.minimumOrder >
                                    _con.subTotal)
                                  Text(
                                    'Atenção!',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.red,
                                        height: 1.3),
                                  ),
                                SizedBox(height: 10),
                                if (_con.carts[0].product.market.minimumOrder >
                                    _con.subTotal)
                                  Text(
                                    'O pedido mínimo é de:',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.red,
                                        height: 1.3),
                                  ),
                                if (_con.carts[0].product.market.minimumOrder >
                                    _con.subTotal)
                                  Text(
                                    // ignore: null_aware_before_operator
                                    setting.value?.defaultCurrency +
                                        _con.carts[0].product.market
                                            .minimumOrder
                                            .toString(),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.red,
                                        height: 1.3),
                                  ),
                                SizedBox(height: 30),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                        S
                                            .of(context)
                                            .pharms_total, //S.of(context).subtotal,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                    ),
                                    Helper.getPrice(_con.subTotal, context,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1),
                                  ],
                                ),
                                SizedBox(height: 0),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                        S.of(context).delivery_fee,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                    ),
                                    if (_Distance >
                                        _con.carts[0].product.market
                                            .deliveryRange)
                                      Helper.getDeliveryFee(
                                          _con.carts[0].product.market
                                              .deliveryMaxFee,
                                          context,
                                          style: Theme.of(context)
                                              .textTheme
                                              .subtitle1)
                                    else
                                      Helper.getDeliveryFee(
                                          _con.carts[0].product.market
                                              .deliveryFee,
                                          context,
                                          style: Theme.of(context)
                                              .textTheme
                                              .subtitle1),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                        '${S.of(context).tax} ',
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                    ),
                                    Helper.getPrice(_con.taxAmount, context,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1)
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                        S.of(context).total,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1
                                            .apply(color: Colors.red),
                                      ),
                                    ),
                                    Helper.getPrice(_con.total, context,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1
                                            .apply(color: Colors.red))
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
        ),
      ),
    );
  }
}
