import 'package:brasil_fields/brasil_fields.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';
import '../../../generated/l10n.dart';
import '../../elements/BlockButtonWidget.dart';
import 'dialog_controller.dart';
import 'mobile_config.dart';
import 'phone_checked_repository.dart';
import '../../helpers/app_config.dart' as config;

class MobileVerification extends StatelessWidget {
  final _phoneController = TextEditingController();
  final _codeController = TextEditingController();

  get child => null;

  Future<bool> loginUser(String phone, BuildContext context) async {
    FirebaseAuth _auth = FirebaseAuth.instance;

    _auth.verifyPhoneNumber(
      phoneNumber: phone,
      timeout: Duration(seconds: 60),
      verificationCompleted: (AuthCredential credential) async {
        removePhone();
        UserCredential result = await _auth.signInWithCredential(credential);
        User user = result.user;

        if (user != null) {
          setPhoneChecked(phone);
          VerificationCompletedDialog(
              context,
              S
                  .of(context)
                  .kriacoes_agency_module_otp_verification_phone_confirmed_header,
              S
                  .of(context)
                  .kriacoes_agency_module_otp_verification_phone_signup_step,
              S.of(context).kriacoes_agency_module_otp_verification_btn_signup);
        }
      },

      verificationFailed: (FirebaseAuthException authException) {
        print(authException.message);

        if (authException.message.contains('not authorized'))
          errorDialog(
              context,
              S
                  .of(context)
                  .kriacoes_agency_module_otp_verification_not_authorized_title,
              S
                  .of(context)
                  .kriacoes_agency_module_otp_verification_not_authorized_msg);
        else if (authException.message.contains('Network'))
          errorDialog(
              context,
              S
                  .of(context)
                  .kriacoes_agency_module_otp_verification_network_title,
              S
                  .of(context)
                  .kriacoes_agency_module_otp_verification_network_msg);
        else if (authException.message.contains('[ Invalid format. ]'))
          errorDialog(
              context,
              S
                  .of(context)
                  .kriacoes_agency_module_otp_verification_invalid_format_title,
              S
                  .of(context)
                  .kriacoes_agency_module_otp_verification_invalid_format_msg);
        else if (authException.message.contains('[ TOO_SHORT ]'))
          errorDialog(
              context,
              S
                  .of(context)
                  .kriacoes_agency_module_otp_verification_too_short_title,
              S
                  .of(context)
                  .kriacoes_agency_module_otp_verification_too_short_msg);
        else if (authException.message.contains('[ TOO_LONG ]'))
          errorDialog(
              context,
              S
                  .of(context)
                  .kriacoes_agency_module_otp_verification_too_long_title,
              S
                  .of(context)
                  .kriacoes_agency_module_otp_verification_too_long_title);
        else if (authException.message.contains('We have blocked'))
          errorDialog(
              context,
              S
                  .of(context)
                  .kriacoes_agency_module_otp_verification_we_have_blocked_title,
              S
                  .of(context)
                  .kriacoes_agency_module_otp_verification_we_have_blocked_msg);
        else
          errorDialog(
              context,
              S
                  .of(context)
                  .kriacoes_agency_module_otp_verification_generic_title,
              S
                      .of(context)
                      .kriacoes_agency_module_otp_verification_generic_msg +
                  authException.message);
      },

      //Código enviado!
      codeSent: (String verificationId, [int forceResendingToken]) {
        showToast(
            S
                    .of(context)
                    .kriacoes_agency_module_otp_verification_sms_send_code +
                " " +
                phone,
            Theme.of(context).accentColor);

        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (context) {
              return AlertDialog(
                title: Text(S
                    .of(context)
                    .kriacoes_agency_module_otp_verification_give_the_code),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    SizedBox(height: 15),
                    TextFormField(
                      maxLength: 6,
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        // ignore: deprecated_member_use
                        WhitelistingTextInputFormatter.digitsOnly
                      ],
                      controller: _codeController,
                      decoration: InputDecoration(
                        labelStyle:
                            TextStyle(color: Theme.of(context).accentColor),
                        contentPadding: EdgeInsets.all(12),
                        hintText: "0 0 0 0 0 0",
                        hintStyle: TextStyle(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.7)),
                        prefixIcon: Icon(Icons.phonelink_lock,
                            color: Theme.of(context).accentColor),
                        suffixIcon: IconButton(
                          onPressed: () {},
                          color: Theme.of(context).accentColor,
                          icon: Icon(Icons.sms),
                        ),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context)
                                    .focusColor
                                    .withOpacity(0.2))),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context)
                                    .focusColor
                                    .withOpacity(0.5))),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context)
                                    .focusColor
                                    .withOpacity(0.2))),
                      ),
                    ),
                  ],
                ),
                actions: <Widget>[
                  SizedBox(height: 15),
                  BlockButtonWidget(
                    text: Text(
                      S
                          .of(context)
                          .kriacoes_agency_module_otp_verification_btn_check,
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    ),
                    color: Theme.of(context).accentColor,
                    onPressed: () async {
                      if (_codeController.text.length == 6) {
                        try {
                          final code = _codeController.text.trim();
                          AuthCredential credential =
                              PhoneAuthProvider.credential(
                                  verificationId: verificationId,
                                  smsCode: code);
                          UserCredential result =
                              await _auth.signInWithCredential(credential);
                          User user = result.user;

                          if (user != null) {
                            setPhoneChecked(phone);
                            VerificationCompletedDialog(
                                context,
                                S
                                    .of(context)
                                    .kriacoes_agency_module_otp_verification_phone_confirmed_header,
                                S
                                    .of(context)
                                    .kriacoes_agency_module_otp_verification_phone_signup_step,
                                S
                                    .of(context)
                                    .kriacoes_agency_module_otp_verification_btn_signup);
                          }
                        } catch (e) {
                          if (e.toString().contains('credential is invalid.') !=
                              null) {
                            showToast(
                                S
                                    .of(context)
                                    .kriacoes_agency_module_otp_verification_error_invalid_verification_code,
                                Colors.red);
                          } else {
                            showToast(e.toString(), Colors.red);
                          }
                        }
                      } else if (_codeController.text.length == 0) {
                        showToast(
                            S
                                .of(context)
                                .kriacoes_agency_module_otp_verification_empty_code,
                            Colors.red);
                      } else {
                        showToast(
                            S
                                .of(context)
                                .kriacoes_agency_module_otp_verification_wrong_size_code,
                            Colors.red);
                      }
                    },
                  ),
                ],
              );
            });
      },
      codeAutoRetrievalTimeout: (String verificationId) {
        showToast(S.of(context).pharms_otp_code_expires, Colors.red);
      },
    );
  }

  void initState() {
    removePhone();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xfff7f6fb),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 24, horizontal: 25),
          child: Column(
            children: [
              SizedBox(
                height: 50,
              ),
              Container(
                  width: 200,
                  height: 200,
                  decoration: BoxDecoration(
                    color: Colors.red.shade50,
                    shape: BoxShape.circle,
                  ),
                  child: Lottie.network(
                    "https://assets2.lottiefiles.com/packages/lf20_n9b10oth.json",
                    //controller: _controller,
                  )),
              SizedBox(
                height: 24,
              ),
              Text(S.of(context).kriacoes_agency_module_otp_verification_header,
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  )),
              SizedBox(
                height: 10,
              ),
              Text(
                S
                    .of(context)
                    .kriacoes_agency_module_otp_verification_sub_header,
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: Colors.black38,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 28,
              ),
              Container(
                padding: EdgeInsets.all(28),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12),
                  boxShadow: [
                    BoxShadow(
                        color: Theme.of(context).focusColor.withOpacity(0.30),
                        blurRadius: 3,
                        offset: Offset(0, 2)),
                  ],
                ),
                child: Column(
                  children: [
                    TextFormField(
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                        TelefoneInputFormatter()
                      ],
                      keyboardType: TextInputType.number,
                      controller: _phoneController,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                      decoration: InputDecoration(
                        hintText: S
                            .of(context)
                            .kriacoes_agency_module_otp_verification_hint,
                        hintStyle: TextStyle(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.7)),
                        //prefixText: MobileConfig.default_ddi + " ",
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black12),
                            borderRadius: BorderRadius.circular(10)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black12),
                            borderRadius: BorderRadius.circular(10)),
                        suffixIcon: Icon(
                          Icons.phone_android,
                          color: Colors.black,
                          size: 32,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: () {
                          final phone = MobileConfig.default_ddi +
                              _phoneController.text.trim();
                          loginUser(phone, context);
                        },
                        style: ButtonStyle(
                          foregroundColor:
                              MaterialStateProperty.all<Color>(Colors.white),
                          backgroundColor:
                              MaterialStateProperty.all<Color>(Colors.red),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(24.0),
                            ),
                          ),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(14.0),
                          child: Text(
                            'Enviar',
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Divider(
                      color: Colors.grey,
                      height: 15,
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: FlatButton(
                        onPressed: () {
                          Navigator.of(context).pushReplacementNamed('/Login');
                        },
                        textColor: Theme.of(context).hintColor,
                        child: Text(S.of(context).i_have_account_back_to_login),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
