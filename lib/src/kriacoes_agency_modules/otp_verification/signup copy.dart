import 'package:shared_preferences/shared_preferences.dart';
import '../../controllers/user_controller.dart';
import '../../elements/BlockButtonWidget.dart';
import '../../helpers/helper.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../../animation/FadeAnimation.dart';
import '../../../generated/l10n.dart';
import 'package:email_validator/email_validator.dart';
import 'dialog_controller.dart';

class SignUpWidget extends StatefulWidget {
  SignUpWidget({
    Key key,
  }) : super(key: key);

  @override
  _SignUpWidgetState createState() => _SignUpWidgetState();
}

class _SignUpWidgetState extends StateMVC<SignUpWidget> {
  UserController _con;

  _SignUpWidgetState() : super(UserController()) {
    _con = controller;
  }

  String _PhoneChecked = null;

  void initState() {
    _getPhoneChecked();
    super.initState();
  }

  _getPhoneChecked() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _PhoneChecked = (prefs.getString('phone_checked') ?? '');
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        key: _con.scaffoldKey,
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pushReplacementNamed('/SignUp');
            },
            icon: Icon(
              Icons.arrow_back_ios,
              size: 20,
              color: Colors.white,
            ),
          ),
        ),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        FadeAnimation(
                            1,
                            Text(
                              S.of(context).pharms_signup_title,
                              style: TextStyle(
                                  fontSize: 30, fontWeight: FontWeight.bold),
                            )),
                        SizedBox(
                          height: 5,
                        ),
                        FadeAnimation(
                            1.2,
                            Text(
                              S.of(context).pharms_signup_message,
                              style: TextStyle(
                                  fontSize: 15, color: Colors.grey[700]),
                            )),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 40),
                      child: Form(
                        key: _con.loginFormKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            FadeAnimation(
                              1.2,
                              TextFormField(
                                keyboardType: TextInputType.text,
                                onSaved: (input) => _con.user.name = input,
                                validator: (input) => input.length < 3
                                    ? S
                                        .of(context)
                                        .should_be_more_than_3_letters
                                    : null,
                                decoration: InputDecoration(
                                  labelText: S.of(context).full_name,
                                  labelStyle: TextStyle(
                                      color: Theme.of(context).accentColor),
                                  contentPadding: EdgeInsets.all(12),
                                  hintText: S.of(context).john_doe,
                                  hintStyle: TextStyle(
                                      color: Theme.of(context)
                                          .focusColor
                                          .withOpacity(0.8)),
                                  prefixIcon: Icon(Icons.person_outline,
                                      color: Theme.of(context).accentColor),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.8))),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.8))),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.8))),
                                ),
                              ),
                            ),
                            SizedBox(height: 20),
                            FadeAnimation(
                              1.3,
                              TextFormField(
                                keyboardType: TextInputType.emailAddress,
                                onSaved: (input) => _con.user.email = input,
                                validator: (input) =>
                                    EmailValidator.validate(input)
                                        ? null
                                        : S.of(context).should_be_a_valid_email,
                                decoration: InputDecoration(
                                  labelText: S.of(context).email,
                                  labelStyle: TextStyle(
                                      color: Theme.of(context).accentColor),
                                  contentPadding: EdgeInsets.all(12),
                                  hintText: S.of(context).pharms_hint_email,
                                  hintStyle: TextStyle(
                                      color: Theme.of(context)
                                          .focusColor
                                          .withOpacity(0.7)),
                                  prefixIcon: Icon(Icons.alternate_email,
                                      color: Theme.of(context).accentColor),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.2))),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.5))),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.2))),
                                ),
                              ),
                            ),
                            SizedBox(height: 20),
                            FadeAnimation(
                              1.3,
                              GestureDetector(
                                onTap: () {
                                  warningDialog(
                                      context,
                                      S
                                          .of(context)
                                          .kriacoes_agency_module_otp_verification_change_number_title,
                                      S
                                              .of(context)
                                              .kriacoes_agency_module_otp_verification_change_number_msg_1 +
                                          " " +
                                          _PhoneChecked +
                                          " " +
                                          S
                                              .of(context)
                                              .kriacoes_agency_module_otp_verification_change_number_msg_2,
                                      S
                                          .of(context)
                                          .kriacoes_agency_module_otp_verification_change_number_no,
                                      S
                                          .of(context)
                                          .kriacoes_agency_module_otp_verification_change_number_yes);
                                },
                                child: TextFormField(
                                  onSaved: (input) =>
                                      _con.user.phone = _PhoneChecked,
                                  enabled: false,
                                  initialValue: _PhoneChecked,
                                  decoration: InputDecoration(
                                    labelStyle: TextStyle(
                                        color: Theme.of(context).accentColor),
                                    contentPadding: EdgeInsets.all(12),
                                    hintText: _PhoneChecked,
                                    hintStyle: TextStyle(
                                        color: Theme.of(context)
                                            .focusColor
                                            .withOpacity(0.7)),
                                    prefixIcon: Icon(Icons.phonelink_lock,
                                        color: Theme.of(context).accentColor),
                                    suffixIcon: IconButton(
                                      color: Theme.of(context).accentColor,
                                      icon: Icon(Icons.verified_user),
                                    ),
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Theme.of(context)
                                                .focusColor
                                                .withOpacity(0.2))),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Theme.of(context)
                                                .focusColor
                                                .withOpacity(0.5))),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Theme.of(context)
                                                .focusColor
                                                .withOpacity(0.2))),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 20),
                            FadeAnimation(
                              1.3,
                              TextFormField(
                                obscureText: _con.hidePassword,
                                onSaved: (input) => _con.user.password = input,
                                validator: (input) => input.length < 6
                                    ? S
                                        .of(context)
                                        .should_be_more_than_6_letters
                                    : null,
                                decoration: InputDecoration(
                                  labelText: S.of(context).password,
                                  labelStyle: TextStyle(
                                      color: Theme.of(context).accentColor),
                                  contentPadding: EdgeInsets.all(12),
                                  hintText: '••••••••••••',
                                  hintStyle: TextStyle(
                                      color: Theme.of(context)
                                          .focusColor
                                          .withOpacity(0.7)),
                                  prefixIcon: Icon(Icons.lock_outline,
                                      color: Theme.of(context).accentColor),
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        _con.hidePassword = !_con.hidePassword;
                                      });
                                    },
                                    color: Theme.of(context).focusColor,
                                    icon: Icon(_con.hidePassword
                                        ? Icons.visibility
                                        : Icons.visibility_off),
                                  ),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.2))),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.5))),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.2))),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    FadeAnimation(
                      1.4,
                      BlockButtonWidget(
                          text: Text(
                            S.of(context).register,
                            style: TextStyle(
                                color: Theme.of(context).primaryColor),
                          ),
                          color: Theme.of(context).accentColor,
                          onPressed: () {
                            _con.register();
                          }),
                    ),
                    FadeAnimation(
                        1.6,
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 40),
                          child: Container(
                            padding: EdgeInsets.only(top: 0, left: 0),
                            child: BlockButtonWidget(
                              text: Text(
                                S.of(context).pharms_have_account,
                                style: TextStyle(color: Colors.black),
                              ),
                              color: Colors.white,
                              onPressed: () {
                                Navigator.of(context).pushNamed('/Login');
                              },
                            ),
                          ),
                        )),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget makeInput({label, obscureText = false}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          label,
          style: TextStyle(
              fontSize: 15, fontWeight: FontWeight.w400, color: Colors.black87),
        ),
        SizedBox(
          height: 5,
        ),
        TextField(
          obscureText: obscureText,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[400])),
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[400])),
          ),
        ),
        SizedBox(
          height: 30,
        ),
      ],
    );
  }
}
