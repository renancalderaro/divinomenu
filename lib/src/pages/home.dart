import 'package:lottie/lottie.dart';

import '../repository/settings_repository.dart';
import '../repository/user_repository.dart';
import 'package:flutter_svg/svg.dart';
import '../elements/BlockButtonWidget.dart';
import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../../generated/l10n.dart';
import '../controllers/home_controller.dart';
import '../elements/CardsCarouselWidget.dart';
import '../elements/CaregoriesCarouselWidget.dart';
import '../elements/DeliveryAddressBottomSheetWidget.dart';
import '../elements/GridWidget.dart';
import '../elements/HomeSliderWidget.dart';
import '../elements/ProductsCarouselWidget.dart';
import '../elements/ReviewsListWidget.dart';
import '../elements/SearchBarWidget.dart';
import '../repository/settings_repository.dart' as settingsRepo;
import '../repository/user_repository.dart' as userRepo;

class HomeWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  HomeWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends StateMVC<HomeWidget> {
  HomeController _con;

  _HomeWidgetState() : super(HomeController()) {
    _con = controller;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return userRepo.currentUser.value.apiToken == null
        ? Container(
            color: Colors.white,
            alignment: AlignmentDirectional.center,
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 50),
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    SizedBox(height: 20),
                    Positioned(
                      right: -30,
                      bottom: -50,
                      child: Container(
                        width: 100,
                        height: 100,
                        decoration: BoxDecoration(
                          color: Theme.of(context)
                              .scaffoldBackgroundColor
                              .withOpacity(0.15),
                          borderRadius: BorderRadius.circular(150),
                        ),
                      ),
                    ),
                    Positioned(
                      left: -20,
                      top: -50,
                      child: Container(
                        width: 120,
                        height: 120,
                        decoration: BoxDecoration(
                          color: Theme.of(context)
                              .scaffoldBackgroundColor
                              .withOpacity(0.15),
                          borderRadius: BorderRadius.circular(150),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 5),
                Image(
                  image: AssetImage('assets/img/logodivino.png'),
                  height: 80,
                ),
                SizedBox(height: 25),
                Opacity(
                  opacity: 1.0,
                  child: Text(
                    S.of(context).pharms_warning_area,
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .caption
                        .merge(TextStyle(color: Colors.black, fontSize: 17)),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                    decoration: BoxDecoration(),
                    child: Lottie.network(
                      "https://assets6.lottiefiles.com/packages/lf20_o2lou8ez.json",
                      //controller: _controller,
                    )),
                SizedBox(height: 25),
                Opacity(
                  opacity: 1.0,
                  child: Text(
                    S.of(context).pharms_warning_login,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
                SizedBox(height: 10),
                FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(5),
                        topLeft: Radius.circular(5),
                        bottomRight: Radius.circular(5),
                        bottomLeft: Radius.circular(5),
                      ),
                    ),
                    color: Colors.red,
                    onPressed: () {
                      Navigator.of(context).pushNamed('/Login');
                    },
                    child: Text(
                      S.of(context).pharms_account_login,
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 17),
                    )),
                SizedBox(height: 10),
                MaterialButton(
                  elevation: 0,
                  onPressed: () {
                    Navigator.of(context).pushReplacementNamed('/SignUp');
                  },
                  padding: EdgeInsets.symmetric(vertical: 12, horizontal: 25),
                  shape: StadiumBorder(),
                  child: Text(
                    'Eu não tenho cadastro :(',
                    style: Theme.of(context)
                        .textTheme
                        .caption
                        .merge(TextStyle(color: Colors.black, fontSize: 17)),
                  ),
                ),
              ],
            ),
          )
        : Scaffold(
            appBar: AppBar(
              leading: new IconButton(
                icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
                onPressed: () =>
                    widget.parentScaffoldKey.currentState.openDrawer(),
              ),
              automaticallyImplyLeading: false,
              backgroundColor: Colors.transparent,
              elevation: 0,
              centerTitle: true,
              title: ValueListenableBuilder(
                valueListenable: settingsRepo.setting,
                builder: (context, value, child) {
                  return Image(
                    image: AssetImage('assets/img/logodivino.png'),
                    height: 50,
                  );
                },
              ),
            ),
            body: settingsRepo.deliveryAddress.value?.address == null
                ? Container(
                    color: Colors.white,
                    alignment: AlignmentDirectional.center,
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 50),
                    height: MediaQuery.of(context).size.height,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            SizedBox(height: 0),
                            Positioned(
                              right: -30,
                              bottom: -50,
                              child: Container(
                                width: 100,
                                height: 100,
                                decoration: BoxDecoration(
                                  color: Theme.of(context)
                                      .scaffoldBackgroundColor
                                      .withOpacity(0.15),
                                  borderRadius: BorderRadius.circular(150),
                                ),
                              ),
                            ),
                            Positioned(
                              left: -20,
                              top: -50,
                              child: Container(
                                width: 120,
                                height: 120,
                                decoration: BoxDecoration(
                                  color: Theme.of(context)
                                      .scaffoldBackgroundColor
                                      .withOpacity(0.15),
                                  borderRadius: BorderRadius.circular(150),
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 5),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 50),
                          child: currentUser.value.apiToken != null
                              ? Column(
                                  children: [
                                    Text(
                                      S.of(context).pharms_welcome +
                                          '${currentUser.value.name}',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 22,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                )
                              : Container(),
                        ),
                        Container(
                            decoration: BoxDecoration(),
                            child: Lottie.network(
                              "https://assets4.lottiefiles.com/packages/lf20_xmnyrzjz.json",
                              //controller: _controller,
                            )),
                        SizedBox(height: 10),
                        Text(
                          S.of(context).pharms_warning_gps,
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.black, fontSize: 18),
                        ),
                        if (deliveryAddress.value?.address != null)
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 30),
                            child: Text(
                              S.of(context).pharms_near_by +
                                  " " +
                                  (deliveryAddress.value?.address),
                              style: TextStyle(color: Colors.white),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        SizedBox(height: 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            AvatarGlow(
                                endRadius: 100,
                                duration: Duration(seconds: 2),
                                glowColor: Colors.red,
                                repeat: true,
                                repeatPauseDuration: Duration(seconds: 1),
                                startDelay: Duration(milliseconds: 100),
                                child: GestureDetector(
                                  onTap: () async {
                                    var bottomSheetController = widget
                                        .parentScaffoldKey.currentState
                                        .showBottomSheet(
                                      (context) =>
                                          DeliveryAddressBottomSheetWidget(
                                              scaffoldKey:
                                                  widget.parentScaffoldKey),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: new BorderRadius.only(
                                            topLeft: Radius.circular(10),
                                            topRight: Radius.circular(10)),
                                      ),
                                    );
                                    bottomSheetController.closed.then((value) {
                                      _con.refreshHome();
                                    });
                                  },
                                  child: Material(
                                      elevation: 8.0,
                                      shape: CircleBorder(),
                                      child: CircleAvatar(
                                        backgroundColor: Theme.of(context)
                                            .scaffoldBackgroundColor,
                                        child: Lottie.network(
                                          "https://assets8.lottiefiles.com/packages/lf20_naef7z00.json",
                                          //controller: _controller,
                                        ),
                                        radius: 50.0,
                                      )),
                                )),
                          ],
                        )
                      ],
                    ),
                  )
                //GPS
                /* Stack(
                    alignment: AlignmentDirectional.center,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Column(
                            children: [
                              Container(
                                child: Lottie.network(
                                  "https://assets4.lottiefiles.com/packages/lf20_xmnyrzjz.json",
                                  //controller: _controller,
                                ),
                                padding: EdgeInsets.only(
                                    top: 0, right: 50, left: 70, bottom: 10),
                              ),
                              Text(
                                S.of(context).pharms_warning_gps,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 18),
                              ),
                              if (deliveryAddress.value?.address != null)
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 30),
                                  child: Text(
                                    S.of(context).pharms_near_by +
                                        " " +
                                        (deliveryAddress.value?.address),
                                    style: TextStyle(color: Colors.white),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                            ],
                          ),
                        ],
                      ),
                      Positioned(
                        top: 50,
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 50),
                          child: currentUser.value.apiToken != null
                              ? Column(
                                  children: [
                                    Text(
                                      S.of(context).pharms_welcome +
                                          '${currentUser.value.name}',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 22,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                )
                              : Container(),
                        ),
                      ),
                      Positioned(
                          bottom: 30,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              AvatarGlow(
                                  endRadius: 100,
                                  duration: Duration(seconds: 2),
                                  glowColor:
                                      Theme.of(context).scaffoldBackgroundColor,
                                  repeat: true,
                                  repeatPauseDuration: Duration(seconds: 1),
                                  startDelay: Duration(milliseconds: 200),
                                  child: GestureDetector(
                                    onTap: () async {
                                      var bottomSheetController = widget
                                          .parentScaffoldKey.currentState
                                          .showBottomSheet(
                                        (context) =>
                                            DeliveryAddressBottomSheetWidget(
                                                scaffoldKey:
                                                    widget.parentScaffoldKey),
                                        shape: RoundedRectangleBorder(
                                          borderRadius: new BorderRadius.only(
                                              topLeft: Radius.circular(10),
                                              topRight: Radius.circular(10)),
                                        ),
                                      );
                                      bottomSheetController.closed
                                          .then((value) {
                                        _con.refreshHome();
                                      });
                                    },
                                    child: Material(
                                        elevation: 8.0,
                                        shape: CircleBorder(),
                                        child: CircleAvatar(
                                          backgroundColor: Theme.of(context)
                                              .scaffoldBackgroundColor,
                                          child: Lottie.network(
                                            "https://assets8.lottiefiles.com/packages/lf20_naef7z00.json",
                                            //controller: _controller,
                                          ),
                                          radius: 50.0,
                                        )),
                                  )),
                            ],
                          )),
                    ],
                  )*/
                : RefreshIndicator(
                    onRefresh: _con.refreshHome,
                    child: SingleChildScrollView(
                      padding:
                          EdgeInsets.symmetric(horizontal: 0, vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: List.generate(
                            settingsRepo.setting.value.homeSections.length,
                            (index) {
                          String _homeSection = settingsRepo
                              .setting.value.homeSections
                              .elementAt(index);
                          switch (_homeSection) {
                            case 'slider':
                              return HomeSliderWidget(slides: _con.slides);
                            case 'search':
                              return Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: SearchBarWidget(
                                  onClickFilter: (event) {
                                    widget.parentScaffoldKey.currentState
                                        .openEndDrawer();
                                  },
                                ),
                              );

                            case 'top_markets_heading':
                              return Padding(
                                padding: const EdgeInsets.only(
                                    top: 15, left: 20, right: 20, bottom: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Text(
                                            S.of(context).top_markets,
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline4,
                                            maxLines: 1,
                                            softWrap: false,
                                            overflow: TextOverflow.fade,
                                          ),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            var bottomSheetController = widget
                                                .parentScaffoldKey.currentState
                                                .showBottomSheet(
                                              (context) =>
                                                  DeliveryAddressBottomSheetWidget(
                                                      scaffoldKey: widget
                                                          .parentScaffoldKey),
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    new BorderRadius.only(
                                                        topLeft:
                                                            Radius.circular(10),
                                                        topRight:
                                                            Radius.circular(
                                                                10)),
                                              ),
                                            );
                                            bottomSheetController.closed
                                                .then((value) {
                                              _con.refreshHome();
                                            });
                                          },
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 6, horizontal: 10),
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5)),
                                              color: settingsRepo
                                                          .deliveryAddress
                                                          .value
                                                          ?.address ==
                                                      null
                                                  ? Theme.of(context)
                                                      .accentColor
                                                  : Theme.of(context)
                                                      .accentColor,
                                            ),
                                            child: Icon(
                                              Icons.arrow_drop_down,
                                              color: Theme.of(context)
                                                  .primaryColor,
                                              size: 18,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    if (settingsRepo
                                            .deliveryAddress.value?.address !=
                                        null)
                                      Padding(
                                        padding: const EdgeInsets.only(top: 10),
                                        child: Text(
                                          (settingsRepo
                                              .deliveryAddress.value?.address),
                                          style: Theme.of(context)
                                              .textTheme
                                              .caption
                                              .merge(TextStyle(
                                                  fontSize: 16,
                                                  color: Theme.of(context)
                                                      .accentColor,
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                      ),
                                  ],
                                ),
                              );
                            case 'top_markets':
                              return CardsCarouselWidget(
                                  marketsList: _con.topMarkets,
                                  heroTag: 'home_top_markets');
                            case 'trending_week_heading':
                              return ListTile(
                                dense: true,
                                contentPadding:
                                    EdgeInsets.symmetric(horizontal: 20),
                                title: Text(
                                  S.of(context).pharms_not_have_pharmacies,
                                  textAlign: TextAlign.center,
                                  style: Theme.of(context)
                                      .textTheme
                                      .caption
                                      .merge(TextStyle(
                                          fontSize: 15,
                                          color: Theme.of(context).accentColor,
                                          fontWeight: FontWeight.bold)),
                                ),
                              );
                            case 'trending_week':
                              return ProductsCarouselWidget(
                                  productsList: _con.trendingProducts,
                                  heroTag: 'home_product_carousel');
                            case 'categories_heading':
                              return Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: ListTile(
                                  dense: true,
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 0),
                                  leading: Icon(
                                    Icons.category,
                                    color: Theme.of(context).hintColor,
                                  ),
                                  title: Text(
                                    S.of(context).product_categories,
                                    style:
                                        Theme.of(context).textTheme.headline4,
                                  ),
                                ),
                              );
                            case 'categories':
                              return CategoriesCarouselWidget(
                                categories: _con.categories,
                              );
                            case 'popular_heading':
                              return Padding(
                                padding: const EdgeInsets.only(
                                    left: 20, right: 20, bottom: 20),
                                child: ListTile(
                                  dense: true,
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 0),
                                  leading: Icon(
                                    Icons.trending_up,
                                    color: Theme.of(context).hintColor,
                                  ),
                                  title: Text(
                                    S.of(context).most_popular,
                                    style:
                                        Theme.of(context).textTheme.headline4,
                                  ),
                                ),
                              );
                            case 'popular':
                              return Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: GridWidget(
                                  marketsList: _con.popularMarkets,
                                  heroTag: 'home_markets',
                                ),
                              );
                            case 'recent_reviews_heading':
                              return Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: ListTile(
                                  dense: true,
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 20),
                                  leading: Icon(
                                    Icons.recent_actors,
                                    color: Theme.of(context).hintColor,
                                  ),
                                  title: Text(
                                    S.of(context).recent_reviews,
                                    style:
                                        Theme.of(context).textTheme.headline4,
                                  ),
                                ),
                              );
                            case 'recent_reviews':
                              return Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: ReviewsListWidget(
                                    reviewsList: _con.recentReviews),
                              );
                            default:
                              return SizedBox(height: 0);
                          }
                        }),
                      ),
                    ),
                  ),
          );
  }
}
