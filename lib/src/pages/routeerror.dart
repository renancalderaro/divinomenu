import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../elements/RoutError.dart';
import '../repository/user_repository.dart';

class RouteErrorWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  RouteErrorWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _RouteErrorWidgetState createState() => _RouteErrorWidgetState();
}

class _RouteErrorWidgetState extends StateMVC<RouteErrorWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          title: Text(
            '',
            style: Theme.of(context)
                .textTheme
                .headline6
                .merge(TextStyle(letterSpacing: 1.3)),
          ),
        ),
        body: currentUser.value.apiToken == null
            ? RoutErrorWidget()
            : RoutErrorWidget());
  }
}
