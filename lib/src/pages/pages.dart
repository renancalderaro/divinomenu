import '../../generated/l10n.dart';
import 'package:flutter/material.dart';

import '../elements/DrawerWidget.dart';
import '../elements/FilterWidget.dart';
import '../helpers/helper.dart';
import '../models/route_argument.dart';
import '../pages/home.dart';
import '../pages/map.dart';
import '../pages/orders.dart';
import '../kriacoes_agency_modules/minimum_order/pages/cart.dart';
import '../pages/favorites.dart';

// ignore: must_be_immutable
class PagesWidget extends StatefulWidget {
  dynamic currentTab;
  RouteArgument routeArgument;
  Widget currentPage = HomeWidget();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  PagesWidget({
    Key key,
    this.currentTab,
  }) {
    if (currentTab != null) {
      if (currentTab is RouteArgument) {
        routeArgument = currentTab;
        currentTab = int.parse(currentTab.id);
      }
    } else {
      currentTab = 2;
    }
  }

  @override
  _PagesWidgetState createState() {
    return _PagesWidgetState();
  }
}

class _PagesWidgetState extends State<PagesWidget> {
  initState() {
    super.initState();
    _selectTab(widget.currentTab);
  }

  @override
  void didUpdateWidget(PagesWidget oldWidget) {
    _selectTab(oldWidget.currentTab);
    super.didUpdateWidget(oldWidget);
  }

  void _selectTab(int tabItem) {
    setState(() {
      widget.currentTab = tabItem;
      switch (tabItem) {
        case 0:
          widget.currentPage = MapWidget(parentScaffoldKey: widget.scaffoldKey);
          break;
        case 1:
          widget.currentPage =
              OrdersWidget(parentScaffoldKey: widget.scaffoldKey);
          break;

        case 2:
          widget.currentPage =
              HomeWidget(parentScaffoldKey: widget.scaffoldKey);
          break;
        case 3:
          widget.currentPage = CartWidget();
          break;
        case 4:
          widget.currentPage = FavoritesWidget();
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        key: widget.scaffoldKey,
        drawer: DrawerWidget(),
        endDrawer: FilterWidget(onFilter: (filter) {
          Navigator.of(context)
              .pushReplacementNamed('/Pages', arguments: widget.currentTab);
        }),
        body: widget.currentPage,
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedItemColor: Theme.of(context).accentColor,
          selectedFontSize: 0,
          unselectedFontSize: 0,
          iconSize: 22,
          elevation: 0,
          backgroundColor: Colors.transparent,
          selectedIconTheme: IconThemeData(size: 22),
          unselectedItemColor: Theme.of(context).focusColor.withOpacity(1),
          currentIndex: widget.currentTab,
          onTap: (int i) {
            this._selectTab(i);
          },
          // this will be set when a new tab is tapped

          items: [
            BottomNavigationBarItem(
                title: new Container(height: 13.0),
                icon: Container(
                  padding: const EdgeInsets.only(
                    top: 8,
                  ),
                  child: (Container(
                    child: Column(children: <Widget>[
                      new Icon(Icons.pin_drop),
                      Text(
                          S.of(context).pharms_bottom_navigation_map,
                        style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                      ),
                    ]),
                  )),
                )),
            BottomNavigationBarItem(
                title: new Container(height: 13.0),
                icon: Container(
                  padding: const EdgeInsets.only(
                    top: 8,
                  ),
                  child: (Container(
                    child: Column(children: <Widget>[
                      new Icon(Icons.local_mall),
                      Text(
                        S.of(context).pharms_bottom_navigation_orders,
                        style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                      ),
                    ]),
                  )),
                )),
            BottomNavigationBarItem(
                title: new Container(height: 13.0),
                icon: Container(
                  padding: const EdgeInsets.only(
                    top: 8,
                  ),
                  child: (Container(
                    child: Column(children: <Widget>[
                      new Icon(Icons.home),
                      Text(
                        S.of(context).pharms_bottom_navigation_home,
                        style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                      ),
                    ]),
                  )),
                )),
            BottomNavigationBarItem(
                title: new Container(height: 13.0),
                icon: Container(
                  child: (Container(
                    padding: const EdgeInsets.only(
                      top: 8,
                    ),
                    child: Column(children: <Widget>[
                      new Icon(Icons.shopping_cart),
                      Text(
                        S.of(context).pharms_bottom_navigation_cart,
                        style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                      ),
                    ]),
                  )),
                )),
            BottomNavigationBarItem(
                title: new Container(height: 13.0),
                icon: Container(
                  child: (Container(
                    padding: const EdgeInsets.only(
                      top: 8,
                    ),
                    child: Column(children: <Widget>[
                      new Icon(Icons.favorite),
                      Text(
                        S.of(context).pharms_bottom_navigation_favorites,
                        style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                      ),
                    ]),
                  )),
                )),
          ],
        ),
      ),
    );
  }
}
